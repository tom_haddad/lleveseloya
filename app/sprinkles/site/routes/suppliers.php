<?php
/**
 * Routes for administrative supplier management.
 */
$app->group('/admin/suppliers', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:pageList')
        ->setName('uri_suppliers');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:pageInfo');
})->add('authGuard');

$app->group('/api/suppliers', function () {
    $this->delete('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:delete');

    $this->delete('/g/access/{user_id}/{supplier_id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:revokeAccess');

    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getList');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getInfo');

    $this->get('/g/{id}/products', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getProducts');

    $this->get('/g/{id}/users', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getUsers');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:create');

    $this->put('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:updateInfo');

    $this->put('/g/add-user/{id}', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:addUser');

})->add('authGuard');

$app->group('/modals/suppliers', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getModalConfirmDelete');

    $this->get('/confirm-remove-access', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getModalConfirmRevoke');

    $this->get('/create', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getModalEdit');

    $this->get('/add_user', 'UserFrosting\Sprinkle\Site\Controller\SupplierController:getModalAddUser');
})->add('authGuard');
