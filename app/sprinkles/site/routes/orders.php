<?php
/**
 * Routes for administrative order management.
 */
$app->group('/admin/orders', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\OrderController:pageList')
        ->setName('uri_orders');

    $this->get('/o/{id}', 'UserFrosting\Sprinkle\Site\Controller\OrderController:pageInfo');
})->add('authGuard');

$app->group('/api/orders', function () {

    $this->put('/create', 'UserFrosting\Sprinkle\Site\Controller\OrderController:processOrder');

    $this->delete('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\OrderController:delete');

    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getList');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getInfo');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\OrderController:create');

    $this->put('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\OrderController:updateInfo');

    $this->get('/o/{id}/items', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getItems');

    $this->put('/mark-shipped', 'UserFrosting\Sprinkle\Site\Controller\OrderController:markShipped');
})->add('authGuard');

$app->group('/modals/orders', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getModalConfirmDelete');

    $this->get('/create', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getModalEdit');

    $this->get('/item', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getModalItem');

    $this->get('/payment', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getModalPayment');
})->add('authGuard');

$app->group('/api/qr', function() {
    $this->get('/o/{reference}.png', 'UserFrosting\Sprinkle\Site\Controller\OrderController:getQRCode');
});
