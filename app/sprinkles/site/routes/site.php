<?php
/**
 * Lléveselo Ya!
 *
 */

global $app;
$config = $app->getContainer()->get('config');

$app->get('/', 'UserFrosting\Sprinkle\Site\Controller\SiteController:pageIndex')
    ->add('checkEnvironment')
    ->setName('index');

$app->get('/front/modal/{name}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:getModal');

$app->get('/front/modal/{name}/{id}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:getModal');

$app->get('/front/side/{name}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:getSideModal');

$app->get('/front/side/{name}/{id}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:getSideModal');

$app->get('/about','UserFrosting\Sprinkle\Core\Controller\CoreController:pageAbout')->add('checkEnvironment');

$app->get('/alerts', 'UserFrosting\Sprinkle\Core\Controller\CoreController:jsonAlerts');

$app->get('/legal', 'UserFrosting\Sprinkle\Core\Controller\CoreController:pageLegal');

$app->get('/privacy', 'UserFrosting\Sprinkle\Core\Controller\CoreController:pagePrivacy');

$app->get('/cron', 'UserFrosting\Sprinkle\Site\Controller\CronController:doAllChecks');

/* Account validation on da fly */

$app->get('/account/sign-in', 'UserFrosting\Sprinkle\Site\Controller\SiteController:pageIndex')->add('checkEnvironment')->setName('login');

$app->get('/account/check-password', 'UserFrosting\Sprinkle\Site\Controller\SiteController:checkPassword');

$app->get('/account/check-conf-password', 'UserFrosting\Sprinkle\Site\Controller\SiteController:checkConfPassword');

$app->get('/account/check-email', 'UserFrosting\Sprinkle\Site\Controller\SiteController:checkEmail');

$app->group('/my-account', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\SiteController:accountSettings');
    $this->get('/settings', 'UserFrosting\Sprinkle\Site\Controller\SiteController:accountSettings');
    $this->get('/orders', 'UserFrosting\Sprinkle\Site\Controller\SiteController:accountOrders');
    $this->get('/history', 'UserFrosting\Sprinkle\Site\Controller\SiteController:accountHistory');
    $this->get('/cancelled', 'UserFrosting\Sprinkle\Site\Controller\SiteController:accountCancelled');
})->add('checkEnvironment')->add('authGuard');

$app->group('/supplier', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierIndex');
    $this->group('/orders', function() {
        $this->get('', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierOrders');
        $this->get('/table', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierOrderTable');
        $this->get('/{id}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierGetOrder');
        $this->post('/{id}/mark-shipped', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierMarkShipped');
    });
    $this->group('/products', function() {
    	$this->get('', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierProducts');
        $this->post('/edit_image/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:updatePhotoInfoFront');
    	$this->get('/inventory', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierInventory');
        $this->get('/inventory/table', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierInventoryTable');
    	$this->get('/new', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierAddProduct');
        $this->get('/new/variant', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierAddVariant');
        $this->get('/new/variants', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierAddVariants');
        $this->get('/table', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierProductTable');
    	$this->get('/{id}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierGetProduct');
    	$this->get('/{id}/variants', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierGetProductVariants');
    	$this->get('/{id}/variants/{variant_id}', 'UserFrosting\Sprinkle\Site\Controller\SiteController:supplierGetProductVariant');

        $this->post('/fee', 'UserFrosting\Sprinkle\Site\Controller\ProductController:frontGetFee');
        $this->post('/new', 'UserFrosting\Sprinkle\Site\Controller\ProductController:frontCreate');
        $this->post('/{product_id}/inventory_update', 'UserFrosting\Sprinkle\Site\Controller\ProductController:frontUpdateInventory');
        $this->post('/{product_id}/variants/save', 'UserFrosting\Sprinkle\Site\Controller\ProductController:frontSaveVariant');
    });
})->add('authGuard');

$app->group('/actions', function () {
    $this->get('/check-subs/{is_sub}/{id}', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getSubDropdown');
})->add('authGuard');
