<?php
/**
 * Routes for administrative countdown management.
 */
$app->group('/admin/countdowns', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:pageList')
        ->setName('uri_countdowns');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:pageInfo');
})->add('authGuard');

$app->group('/api/countdowns', function () {
    $this->delete('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:delete');

    $this->delete('/g/access/{user_id}/{countdown_id}', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:revokeAccess');

    $this->get('/bespoke', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getBespokeList');

    $this->get('/weekly', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getWeeklyList');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getInfo');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:create');

    $this->put('/g/{id}/bespoke', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:updateInfoBespoke');

    $this->put('/g/{id}/weekly', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:updateInfoWeekly');

    $this->put('/g/add-user/{id}', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:addUser');

    $this->put('/g/{id}/products', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:updateProducts');

})->add('authGuard');

$app->group('/modals/countdowns', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalConfirmDelete');

    $this->get('/confirm-remove-access', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalConfirmRevoke');

    $this->get('/create-bespoke', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalCreateBespoke');

    $this->get('/create-weekly', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalCreateWeekly');

    $this->get('/products', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalProducts');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalEdit');

    $this->get('/add_user', 'UserFrosting\Sprinkle\Site\Controller\CountdownController:getModalAddUser');
})->add('authGuard');
