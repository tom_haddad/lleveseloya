<?php

$app->group('/api/images/upload', function () {
	$this->post('', 'UserFrosting\Sprinkle\Site\Controller\ImageController:upload');
    $this->post('/{model}/{id}/{field}', 'UserFrosting\Sprinkle\Site\Controller\ImageController:upload');
    $this->post('/{model}/{id}/{field}/{id_2}/{field_2}', 'UserFrosting\Sprinkle\Site\Controller\ImageController:upload');
    $this->post('/{model}/{id}/{field}/{id_2}/{field_2}/{action}', 'UserFrosting\Sprinkle\Site\Controller\ImageController:upload');
})->add('authGuard');

$app->group('/get_image', function () {
    $this->get('/{file_name}.{ext}', 'UserFrosting\Sprinkle\Site\Controller\ImageController:get');
});

$app->group('/get_image_modal', function () {
    $this->get('/{id}', 'UserFrosting\Sprinkle\Site\Controller\ImageController:getModal');
});
