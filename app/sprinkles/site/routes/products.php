<?php
/**
 * Routes for administrative product management.
 */
$app->group('/admin/products', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\ProductController:pageList')
        ->setName('uri_products');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:pageInfo');
})->add('authGuard');

$app->group('/api/products', function () {
    $this->delete('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:delete');

    $this->delete('/g/delete-photo/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:deletePhoto');

    $this->delete('/g/delete-variant/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:deleteVariant');

    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getList');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getInfo');

    $this->get('/g/{id}/deals', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getDeals');

    $this->get('/g/{id}/stock', 'UserFrosting\Sprinkle\Site\Controller\ProductController:checkStock');

    $this->get('/g/{id}/variants', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getVariants');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\ProductController:create');

    $this->put('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:updateInfo');

    $this->put('/g/add-deal/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:addDeal');

    $this->put('/g/add-variant/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:addVariant');

    $this->put('/g/edit-deal/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:updateDealInfo');

    $this->put('/g/edit-photo/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:updatePhotoInfo');

    $this->put('/g/edit-variant/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:updateVariantInfo');

    $this->put('/mark-approved', 'UserFrosting\Sprinkle\Site\Controller\ProductController:markApproved');

    $this->put('/multistock', 'UserFrosting\Sprinkle\Site\Controller\ProductController:checkStockMultiple');
})->add('authGuard');

$app->group('/modals/products', function () {

    $this->get('/add-deal', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalDealCreate');

    $this->get('/add-photos', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalPhotosAdd');

    $this->get('/add-variant', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalVariantCreate');

    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalConfirmDelete');

    $this->get('/confirm-delete-deal', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalConfirmDeleteDeal');

    $this->get('/confirm-delete-photo', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalConfirmDeletePhoto');

    $this->get('/confirm-delete-variant', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalConfirmDeleteVariant');

    $this->get('/create', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalEdit');

    $this->get('/edit-deal', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalEditDeal');

    $this->get('/edit-photo-details', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalEditPhoto');

    $this->get('/edit-variant', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getModalEditVariant');
})->add('authGuard');

$app->group('/product', function () {
    $this->get('/{category}/{name}/{id}', 'UserFrosting\Sprinkle\Site\Controller\ProductController:getProduct');
})->add('authGuard');
