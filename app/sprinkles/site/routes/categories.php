<?php
/**
 * Routes for administrative category management.
 */
$app->group('/admin/categories', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:pageList')
        ->setName('uri_categories');

    //$this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:pageInfo');
})->add('authGuard');

$app->group('/api/categories', function () {
    $this->delete('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:delete');

    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getList');

    $this->get('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getInfo');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:create');

    $this->put('/g/{id}', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:updateInfo');
})->add('authGuard');

$app->group('/modals/categories', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getModalConfirmDelete');

    $this->get('/confirm-delete-sub', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getModalConfirmDeleteSub');

    $this->get('/create', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getModalEdit');

    $this->get('/edit-sub', 'UserFrosting\Sprinkle\Site\Controller\CategoryController:getModalEditSub');
})->add('authGuard');
