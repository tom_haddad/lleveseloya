<?php
global $app;

$app->group('/customers', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:pageCustomers')
        ->setName('uri_users');

    $this->get('/u/{user_name}', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:pageInfoCustomer');
});

$app->group('/users', function () {
    $this->get('/u/{user_name}', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:pageInfo');
})->add('authGuard');

$app->group('/api/customers', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:getListCustomers');

    $this->get('/u/{user_name}/orders', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:getOrders');
});

$app->group('/api/users', function () {
    $this->delete('/u/{user_name}', 'UserFrosting\Sprinkle\Admin\Controller\UserController:delete');

    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:getList');

    $this->get('/u/{user_name}', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getInfo');

    $this->get('/u/{user_name}/activities', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getActivities');

    $this->get('/u/{user_name}/roles', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getRoles');

    $this->post('', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:create');

    $this->post('/u/{user_name}/password-reset', 'UserFrosting\Sprinkle\Admin\Controller\UserController:createPasswordReset');

    $this->put('/u/{user_name}', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:updateInfo');

    $this->put('/u/{user_name}/{field}', 'UserFrosting\Sprinkle\Admin\Controller\UserController:updateField');
})->add('authGuard');

$app->group('/modals/users', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getModalConfirmDelete');

    $this->get('/create', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Site\Controller\UsermetaController:getModalEdit');

    $this->get('/password', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getModalEditPassword');

    $this->get('/roles', 'UserFrosting\Sprinkle\Admin\Controller\UserController:getModalEditRoles');
})->add('authGuard');
