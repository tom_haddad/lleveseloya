<?php

$app->group('/api/cart', function () {
    $this->put('/add/{id}', 'UserFrosting\Sprinkle\Site\Controller\CartController:addItem');
    $this->put('/remove/{id}', 'UserFrosting\Sprinkle\Site\Controller\CartController:removeItem');
    $this->get('/figures', 'UserFrosting\Sprinkle\Site\Controller\CartController:getFigures');
    $this->put('/update/{id}', 'UserFrosting\Sprinkle\Site\Controller\CartController:updateQuantity');
})->add('authGuard');

$app->group('/cart', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CartController:getCart');
})->add('authGuard');

$app->group('/checkout', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CartController:getCheckout');
})->add('authGuard');