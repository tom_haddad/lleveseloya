<?php
$app->group('/admin', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\LYAdminController:pageDashboard')
         ->setName('dashboard');
})->add('authGuard');

$app->group('/cron', function () {
    $this->get('', 'UserFrosting\Sprinkle\Site\Controller\CronController:doAllChecks')
         ->setName('dashboard');
})->add('authGuard');