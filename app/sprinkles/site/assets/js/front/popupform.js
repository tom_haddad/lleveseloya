var PopupForm = {
    buttons: function() {
        $('body').on('click', '.ly-modal-close', function() {
            Popup.close();
        });
        $('body').on('click', '.modal-holder', function(e) {
            if (!$(e.target).closest('.ly-modal').length) {
                Popup.close();
            }
        });
    },
    init: function() {
        $('body').on('click', '.btn-form-holder', function(e) {
            if (!$(e.target).closest('.popup-form').length) {
                $('.popup-form').fadeOut();
                $(this).find('.popup-form').fadeIn(200);
            }
        });
        $('body').on('click', function(e) {
            if (!$(e.target).closest('.popup-form').length && !$(e.target).closest('.btn-form-holder').length) {
                $('.popup-form').fadeOut(200);
            }
        });
    }
};