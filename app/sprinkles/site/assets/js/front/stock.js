var Stock = {
    checkItem: function(id, addToCart, btn) {
        $.ajax({
            url: site.uri.public + "/api/products/g/" + id + "/stock",
            cache: false,
            dataType: "json",
            success: function(data) {
                if (data.available) {
                    if (data.max) {
                        Notify.add('cart', 'Maximum quantity reached', 'error');
                        btn.removeClass('btn-loading');
                    } else {
                        if (addToCart) {
                            Cart.addItem(id, btn);
                        }
                    }
                } else {
                    Stock.setSoldOut(id);
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    checkSeveralItems: function(ids, btn) {
        var data = {};

        data[site.csrf.keys.name] = site.csrf.name;
        data[site.csrf.keys.value] = site.csrf.value;

        data['ids'] = JSON.stringify(ids);

        $.ajax({
            url: site.uri.public + "/api/products/multistock",
            cache: false,
            dataType: "json",
            type: 'PUT',
            data: data,
            success: function(data) {
                var go = true;
                if (data.soldout.length > 0) {
                    go = false;
                    Notify.add('screen', 'Sorry, one or more of the items in your cart has sold out. Please confirm your selections and try again.', 'error');
                    for (var i in data.soldout) {
                        $('.cart-list li.checkout-item[data-product-id="' + i + '"]').fadeOut();
                    }
                    btn.removeClass('btn-loading');
                }

                if (data.lessquantity.length > 0) {
                    go = false;
                    Notify.add('screen', 'Sorry, one or more items in your cart is not available in this quantity. Please confirm your selections and try again.', 'error');
                    for (var i in data.lessquantity) {
                        $('.cart-list li.checkout-item[data-product-id="' + i.id + '"] .quantity').text(i.amt);
                    }
                    btn.removeClass('btn-loading');
                }

                if (go) {
                    Order.create(btn);
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    setSoldOut: function(id) {
        $('li[data-product-id="' + id + '"]').addClass('sold-out').prepend(Stock.soldOutSign);
    },
    soldOutSign: '<div class="listing-soldout"><div class="soldout-txt">SOLD OUT<br><span>Better luck next time!</span></div></div>'
};