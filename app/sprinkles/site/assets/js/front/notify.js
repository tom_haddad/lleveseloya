var Notify = {
    add: function(type, message, status) {
        var _ = this,
            newNotification = $(Notify.notification);
        switch (type) {
            case 'cart':
                var anchor = $('.notifier[data-type="cart"]');
                break;
            case 'screen':
                var anchor = $('.notifier[data-type="screen"]');
                break;
            default:
                break;
        }
        newNotification.addClass(status).text(message);
        anchor.append(newNotification);
        _.remove(newNotification);
    },
    notification: '<div class="notification"></div>',
    remove: function(notification) {
        setTimeout(function() {
            notification.fadeOut(function() {
                notification.remove();
            });
        }, 2500);
    }
}