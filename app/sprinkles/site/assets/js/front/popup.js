var Popup = {
    buttons: function() {
        $('body').on('click', '.ly-modal-close', function() {
            Popup.close();
        });
        $('body').on('click', '.modal-holder', function(e) {
            if (!$(e.target).closest('.ly-modal').length) {
                Popup.close();
            }
        });
    },
    close: function() {
        $('.body-content-holder').removeClass('fixedtop');
        $('.modal-holder').hide();
    },
    get: function(name, id) {
        var url = (id !== false) ? (name + '/' + id) : name;
        $('.modal-holder').load(site.uri.public + '/front/modal/' + url, function() {
            Site.toggleFixedBody(true);
            $('.modal-holder').show().removeClass('fadeOut').addClass('fadeIn');
            switch (name) {
                case 'login':
                    Site.loginEvents();
                    break;
                case 'success':
                    Cart.pushItem(0, false, false);
                    break;
            }
        })
    }
};