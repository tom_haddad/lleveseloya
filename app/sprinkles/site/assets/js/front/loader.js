var Loader = {
    get: function(url, nopopstate) {
        var _ = this;
        clearTimeout(_.timer);
        $('.page-loading-bar').show();
        $.ajax({
            url: site.uri.public + "/" + url,
            cache: false,
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                        $('.page-loading-bar .the-bar').css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: function(data) {
                _.timer = setTimeout(function() {
                    $('.page-loading-bar').fadeOut(function() {
                        $('.page-loading-bar .the-bar').width(0);
                    });
                }, 500)
                $('.body-loader').html(data);
                if (!nopopstate) {
                    _.state(url);
                }
                Site.onLoad();
            },
            error: function(xhr, status, text) {
                ErrorHandler.ajax(xhr, status, text);
                $('.page-loading-bar').fadeOut();
            }
        });
    },
    init: function() {
        var _ = this;
        if (window.history && window.history.pushState) {
            window.onpopstate = function(e) {
                if (_.pageCount > 0) {
                    _.popState();
                }
            }
        }
    },
    pageCount: 0,
    popState: function() {
        var url = window.location.pathname.replace('/lleveseloya/', ''),
            _ = this;
        _.get(url, true);
    },
    state: function(url) {
        var _ = this;
        if (window.history && window.history.pushState) {
            history.pushState({}, '', site.uri.public + "/" + url);
            _.pageCount++;
        }
    },
    timer: null
}