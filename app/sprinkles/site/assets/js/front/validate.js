var Validate = {
    go: function(form, data, prefix) {
        if (typeof data.errors !== 'undefined' && data.errors.length > 0) {
            for (var i = 0; i < data.errors.length; i++) {
                var cur = data.errors[i];
                if (typeof translations !== 'undefined' && cur.message in translations) {
                    if (typeof prefix == 'undefined') {
                        var prefix = "";
                    }
                    if (form.find('[name="' + prefix + cur.field + '"]').length) {
                        form.find('[name="' + prefix + cur.field + '"]').parent().addClass('has-error').append('<p>' + translations[cur.message] + '</p>');
                    } else if (form.find('[data-for="' + prefix + cur.field + '"]').length) {
                        form.find('[data-for="' + prefix + cur.field + '"]').addClass('has-error').append('<p>' + translations[cur.message] + '</p>');
                    }
                }
            }
        } else {
            console.log('nothing to validate');
        }
    },
    init: function(form) {
        form.find('input, select, textarea').on('change keyup', function() {
            $(this).parent().removeClass('has-error').find('p').remove();
        })
    }
}