var Side = {
    buttons: function() {
        // $('body').on('click', '.ly-modal-close', function() {
        //     Side.close();
        // });
        // $('body').on('click', '.modal-holder', function(e) {
        //     if (!$(e.target).closest('.ly-modal').length) {
        //         Side.close();
        //     }
        // });
    },
    close: function() {
        $('.body-content-holder').removeClass('fixedtop');
        $('.modal-holder').addClass('fadeOut');
        $('.modal-holder').fadeOut();
    },
    get: function(name, id, btn) {
        var url = (id !== false) ? (name + '/' + id) : name;
        $('.side-modal .animator').removeClass('visible');
        $('.side-modal .animator').load(site.uri.public + '/front/side/' + url, function() {
            $('.side-modal .animator').addClass('visible');
            $('.side-modal').addClass('visible');
            btn.removeClass('btn-loading');
            switch (name) {
                case 'login':
                    Site.loginEvents();
                    break;
                case 'signup':
                    Site.signupEvents();
                    break;
                case 'success':
                    Cart.pushItem(0, false, false);
                    break;
            }
            $('.js-icheck').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        })
    }
};