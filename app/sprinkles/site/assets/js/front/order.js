var Order = {
    cancel: function(order_id) {
        /* process to remove order when payment fails */
        Notify.add('screen', 'Error processing payment. Please check your details and try again.', 'error');
    },
    create: function(btn) {
        var _ = this,
            data = {};

        data[site.csrf.keys.name] = site.csrf.name;
        data[site.csrf.keys.value] = site.csrf.value;

        $.ajax({
            url: site.uri.public + "/api/orders/create",
            cache: false,
            dataType: "json",
            type: 'PUT',
            data: data,
            success: function(data) {
                btn.removeClass('btn-loading');
                if (data.worked) {
                    _.payment(data.order_id, btn);
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    payment: function(order_id, btn) {
        var _ = this,
            paymentSuccessful = true;

        /* payment process here */

        if (paymentSuccessful) {
            Loader.get('', false);
            Popup.get('success', order_id);
        } else {
            _.cancel(order_id);
        }
    },
}