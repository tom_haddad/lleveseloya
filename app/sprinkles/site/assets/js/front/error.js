var ErrorHandler = {
    ajax: function(xhr, status, text) {
        var response = xhr.responseText;
        try {
            response = $.parseJSON(response);
        } catch (e) {
            console.log(e);
        }

        if (Site.debug) {
            console.log(status);
            //console.log(response);
            console.log(text);
            console.log(xhr.getAllResponseHeaders());
        }

        Notify.add('screen', text + '. Code ' + xhr.status, 'error');
    }
}