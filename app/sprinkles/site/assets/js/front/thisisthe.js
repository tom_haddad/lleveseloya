$(document).on('ready', function() {
    Site.init();
});

var Site = {
    init: function() {
        var _ = this;
        _.buttons();
        _.onLoad();
        _.timer();
        Cart.buttons();
        Popup.buttons();
        PopupForm.init();
        Loader.init();
        if ($('#register').length) {
            Site.signupEvents();
            $('.animator').addClass('visible');
        }

        if ($('.supplier-area-skin').length) {
            Supplier.init();
        }
    },
    buttons: function() {
        $('body').on('click touchstart', '.popup-link:not(.btn-disabled)', function() {
            var link = $(this).data('popup');
            if (!(link == "" || link == false)) {
                Popup.get(link, false);
            }
        }).on('click touchstart', '.side-link:not(.btn-disabled)', function() {
            var link = $(this).data('popup'),
                btn = $(this);
            if (!(link == "" || link == false) && !$('[data-side-modal="' + link + '"]').length) {
                btn.addClass('btn-loading');
                Side.get(link, false, btn);
            }
        }).on('click touchstart', '.listing-holder .buy-product, .product-holder .buy-product', function() {
            var btn = $(this),
                item_id = $(this).closest('li').data('product-id') || $(this).closest('.product-holder').data('product-id');
            btn.addClass('btn-loading');
            Stock.checkItem(item_id, true, btn);
        }).on('click touchstart', '.listing-holder .view-product, .cart-item-holder .view-product', function() {
            var listing = $(this).closest('li'),
                link = 'product/' + listing.data('product-category') + '/' + listing.data('product-slug') + '/' + listing.data('product-id');
            Loader.get(link);
        }).on('click touchstart', '.cart-list .cart-remove', function() {
            var btn = $(this),
                cart_id = $(this).closest('li').data('id');
            btn.addClass('btn-loading');
            Cart.removeItem(cart_id, btn);
        }).on('click touchstart', '.load-page', function() {
            var link = $(this).data('url');
            Loader.get(link);
        }).on('click', '.menu-icon', function() {
            Site.menuToggle();
        }).on('click', '.profile-icon', function() {
            $('.profile-icon ul').toggle();
        });
    },
    debug: true,
    loginEvents: function() {
        function redirectOnLogin(jqXHR) {
            if (jqXHR.getResponseHeader('UF-Redirect')) {
                window.location.replace(jqXHR.getResponseHeader('UF-Redirect'));
            } else {
                window.location.replace(site.uri.public);
            }
        }

        $("#sign-in").ufForm({
            validators: page.validators.login,
            msgTarget: $("#alerts-page")
        }).on("submitSuccess.ufForm", function(event, data, textStatus, jqXHR) {
            redirectOnLogin(jqXHR);
        });
    },
    menuToggle: function() {
        var _ = this;

        _.toggleFixedBody();
        $('.hamburger').toggleClass('open');
        $('.site-menu').toggleClass('open');
    },
    onLoad: function() {
        Supplier.onLoad();
        if ($('.verify-card-num').length) {
            $('.verify-card-num').validateCreditCard(function(result) {
                if (result.card_type) {
                    var cardClass = "fa-credit-card";
                    switch (result.card_type.name) {
                        case 'amex':
                            cardClass = "fa-cc-amex";
                            break;
                        case 'dankort':
                            break;
                        case 'diners_club_carte_blanche':
                        case 'diners_club_international':
                            cardClass = "fa-cc-diners-club";
                            break;
                        case 'mastercard':
                            cardClass = "fa-cc-mastercard";
                            break;
                        case 'discover':
                            cardClass = "fa-cc-discover";
                            break;
                        case 'jcb':
                            cardClass = "fa-cc-jcb";
                            break;
                        case 'laser':
                            break;
                        case 'maestro':
                            break;
                        case 'uatp':
                            break;
                        case 'visa':
                        case 'visa_electron':
                            cardClass = "fa-cc-visa";
                            break;
                    }
                    $('.card-type-select i').attr('class', 'fa ' + cardClass);
                    if (result.valid && result.luhn_valid && result.length_valid) {
                        $('.verify-card-num').parent().addClass('has-success');
                    } else {
                        $('.verify-card-num').parent().removeClass('has-success');
                    }
                }
            });
        }
        $('.body-content-holder').removeClass('fixedtop');
        $('.hamburger').removeClass('open');
        $('.site-menu').removeClass('open');

        if ($('.datepicker').length) {
            $('.datepicker').datepicker({
                beforeShow: function() {
                    setTimeout(function() {
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                },
                dateFormat: 'dd-mm-yy'
            });
        }
    },
    signupEvents: function() {
        var pcTimer = null,
            registrationValidators = $.extend(
                true, // deep extend
                page.validators.register, {
                    rules: {
                        user_name: {
                            remote: {
                                url: site.uri.public + '/account/check-username',
                                dataType: 'text'
                            }
                        },
                        email: {
                            remote: {
                                url: site.uri.public + '/account/check-email',
                                dataType: 'text'
                            }
                        },
                        password: {
                            remote: {
                                url: site.uri.public + '/account/check-password',
                                dataType: 'text'
                            }
                        }
                    }
                }
            );

        $('input[name="passwordc"]').on('keyup blur', function(e) {
            var inp = $(this),
                val = inp.val(),
                pw = $('input[name="password"]').val(),
                errorBlock = $('<p id="f_confirm-error" class="error-block">'),
                time = (e.type == "keyup") ? 1000 : 0;

            inp.parent().removeClass('has-success has-error');
            inp.parent().find('i').remove();

            errorBlock.text(page.validators.messages.passwordc.matchFormField);
            clearTimeout(pcTimer);
            pcTimer = setTimeout(function() {
                if (val !== pw) {
                    inp.parent().addClass('has-error').removeClass('has-success');
                    errorBlock.insertAfter(inp);
                } else {
                    inp.parent().addClass('has-success').removeClass('has-error');
                }
            }, time);
        })

        // Handles form submission
        $("#register").ufForm({
            validators: registrationValidators,
            msgTarget: $("#alerts-page"),
            keyupDelay: 1500
        }).on("submitSuccess.ufForm", function() {
            // Reload to clear form and show alerts
            window.location.reload();
        });
    },
    timer: function() {
        if ($('.timer').length) {
            var _ = $('.timer'),
                now = new Date().getTime(),
                seconds = _.data('seconds'),
                toGetTo = (seconds * 1000) + now,
                x = setInterval(function() {
                    var now = new Date().getTime(),
                        distance = toGetTo - now,
                        days = Math.floor(distance / (1000 * 60 * 60 * 24)),
                        hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + (days * 24),
                        minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
                        seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    _.find('.hours').text(pad(hours, 2));
                    _.find('.minutes').text(pad(minutes, 2));
                    _.find('.seconds').text(pad(seconds, 2));

                    if (distance <= 0) {
                        clearInterval(x);
                        location.reload();
                    }
                }, 20);
        }
    },
    toggleFixedBody() {
        if (!$('.body-content-holder').hasClass('fixedtop')) {
            var oldscroll = $(document).scrollTop();
            $('.body-content-holder').addClass('fixedtop').css('top', -oldscroll + 'px');
        } else {
            var oldscroll = parseInt($('.body-content-holder').css('top'));
            $('.body-content-holder').removeClass('fixedtop').css('top', 0);
            $(document).scrollTop(-oldscroll);
        }
    }
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

function executeFunctionByName(functionName, context, args) {
    var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}