var Cart = {
    addItem: function(id, btn) {
        var data = {};

        data[site.csrf.keys.name] = site.csrf.name;
        data[site.csrf.keys.value] = site.csrf.value;

        $.ajax({
            url: site.uri.public + "/api/cart/add/" + id,
            cache: false,
            dataType: "json",
            type: 'PUT',
            data: data,
            success: function(data) {
                btn.removeClass('btn-loading');
                if (data.worked) {
                    Cart.pushItem(data.count, data.action);
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    buttons: function() {
        var _ = this;
        $('body').on('click', '.cart-list .submit-popup-form', function() {
            var action = $(this).data('action'),
                btn = $(this),
                form = $(this).closest('.popup-form-content');
            $(this).addClass('btn-loading');
            switch (action) {
                case 'quantity':
                    _.updateQuantity(form, btn);
                    break;
            }
        });
        $('body').on('click', '.process-order', function() {
            var btn = $(this),
                ids = [];

            $('.cart-list li.checkout-item').each(function() {
                ids.push($(this).data('product-id'));
            });

            btn.addClass('btn-loading');

            Stock.checkSeveralItems(ids, btn);
        });
    },
    pushItem: function(count, action, btn) {
        var msg = '';

        switch (action) {
            case 'added':
                msg = 'Item added to cart';
                break;
            case 'updated':
                msg = 'Item quantity updated';
                break;
            case 'remove':
                msg = 'Item removed';
                break;
            default:
                msg = false;
                break;
        }

        if (msg !== false) {
            Notify.add('cart', msg, 'ok');
        }

        if (count > 0) {
            $('header .cart-count').show().text(count);
        } else {
            $('header .cart-count').hide();
        }

        if ($('.cart-list').length) {
            $.ajax({
                url: site.uri.public + "/api/cart/figures/",
                cache: false,
                dataType: "json",
                type: 'GET',
                success: function(data) {
                    if (btn !== false) {
                        btn.removeClass('btn-loading');
                    }
                    Cart.setFigures(data);
                },
                error: function(xhr, status, text) {
                    if (btn !== false) {
                        btn.removeClass('btn-loading');
                    }
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        }
    },
    removeItem: function(id, btn) {
        var data = {};

        data[site.csrf.keys.name] = site.csrf.name;
        data[site.csrf.keys.value] = site.csrf.value;

        $.ajax({
            url: site.uri.public + "/api/cart/remove/" + id,
            cache: false,
            dataType: "json",
            type: 'PUT',
            data: data,
            success: function(data) {
                btn.removeClass('btn-loading');
                if (data.worked) {
                    $('.cart-list li[data-id="' + id + '"]').fadeOut(function() {
                        $(this).remove();
                    });
                    Cart.pushItem(data.count, 'remove', btn);
                    if (data.count == 0) {
                        Loader.get('', false);
                    }
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    setFigures: function(data) {
        $('.cart-bottom .cart-st b').text(data.subtotal);
        $('.cart-bottom .cart-d b').text(data.discount);
        $('.cart-bottom .cart-tt b').text(data.tax);
        $('.cart-t b').text(data.total);
    },
    updateQuantity: function(form, btn) {
        var fd = form.serializeArray(),
            action = $(this).data('action'),
            id = form.data('item-id'),
            data = {},
            _ = this;

        data[site.csrf.keys.name] = site.csrf.name;
        data[site.csrf.keys.value] = site.csrf.value;

        $.each(fd, function(key, input) {
            data[input.name] = input.value;
        });

        $.ajax({
            url: site.uri.public + "/api/cart/update/" + id,
            cache: false,
            dataType: "json",
            type: 'PUT',
            data: data,
            success: function(data) {
                _.pushItem(data.count, data.action, btn);
            },
            error: function(xhr, status, text) {
                ErrorHandler.ajax(xhr, status, text);
            }
        });

    }
};