var Supplier = {
    buttons: function() {
        var _ = this;
        $('a.photo-click').unbind('click');
        $('a.photo-click').on('click', function(e) {
            e.stopPropagation();
            $('form.dropzone').click();
        });
        $('a.variant-click').unbind('click');
        $('a.variant-click').on('click', function(e) {
            e.stopPropagation();
            if (!$('.variant-area').hasClass('variants-initialised')) {
                _.variants.init();
            } else {
                $(this).text('Add Variants');
                _.variants.cancel();
            }
        });
        $('a.save-product').unbind('click');
        $('a.save-product').on('click', function(e) {
            e.stopPropagation();
            if (!_.calculating) {
                _.saveProduct($(this));
            } else {
                Notify.add('screen', 'Please wait, updating pricing section.', 'error');
            }
        });
        $('a.save-variant').unbind('click');
        $('a.save-variant').on('click', function(e) {
            e.stopPropagation();
            _.saveVariant($(this));
        });
        $('a.mark-shipped').unbind('click');
        $('a.mark-shipped').on('click', function(e) {
            e.stopPropagation();
            _.markShipped($(this));
        });
        $('.modal-holder').on('click', '.edit-photo', function(e) {
            e.stopPropagation();
            e.preventDefault();
            var form = $(this).closest('form');
            _.editPhoto(form);
        });

        $('.site-menu li').unbind('click');
        $('.site-menu li').on('click', function() {
            if ($(this).find('ul').length) {
                $(this).find('ul').show();
            }
        })
    },
    calculateFees: function() {
        var _ = this,
            action = $('form.product_data').data('fee'),
            feeArea = $('.fee-calculation'),
            productData = $('form.product_data').serialize(),
            theForm = $('.supplier-area-form');

        _.calculating = true;

        $.ajax({
            type: "POST",
            url: action,
            data: productData,
            success: function(data) {
                data = JSON.parse(data);
                _.calculating = false;
                if (data.worked) {
                    if (parseInt(data.featured_fee) > 0) {
                        feeArea.find('[data-row="featured"]').show().find('span').text('₡' + data.featured_fee);
                    } else {
                        feeArea.find('[data-row="featured"]').hide();
                    }
                    if (parseInt(data.admin_fee) > 0) {
                        feeArea.find('[data-row="admin"]').show().find('span').text('₡' + data.admin_fee);
                    } else {
                        feeArea.find('[data-row="admin"]').hide();
                    }
                    feeArea.find('[data-row="customer"]').find('span').text('₡' + data.customer);
                }
            },
            error: function(xhr, status, text) {
                _.calculating = false;
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    calculating: false,
    categories: function() {
        $('.body-loader').on('change', 'select[name="category_id"], select[name="subcategory_id"]', function() {
            $(this).parent().nextAll('.supplier-row:not(.load-subcategories)').remove();
            var isSub = 0,
                val = $(this).val();
            if ($(this).attr('name') == "subcategory_id") {
                isSub = 1;
                $('input[name="pd_subcategory_id"]').val(val);
            }
            $.ajax({
                url: site.uri.public + "/actions/check-subs/" + isSub + "/" + val,
                cache: false,
                success: function(data) {
                    if (data !== "") {
                        var holder = $('.load-subcategories'),
                            length = $('.category-holder .supplier-row').length - 2,
                            toPrepend = '',
                            newOne = holder.clone();

                        for (var i = 0; i < length; i++) {
                            toPrepend += '> ';
                        }
                        holder.html(data).removeClass('load-subcategories').find('option:not(:first-child)').prepend(toPrepend);
                        newOne.insertAfter(holder);
                    }
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                    $('.page-loading-bar').fadeOut();
                }
            });
        });
    },
    editPhoto: function(theForm) {
        var action = theForm.attr('action'),
            productData = theForm.serialize();

        $.ajax({
            type: "POST",
            url: action,
            data: productData,
            success: function(data) {
                $('.modal-holder').fadeOut(function() {
                    $('.modal-holder').html('');
                    Notify.add('screen', 'Photo updated', 'success');
                })

            },
            error: function(xhr, status, text) {
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    init: function() {
        var _ = this;
        _.categories();
        _.onLoad();
    },
    images: [],
    imgGrid: function() {
        var _ = this;
        $('.img-grid').unbind('click');
        $('.img-grid').on('click', '.img-zoom', function() {
            var id = $(this).data('id');
            $.ajax({
                url: site.uri.public + "/get_image_modal/" + id,
                cache: false,
                success: function(data) {
                    $('.modal-holder').html(data).addClass('img-holder').fadeIn();
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        });
        $('.img-grid').on('click', '.img-remove', function() {
            if ($('.img-grid .image-grid-btn').length > 1) {
                if ($(this).data('product-img-id')) {
                    var id = $(this).data('id');
                } else {
                    var id = $(this).data('id');
                }
                $(this).parent().parent().fadeOut(function() {
                    $(this).remove();
                })
                _.images = $.grep(_.images, function(value) {
                    return value != id;
                });
                $('input[name="photos"]').val(JSON.stringify(_.images));
            } else {
                Notify.add('screen', 'You must have at least one photo for your product.', 'error');
            }
        });
        $('.img-grid').on('click', '.img-edit-details', function() {
            if ($(this).data('product-img-id')) {
                var id = "product-" + $(this).data('product-img-id');
            } else {
                var id = $(this).data('id');
            }
            $('.modal-holder').addClass('img-holder');
            $.ajax({
                url: site.uri.public + "/front/modal/image/" + id,
                cache: false,
                success: function(data) {
                    $('.modal-holder').html(data).addClass('img-holder').fadeIn();
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        });
    },
    keyUps: function() {
        var _ = this,
            accepted = ['name', 'featured', 'description', 'category_id', 'subcategory_id', 'price', 'discount_price', 'total_quantity', 'sku', 'barcode'],
            toDoFee = ['pd_featured', 'pd_price', 'pd_discount_price', 'pd_category_id', 'pd_subcategory_id'];

        if ($('.supplier-area-form').hasClass('variant-form')) {
            accepted.push('quantity');
            $('.options input').each(function() {
                accepted.push($(this).attr('name'));
            });
        }
        $('.supplier-area-form input, .supplier-area-form textarea, .supplier-area-form select').each(function() {
            if ($.inArray($(this).attr('name'), accepted) >= 0) {
                $(this).unbind('keyup change');
                $(this).on('keyup change', function(e) {
                    var name = $(this).attr('name').replace('total_', '');
                    if ($('.supplier-area-form').hasClass('variant-form')) {
                        name = "vd_" + name;
                    } else {
                        name = "pd_" + name;
                    }
                    if (name == 'pd_featured') {
                        if ($(this).is(':checked')) {
                            $('input[name="' + name + '"]').val("1").trigger('change');
                        } else {
                            $('input[name="' + name + '"]').val("0").trigger('change');
                        }
                    } else {
                        switch (name) {
                            case 'pd_price':
                            case 'pd_discount_price':
                                var charCode = (e.which) ? e.which : e.keyCode;
                                if ($(this).val().length > 0) {
                                    var selection = window.getSelection().toString();
                                    if (selection !== '') {
                                        return;
                                    }

                                    if ($.inArray(e.keyCode, [38, 40, 37, 39, 190, 188]) !== -1) {
                                        return;
                                    }

                                    var input = $(this).val().replace(/[^0-9.]+/g, "");

                                    input = input ? parseFloat(input, 10) : 0;

                                    $(this).val(function() {
                                        return (input === 0) ? "" : input.toLocaleString("en-US");
                                    });
                                    $('input[name="' + name + '"]').val($(this).val().replace(/\D/g, '')).trigger('change');
                                }
                                break;
                            default:
                                $('input[name="' + name + '"]').val($(this).val()).trigger('change');
                                break;
                        }
                    }
                });
            }
        });
        $('form.product_data input').on('change', function() {
            if ($.inArray($(this).attr('name'), toDoFee) >= 0) {
                var go = true;
                for (var i = 0; i < toDoFee.length; i++) {
                    switch (toDoFee[i]) {
                        case 'pd_featured':
                        case 'pd_subcategory_id':
                            break;
                        default:
                            if ($('form.product_data input[name="' + toDoFee[i] + '"]').val().length > 0) {

                            } else {
                                go = false;
                            }
                            break;
                    }
                }

                if (go && !_.calculating) {
                    _.calculateFees();
                }

            }
        });
    },
    markShipped: function(btn) {
        var action = $('form.order_data').attr('action'),
            productData = $('form.order_data').serialize();

        btn.addClass('btn-loading');

        $.ajax({
            type: "POST",
            url: action,
            data: productData,
            success: function(data) {
                data = JSON.parse(data);
                btn.removeClass('btn-loading');
                if (data.worked) {
                    Loader.get('supplier/orders/' + data.order_id);
                    Notify.add('screen', 'Order marked as shipped', 'success');
                } else {
                    Notify.add('screen', 'There has been an error, please try again', 'error');
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    onLoad: function() {
        var _ = this;
        _.buttons();
        _.imgGrid();
        _.keyUps();
        if ($('.supplier-product-table-holder').length) {
            _.productTable();
        }
        Validate.init($('.supplier-area-form'));
        if ($('#product-photo-uploader').length) {
            var maxFiles = 12;
            $('#product-photo-uploader').dropzone({
                acceptedFiles: 'image/*',
                autoProcessQueue: true,
                createImageThumbnails: true,
                dictDefaultMessage: 'Drop files here to upload',
                maxFiles: maxFiles,
                parallelUploads: maxFiles,
                thumbnailWidth: 140,
                init: function() {
                    this.on('success', function(file, response) {
                        $('[data-for="photos"]').removeClass('has-error').find('p').remove();
                        response = JSON.parse(response);
                        _.images.push(response.id);
                        $(file.previewElement).remove();
                        $(response.html).insertAfter('.img-grid form .dz-message');
                        $('input[name="photos"]').val(JSON.stringify(_.images));
                    }).on('queuecomplete', function(file, response) {
                        $('input[name="images"]').val(_.images);
                    });
                }
            });
        }
        if ($('.variant-image').length) {
            var maxFiles = 1;
            $('.variant-image').dropzone({
                acceptedFiles: 'image/*',
                autoProcessQueue: true,
                createImageThumbnails: true,
                dictDefaultMessage: 'Upload Image',
                maxFiles: maxFiles,
                thumbnailWidth: 140,
                init: function() {
                    this.on('success', function(file, response) {
                        $('.variant-list ul li.cur a').click();
                    });
                }
            });
        }
        if ($('textarea.html_editor').length) {
            tinymce.remove();
            tinymce.init({
                selector: 'textarea.html_editor',
                height: 150,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help'
                ],
                toolbar: 'undo redo | bold italic underline | forecolor link | fontsizeselect | bullist numlist | removeformat',
                setup: function(ed) {
                    ed.on('keyup', function(e) {
                        var name = "pd_description";
                        $('input[name="' + name + '"]').val(ed.getContent());
                        $('.wmce').removeClass('has-error').find('> p').remove();
                    });
                    ed.on('change', function(e) {
                        var name = "pd_description";
                        $('input[name="' + name + '"]').val(ed.getContent());
                        $('.wmce').removeClass('has-error').find('> p').remove();
                    });
                }
            });
        }

        if ($('.variant-table').length) {
            _.variants.updateValues();
            $('.variant-table tr').each(function() {
                $(this).on('keyup', 'input[name="quantity"], input[name="v_sku"], input[name="v_barcode"]', function() {
                    _.variants.updateValues();
                });
                $(this).on('change', 'input[type="checkbox"]', function() {
                    _.variants.updateValues();
                });
                $(this).on('click', '.fa-trash', function() {
                    $(this).parent().parent().addClass('deleting');
                    $(this).addClass('fa-undo').removeClass('fa-trash');
                    _.variants.updateValues();
                });
                $(this).on('click', '.fa-undo', function() {
                    $(this).parent().parent().removeClass('deleting');
                    $(this).removeClass('fa-undo').addClass('fa-trash');
                    _.variants.updateValues();
                });
            })
        }

        if ($('.img-grid .image-grid-btn').length) {
            _.images = [];
            $('.image-grid-btn').each(function() {
                var id = $(this).find('.img-zoom').data('id');
                _.images.push(id);
            });
            $('input[name="photos"]').val(JSON.stringify(_.images));

        }
    },
    productTable: function() {
        var _ = this,
            base = site.uri.public + "/supplier/products/table";

        if ($('.product-table').hasClass('inventory-table')) {
            base = site.uri.public + "/supplier/products/inventory/table";
        } else if ($('.product-table').hasClass('order-table')) {
            base = site.uri.public + "/supplier/orders/table";
        }
        $.ajax({
            url: base,
            cache: false,
            success: function(data) {
                $('.product-table').removeClass('iamloading');
                $('.supplier-product-table-holder').removeClass('spinner').html(data);
                _.productTablePagination();
            },
            error: function(xhr, status, text) {
                ErrorHandler.ajax(xhr, status, text);
            }
        });
        $('.product-table thead td:not(.nosort)').unbind('click');
        $('.product-table thead td:not(.nosort)').on('click', function() {
            var column = $(this).attr('class');
            if ($(this).data('order')) {
                if ($(this).data('order') == "asc") {
                    $(this).data('order', 'desc');
                } else {
                    $(this).data('order', 'asc');
                }
            } else {
                $(this).data('order', 'desc');
            }
            var order = $(this).data('order');
            $('.product-table').addClass('iamloading');
            $('.supplier-product-table-holder').html('').addClass('spinner');
            $.ajax({
                url: base + "?" + column + "=" + order,
                cache: false,
                success: function(data) {
                    $('.product-table').removeClass('iamloading');
                    $('.supplier-product-table-holder').removeClass('spinner').html(data);
                    _.productTablePagination();
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        });
        $('input[name="table-search"]').unbind('keyup');
        $('input[name="table-search"]').on('keyup', function() {
            var value = $(this).val(),
                url = base + "?search=" + value;
            clearTimeout(_.productTableTimer);
            $('.product-table').addClass('iamloading');
            $('.supplier-product-table-holder').html('').addClass('spinner');
            $('.orderdates').each(function() {
                if ($(this).val() !== "") {
                    url += "&" + $(this).attr('name') + "=" + $(this).val();
                }
            });
            _.productTableTimer = setTimeout(function() {
                $.ajax({
                    url: url,
                    cache: false,
                    success: function(data) {
                        $('.product-table').removeClass('iamloading');
                        $('.supplier-product-table-holder').removeClass('spinner').html(data);
                        _.productTablePagination();
                    },
                    error: function(xhr, status, text) {
                        ErrorHandler.ajax(xhr, status, text);
                    }
                });
            }, 400);
        });
        $('.orderdates').unbind('change');
        $('.orderdates').on('change', function() {
            var value = $(this).val(),
                url = base + "?yes=1";
            $('.product-table').addClass('iamloading');
            $('.supplier-product-table-holder').html('').addClass('spinner');
            $('.table-search input').each(function() {
                if ($(this).val() !== "") {
                    url += "&" + $(this).attr('name') + "=" + $(this).val();
                }
            });
            _.productTableTimer = setTimeout(function() {
                $.ajax({
                    url: url,
                    cache: false,
                    success: function(data) {
                        $('.product-table').removeClass('iamloading');
                        $('.supplier-product-table-holder').removeClass('spinner').html(data);
                        _.productTablePagination();
                    },
                    error: function(xhr, status, text) {
                        ErrorHandler.ajax(xhr, status, text);
                    }
                });
            }, 400);
        });
        $('.table-pagination .fa').unbind('click');
        $('.table-pagination .fa').on('click', function() {
            var field = $('input[name="field"]').val(),
                page = $(this).attr('data-page'),
                value = $('input[name="value"]').val(),
                url = site.uri.public + "/supplier/products/table?go=yes";


            if ($('.product-table').hasClass('inventory-table')) {
                url = site.uri.public + "/supplier/products/inventory/table?go=yes";
            } else if ($('.product-table').hasClass('order-table')) {
                url = site.uri.public + "/supplier/orders/table?go=yes";
            }

            if (page > 0) {
                $('.product-table').addClass('iamloading');
                $('.supplier-product-table-holder').html('').addClass('spinner');

                if (field && value) {
                    url += "&" + field + "=" + value;
                }

                url += "&page=" + page;
                $.ajax({
                    url: url,
                    cache: false,
                    success: function(data) {
                        $('.product-table').removeClass('iamloading');
                        $('.supplier-product-table-holder').removeClass('spinner').html(data);
                        _.productTablePagination();
                    },
                    error: function(xhr, status, text) {
                        ErrorHandler.ajax(xhr, status, text);
                    }
                });
            }
        })
    },
    productTablePagination: function() {
        var cur = $('input[name="cur"]').val(),
            next = $('input[name="next"]').val(),
            prev = $('input[name="prev"]').val(),
            total = $('input[name="total"]').val();

        if (total > 1) {
            $('.table-pagination').show();
            $('.table-pagination span').text(cur + ' / ' + total);
            if (prev > 0) {
                $('.table-pagination .fa-chevron-left').attr('data-page', prev).removeClass('no');
            } else {
                $('.table-pagination .fa-chevron-left').attr('data-page', 0).addClass('no');
            }
            if (next > 0) {
                $('.table-pagination .fa-chevron-right').attr('data-page', next).removeClass('no');
            } else {
                $('.table-pagination .fa-chevron-right').attr('data-page', 0).addClass('no');
            }
        } else {
            $('.table-pagination').hide();
        }
        if ($('.product-table').hasClass('inventory-table')) {
            $('.inventory-table td.update').on('click', ' .button', function(e) {
                var theForm = $(this).parent();
                e.stopPropagation();
                e.preventDefault();

                switch (true) {
                    case $(this).hasClass('add'):
                        theForm.find('input[name="newQuantity"]').val(0);
                        theForm.parent().parent().find('.addition').removeClass('vis');
                        theForm.find('input[type="number"]').val(0);
                        theForm.find('.set').removeClass('btn-grey').addClass('btn-highlight');
                        $(this).addClass('btn-grey').removeClass('btn-highlight');
                        break;
                    case $(this).hasClass('set'):
                        theForm.find('input[name="newQuantity"]').val(0);
                        theForm.parent().parent().find('.addition').removeClass('vis');
                        theForm.find('input[type="number"]').val(0);
                        theForm.find('.add').removeClass('btn-grey').addClass('btn-highlight');
                        $(this).addClass('btn-grey').removeClass('btn-highlight');
                        break;
                    case $(this).hasClass('save'):
                        $(this).addClass('btn-loading');
                        var action = theForm.attr('action'),
                            btn = $(this),
                            data = theForm.serialize()
                        $.ajax({
                            type: "POST",
                            url: action,
                            data: data,
                            success: function(data) {
                                data = JSON.parse(data);
                                btn.removeClass('btn-loading');
                                if (data.worked) {
                                    Notify.add('screen', 'Quantity updated', 'success');
                                    theForm.parent().parent().find('.val').text(data.quantity);
                                    theForm.parent().parent().find('.addition').removeClass('vis');
                                    theForm.find('input[name="oldQuantity"]').val(data.quantity);
                                    theForm.find('input[name="newQuantity"]').val(0);
                                    theForm.parent().parent().find('.addition').removeClass('vis');
                                    theForm.find('input[type="number"]').val(0);
                                    theForm.find('.set').removeClass('btn-grey').addClass('btn-highlight');
                                    theForm.find('.add').addClass('btn-grey').removeClass('btn-highlight');
                                } else {
                                    Validate.go(theForm, data);
                                    Notify.add('screen', 'Error updating quantity.', 'error');
                                }
                            },
                            error: function(xhr, status, text) {
                                btn.removeClass('btn-loading');
                                ErrorHandler.ajax(xhr, status, text);
                            }
                        });
                        break;
                }
            });
            $('.inventory-table td.update').on('change keyup mouseup', 'input[type="number"]', function(e) {
                var adding = true,
                    theForm = $(this).parent(),
                    theNew = theForm.find('input[name="newQuantity"]'),
                    theOld = theForm.find('input[name="oldQuantity"]'),
                    theTR = theForm.parent().parent(),
                    val = parseInt($(this).val());

                if (theForm.find('.set').hasClass('btn-grey')) {
                    adding = false;
                }

                if (adding) {
                    if (val > 0 || val < 0) {
                        var newVal = parseInt(theOld.val()) + parseInt(val);
                        theTR.find('.addition').addClass('vis').find('b').text(newVal);
                        theNew.val(newVal);
                    } else {
                        theTR.find('.addition').removeClass('vis');
                    }
                } else {
                    if (val >= 0) {
                        var newVal = val;
                        theTR.find('.addition').addClass('vis').find('b').text(val);
                        theNew.val(newVal);
                    } else {
                        theTR.find('.addition').removeClass('vis');
                        $(this).val(0);
                    }
                }
            });
        }
    },
    productTableTimer: null,
    saveProduct: function(btn) {
        var action = $('form.product_data').attr('action'),
            productData = $('form.product_data').serialize(),
            theForm = $('.supplier-area-form');

        $('.has-error').removeClass('has-error').find('p').remove();

        btn.addClass('btn-loading');

        $.ajax({
            type: "POST",
            url: action,
            data: productData,
            success: function(data) {
                data = JSON.parse(data);
                btn.removeClass('btn-loading');
                if (data.worked) {
                    //Loader.get('supplier/products/' + data.product_id);
                    Loader.get('supplier/products');
                    if (data.approved == 0) {
                        Notify.add('screen', 'Product ' + ((data.updating == 1) ? 'updated' : 'created succesfully') + ', awaiting admin approval', 'error');
                    } else {
                        if (data.updating == 1) {
                            Notify.add('screen', 'Product updated', 'success');
                        } else {
                            Notify.add('screen', 'Product created successfully', 'success');
                        }
                    }
                } else {
                    Validate.go(theForm, data);
                    if (data.disabled) {
                        Notify.add('screen', 'Product already submitted, please contact an administrator to make amendments', 'error');
                    } else {
                        Notify.add('screen', 'Error adding product, please check all required fields have been filled in', 'error');
                    }
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    saveVariant: function(btn) {
        var action = $('form.variant_data').attr('action'),
            productData = $('form.variant_data').serialize(),
            theForm = $('.variant-form');

        $('.has-error').removeClass('has-error').find('p').remove();

        btn.addClass('btn-loading');

        $.ajax({
            type: "POST",
            url: action,
            data: productData,
            success: function(data) {
                data = JSON.parse(data);
                btn.removeClass('btn-loading');
                if (data.worked) {
                    Loader.get('supplier/products/' + data.product_id + '/variants/' + data.variant_id);
                    //Loader.get('supplier/products/');
                    if (data.updating == 1) {
                        Notify.add('screen', 'Variant updated', 'success');
                    } else {
                        Notify.add('screen', 'Variant created successfully', 'success');
                    }
                } else {
                    Validate.go(theForm, data);
                    Notify.add('screen', 'Error adding variant, please check all required fields have been filled in', 'error');
                }
            },
            error: function(xhr, status, text) {
                btn.removeClass('btn-loading');
                ErrorHandler.ajax(xhr, status, text);
            }
        });
    },
    variants: {
        addTag: function(obj, val) {
            var _ = this,
                list = obj.find('ul.tag-holder'),
                newTag = $('<li class="variant-tag">'),
                removeBtn = $('<i class="fa fa-close remove-tag">');
            val = $.trim(val);
            if (val !== "") {
                if (obj.find('li:textEquals(' + val + ')').length) {
                    Notify.add('screen', 'This variant value already exists', 'error');
                } else {
                    newTag.text(val).append(removeBtn);
                    newTag.appendTo(list);
                    newTag.on('click', 'i', function() {
                        var tag = $(this).parent();
                        _.removeTag(tag);
                    })
                    obj.find('.tag-input').val('');
                    _.update();
                }
            }
        },
        addVariant: function() {
            var _ = this;
            $.ajax({
                url: site.uri.public + "/supplier/products/new/variant",
                cache: false,
                success: function(data) {
                    $('.variant-area .add-variant').before(data);
                    if ($('.new-variant').length >= 3) {
                        $('.add-variant').hide();
                    } else {
                        $('.add-variant').show();
                    }
                    if ($('.new-variant').length > 1) {
                        $('.variant-trash').show();
                    } else {
                        $('.variant-trash').hide();
                    }
                    _.checkTags();
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        },
        cancel: function() {
            $('.variant-area').removeClass('variants-initialised').html('');
        },
        checkTags: function() {
            var _ = this;
            if ($('.tag-area:not(.tag-initialised)').length) {
                $('.tag-area:not(.tag-initialised)').each(function() {
                    _.initTags($(this));
                });
            }
        },
        drawTable: function() {
            var _ = this;
            $('.variant-table tbody tr:not(.cloneme)').remove();
            if (_.variantRows !== null && _.variantRows.length > 0) {
                $('.variant-table').show();
                for (r in _.variantRows) {
                    var rowDOM = $('tr.cloneme').clone(),
                        row = _.variantRows[r];
                    rowDOM.removeClass('cloneme');
                    for (var k in row) {
                        var span = $('<span class="' + k + '">');
                        span.text(row[k]);
                        rowDOM.find('.variant-title').append(span);
                        rowDOM.attr('data-' + k, row[k]);
                    }
                    rowDOM.on('keyup', 'input[name="quantity"], input[name="v_sku"], input[name="v_barcode"]', function() {
                        _.updateValues();
                    });
                    rowDOM.on('change', 'input[type="checkbox"]', function() {
                        _.updateValues();
                    });
                    $('.variant-table tbody').append(rowDOM);
                }
            } else {
                $('.variant-table').hide();
            }
            _.updateValues();
        },
        init: function() {
            var _ = this;
            $.ajax({
                url: site.uri.public + "/supplier/products/new/variants",
                cache: false,
                success: function(data) {
                    $('.variant-area').addClass('variants-initialised').html(data);
                    $('a.variant-click').text('Cancel');
                    $('.add-variant').on('click', function() {
                        _.addVariant();
                    });
                    $('.variants-holder').on('click', '.variant-trash', function() {
                        var obj = $(this).parent();
                        _.removeVariant(obj);
                    })
                    _.checkTags();
                },
                error: function(xhr, status, text) {
                    ErrorHandler.ajax(xhr, status, text);
                }
            });
        },
        initTags: function(obj) {
            var _ = this;
            obj.addClass('tag-initialised');
            obj.on('keypress', '.tag-input', function(e) {
                var val = $(this).val();
                val = capitalize(val);
                $(this).val(val);
                if (e.keyCode == 188 || e.keyCode == 13 || e.keyCode == 44 || e.keyCode == 190) {
                    e.preventDefault();

                    _.addTag(obj, val);
                }
            });
            obj.parent().parent().on('keyup', '.variant-name input', function(e) {
                var val = $(this).val();
                val = capitalize(val);
                $(this).val(val);
                _.update();
            });
        },
        removeTag: function(obj) {
            var _ = this;
            obj.remove();
            _.update();
        },
        removeVariant: function(obj) {
            var _ = this;
            obj.remove();
            if ($('.new-variant').length < 3) {
                $('.add-variant').show();
            }
            if ($('.new-variant').length > 1) {
                $('.variant-trash').show();
            } else {
                $('.variant-trash').hide();
            }
            _.update();
        },
        update: function() {
            var _ = this,
                returner = [];
            $('.new-variant').each(function() {
                var name = $(this).find('.variant-name input').val(),
                    options = [];
                $(this).find('.variant-tag').each(function() {
                    var val = $(this).text();
                    options.push(val);
                });
                returner.push({
                    'name': name,
                    'options': options
                });
            });
            if (returner.length > 0) {
                _.variantData = returner;
                returner = JSON.stringify(returner);
                $('input[name="variants"]').val(returner);
            } else {
                _.variantData = null;
                $('input[name="variants"]').val("");
            }
            _.updateTable();
        },
        updateTable: function() {
            var _ = this,
                options = [],
                rows = [];

            if (_.variantData !== null) {
                for (var i = 0; i < _.variantData.length; i++) {
                    var variant = _.variantData[i];
                    options[i] = [];
                    for (var j = 0; j < variant.options.length; j++) {
                        var option = variant.options[j];
                        options[i].push(option);
                    }
                }
            }

            if (typeof options[0] !== 'undefined' && options[0].length > 0) {
                for (var i = 0; i < options[0].length; i++) {
                    if (typeof options[1] !== 'undefined' && options[1].length > 0) {
                        for (var j = 0; j < options[1].length; j++) {
                            if (typeof options[2] !== 'undefined' && options[2].length > 0) {
                                for (var k = 0; k < options[2].length; k++) {
                                    rows.push({
                                        'option_1': options[0][i],
                                        'option_2': options[1][j],
                                        'option_3': options[2][k]
                                    });
                                }
                            } else {
                                rows.push({
                                    'option_1': options[0][i],
                                    'option_2': options[1][j]
                                });
                            }
                        }
                    } else {
                        rows.push({
                            'option_1': options[0][i]
                        });
                    }
                }
            }
            _.variantRows = rows;
            _.drawTable();
        },
        updateValues: function() {
            var _ = this,
                option_1 = false,
                option_2 = false,
                option_3 = false,
                values = [];

            if ($('.variant-table tr:not(.cloneme)').length > 0) {
                if ($('.new-variant').length) {
                    for (var i = 0; i < $('.new-variant').length; i++) {
                        if (i == 0) {
                            option_1 = $('.new-variant').eq(i).find('input[name="variant_name"]').val();
                        } else if (i == 1) {
                            option_2 = $('.new-variant').eq(i).find('input[name="variant_name"]').val();
                        } else if (i == 2) {
                            option_3 = $('.new-variant').eq(i).find('input[name="variant_name"]').val();
                        }
                    }
                } else {
                    if ($('input[name="variant_option_1"]').length) {
                        option_1 = $('input[name="variant_option_1"]').val();
                    }
                    if ($('input[name="variant_option_2"]').length) {
                        option_2 = $('input[name="variant_option_2"]').val();
                    }
                    if ($('input[name="variant_option_3"]').length) {
                        option_3 = $('input[name="variant_option_3"]').val();
                    }
                }
                $('.variant-table tr:not(.cloneme):not(.deleting)').each(function() {
                    if ($(this).find('input[type="checkbox"]').prop('checked') == true) {
                        var nameArray = [],
                            barcode = $(this).find('input[name="v_barcode"]').val(),
                            sku = $(this).find('input[name="v_sku"]').val(),
                            quantity = $(this).find('input[name="quantity"]').val();

                        if (typeof $(this).data('option_1') !== "undefined") {
                            nameArray.push({
                                'type': option_1,
                                'name': $(this).data('option_1')
                            });
                        }
                        if (typeof $(this).data('option_2') !== "undefined") {
                            nameArray.push({
                                'type': option_2,
                                'name': $(this).data('option_2')
                            });
                        }
                        if (typeof $(this).data('option_3') !== "undefined") {
                            nameArray.push({
                                'type': option_3,
                                'name': $(this).data('option_3')
                            });
                        }

                        values.push({
                            'data': nameArray,
                            'barcode': barcode,
                            'sku': sku,
                            'quantity': quantity
                        });
                    }
                });
                $('input[name="variant_detail"]').val(JSON.stringify(values));
                _.variantValues = values;
            } else {
                $('input[name="variant_detail"]').val('');
                _.variantValues = null;
            }
        },
        variantData: null,
        variantRows: null,
        variantValues: null
    }
}

$.expr[':'].textEquals = $.expr.createPseudo(function(arg) {
    return function(elem) {
        return $(elem).text().match("^" + arg + "$");
    };
});

function capitalize(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}