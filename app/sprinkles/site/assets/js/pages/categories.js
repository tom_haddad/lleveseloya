/**
 * Page-specific Javascript file.  Should generally be included as a separate asset bundle in your page template.
 * example: {{ assets.js('js/pages/sign-in-or-register') | raw }}
 *
 * This script depends on widgets/categories.js, uf-table.js, moment.js, handlebars-helpers.js
 *
 * Target page: /categories
 */

$(document).ready(function() {
    $("#widget-categories").ufMerjeTable({
        dataUrl: site.uri.public + "/api/categories"
    });

    // Bind creation button
    bindGroupCreationButton($("#widget-categories"));

    // Bind table buttons
    $("#widget-categories").on("pagerComplete.ufMerjeTable", function() {
        bindGroupButtons($(this));
    });

    bindGroupButtons($('#view-category'));
});