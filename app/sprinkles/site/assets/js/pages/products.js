/**
 * Page-specific Javascript file.  Should generally be included as a separate asset bundle in your page template.
 * example: {{ assets.js('js/pages/sign-in-or-register') | raw }}
 *
 * This script depends on widgets/products.js, uf-table.js, moment.js, handlebars-helpers.js
 *
 * Target page: /products
 */

$(document).ready(function() {
    $("#widget-products").ufTable({
        dataUrl: site.uri.public + "/api/products"
    });

    // Bind creation button
    bindGroupCreationButton($("#widget-products"));

    // Bind table buttons
    $("#widget-products").on("pagerComplete.ufTable", function() {
        bindGroupButtons($(this));
        doMultipleSelect($(this));
    });

    bindGroupButtons($('#view-product'));
    if ($("#widget-product-variants").length) {
        var product_id = $("#widget-product-variants").data('id');
        $("#widget-product-variants").ufTable({
            dataUrl: site.uri.public + "/api/products/g/" + product_id + "/variants",
            rowName: 'variant'
        });

        // Bind creation button
        bindGroupCreationButton($("#widget-product-variants"));

        // Bind table buttons
        $("#widget-product-variants").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
        });
    }

    if ($("#widget-product-deals").length) {
        var product_id = $("#widget-product-deals").data('id');
        $("#widget-product-deals").ufTable({
            dataUrl: site.uri.public + "/api/products/g/" + product_id + "/deals"
        });

        // Bind creation button
        bindGroupCreationButton($("#widget-product-deals"));

        // Bind table buttons
        $("#widget-product-deals").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
        });
    }

    if ($("#widget-product-photos").length) {
        bindGroupButtons($("#widget-product-photos"));
    }
});