$(document).ready(function() {
    // Set up table of users
    $("#widget-customers").ufTable({
        dataUrl: site.uri.public + "/api/customers",
        useLoadingTransition: site.uf_table.use_loading_transition
    });

    // // Bind creation button
    // bindUserCreationButton($("#widget-users"));

    // Bind table buttons
    $("#widget-customers").on("pagerComplete.ufTable", function() {
        bindUserButtons($(this));
    });
});