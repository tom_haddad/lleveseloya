$(document).ready(function() {
    // Set up table of users
    $("#widget-user-orders").ufTable({
        dataUrl: site.uri.public + "/api/customers/u/" + page.user_name + "/orders",
        useLoadingTransition: site.uf_table.use_loading_transition
    });
});