/**
 * Page-specific Javascript file.  Should generally be included as a separate asset bundle in your page template.
 * example: {{ assets.js('js/pages/sign-in-or-register') | raw }}
 *
 * This script depends on widgets/suppliers.js, uf-table.js, moment.js, handlebars-helpers.js
 *
 * Target page: /suppliers
 */

$(document).ready(function() {
    $("#widget-suppliers").ufTable({
        dataUrl: site.uri.public + "/api/suppliers"
    });

    // Bind creation button
    bindGroupCreationButton($("#widget-suppliers"));

    // Bind table buttons
    $("#widget-suppliers").on("pagerComplete.ufTable", function() {
        bindGroupButtons($(this));
    });

    bindGroupButtons($('#view-supplier'));
    if ($("#widget-supplier-users").length) {
        var supplier_id = $("#widget-supplier-users").data('id');
        $("#widget-supplier-users").ufTable({
            dataUrl: site.uri.public + "/api/suppliers/g/" + supplier_id + "/users"
        });

        // Bind creation button
        bindGroupCreationButton($("#widget-supplier-users"));

        // Bind table buttons
        $("#widget-supplier-users").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
        });
    }

    if ($("#widget-supplier-products").length) {
        var supplier_id = $("#widget-supplier-products").data('id');
        $("#widget-supplier-products").ufTable({
            dataUrl: site.uri.public + "/api/suppliers/g/" + supplier_id + "/products"
        });

        // Bind creation button
        bindGroupCreationButton($("#widget-supplier-products"));

        // Bind table buttons
        $("#widget-supplier-products").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
        });
    }

    Dropzone.options.logoUploader = {
        createImageThumbnails: true,
        thumbnailWidth: 140,
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: 'Upload your logo here',
        init: function() {
            this.on('complete', function() {
                location.reload();
            });
        }
    }
});