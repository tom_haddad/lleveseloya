/**
 * Page-specific Javascript file.  Should generally be included as a separate asset bundle in your page template.
 * example: {{ assets.js('js/pages/sign-in-or-register') | raw }}
 *
 * This script depends on widgets/countdowns.js, uf-table.js, moment.js, handlebars-helpers.js
 *
 * Target page: /countdowns
 */

$(document).ready(function() {
    $("#widget-bespoke-countdowns").ufTable({
        dataUrl: site.uri.public + "/api/countdowns/bespoke"
    });

    // Bind creation button
    bindGroupCreationButton($("#widget-bespoke-countdowns"));

    // Bind table buttons
    $("#widget-bespoke-countdowns").on("pagerComplete.ufTable", function() {
        bindGroupButtons($(this));
    });

    $("#widget-weekly-countdowns").ufTable({
        dataUrl: site.uri.public + "/api/countdowns/weekly"
    });

    // Bind creation button
    bindGroupCreationButton($("#widget-weekly-countdowns"));

    // Bind table buttons
    $("#widget-weekly-countdowns").on("pagerComplete.ufTable", function() {
        bindGroupButtons($(this));
    });

    bindGroupButtons($('#view-countdown'));

});