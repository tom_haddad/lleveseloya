/**
 * Page-specific Javascript file.  Should generally be included as a separate asset bundle in your page template.
 * example: {{ assets.js('js/pages/sign-in-or-register') | raw }}
 *
 * This script depends on widgets/orders.js, uf-table.js, moment.js, handlebars-helpers.js
 *
 * Target page: /orders
 */

$(document).ready(function() {

    $('.datepicker').datepicker({
        beforeShow: function() {
            setTimeout(function() {
                $('.ui-datepicker').css('z-index', 99999999999999);
            }, 0);
        },
        dateFormat: 'dd-mm-yy'
    });

    if ($('input[name="start_date"]').val() !== "") {
        var dateRange = {
            'filters[start_date]': $('input[name="start_date"]').val(),
            'filters[end_date]': $('input[name="end_date"]').val()
        };
    } else {
        var dateRange = {};
    }

    if ($("#widget-orders").length) {
        $("#widget-orders").ufTable({
            dataUrl: site.uri.public + "/api/orders",
            addParams: dateRange
        });

        bindGroupCreationButton($("#widget-orders"));

        // Bind table buttons
        $("#widget-orders").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
        });

        bindGroupButtons($('#view-order'));
    }

    if ($("#widget-order-items").length) {
        var order_id = $("#widget-order-items").data('id');

        $("#widget-order-items").ufTable({
            dataUrl: site.uri.public + "/api/orders/o/" + order_id + "/items"
        });

        // Bind creation button
        bindGroupCreationButton($("#widget-order-items"));

        // Bind table buttons
        $("#widget-order-items").on("pagerComplete.ufTable", function() {
            bindGroupButtons($(this));
            doMultipleSelect($(this))
        });
    }

});