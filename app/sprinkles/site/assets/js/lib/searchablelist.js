$(document).ajaxComplete(function() {
    if ($('.searchable-checklist').length) {
        $('.searchable-checklist').each(function() {
            var _ = $(this),
                create = (_.data('create') == true);
            _.find('input[type="radio"].top_level').on('click', function() {
                _.find('input[type="radio"]:not(.top_level)').prop('checked', false);
            });
            _.find('input[type="radio"]:not(.top_level)').on('click', function() {
                _.find('input[type="radio"].top_level[value="' + $(this).data('top-level') + '"]').prop('checked', true);
            })
            _.find('input[type="text"][name="search"]').on('keyup', function() {
                var term = $(this).val();
                if (term !== "") {
                    _.find('li:not(.sl-selectall)').hide();
                    _.find('li:not(.sl-selectall)').each(function() {
                        if ($.trim($(this).find('.searchable-info').text()).toLowerCase().indexOf($.trim(term.toLowerCase())) >= 0) {
                            $(this).show();
                        }
                    });
                } else {
                    _.find('li:not(.sl-selectall)').show();
                }
            });
            if (create) {
                _.find('.sl-new-holder .input-group-addon').on('click', function() {
                    var val = _.find('.sl-new-input').val();
                    if (val !== "") {
                        var exists = false;
                        _.find('li:not(.sl-selectall)').each(function() {
                            if ($.trim($(this).find('.searchable-info').text().toLowerCase()) == $.trim(val.toLowerCase())) {
                                exists = true;
                            } else {
                                console.log($(this).find('.searchable-info').text().toLowerCase());
                            }
                        });
                        if (exists) {
                            alert('Item already exists with this name');
                        } else {
                            var newItem = _.find('.sl-cloneable').clone();
                            newItem.removeClass('sl-cloneable');
                            newItem.data('term', val);
                            newItem.find('input').prop('disabled', false).prop('checked', true);
                            newItem.find('input').val(val);
                            newItem.find('.searchable-info b').text(val);
                            _.find('.searchable-list .sl-selectall').before(newItem);
                        }
                    }
                })
            }
        });
        $('.searchable-checklist').on('change', '.sl-selectall input[type="checkbox"]', function() {
            var checked = $(this).is(':checked');
            $(this).closest('.searchable-checklist').find('li:visible input[type="checkbox"]').prop('checked', checked);
        })
    }
})