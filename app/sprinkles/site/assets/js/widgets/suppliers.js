/**
 * Groups widget.  Sets up dropdowns, modals, etc for a table of suppliers.
 */

/**
 * Set up the form in a modal after being successfully attached to the body.
 */
function attachGroupForm() {
    $("body").on('renderSuccess.ufModal', function(data) {
        var modal = $(this).ufModal('getModal');
        var form = modal.find('.js-form');


        form.find('.supplier-star-rating.editable').on('click', '.fa-fw', function() {
            var index = $(this).index();
            $('.supplier-star-rating.editable .fa-fw').removeClass('fa-star fa-star-o');
            for (var i = 0; i < 5; i++) {
                if (i <= index) {
                    $('.supplier-star-rating.editable .fa-fw').eq(i).addClass('fa-star');
                } else {
                    $('.supplier-star-rating.editable .fa-fw').eq(i).addClass('fa-star-o');
                }
            }
            $('input[name="rating"]').val(index + 1);
        });

        // Set icon when changed
        form.find('input[name=icon]').on('input change', function() {
            $(this).prev(".icon-preview").find("i").removeClass().addClass($(this).val());
        });

        // Set up the form for submission
        form.ufForm({
            validators: page.validators,
            binaryCheckboxes: false
        }).on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });
    });
}

/**
 * Link supplier action buttons, for example in a table or on a specific supplier's page.
 */
function bindGroupButtons(el) {
    /**
     * Link row buttons after table is loaded.
     */

    /**
     * Buttons that launch a modal dialog
     */
    // Edit supplier details button
    el.find('.js-supplier-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/suppliers/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    // Delete supplier button
    el.find('.js-supplier-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/suppliers/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-product-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    // Delete product button
    el.find('.js-product-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-supplier-user-remove').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/suppliers/confirm-remove-access",
            ajaxParams: {
                supplier_id: $(this).data('supplier_id'),
                user_id: $(this).data('user_id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-supplier-add-user').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/suppliers/add_user",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
}

function bindGroupCreationButton(el) {
    // Link create button
    el.find('.js-supplier-create').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/suppliers/create",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
};