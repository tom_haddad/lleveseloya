/**
 * Groups widget.  Sets up dropdowns, modals, etc for a table of products.
 */

/**
 * Set up the form in a modal after being successfully attached to the body.
 */
function attachGroupForm() {
    $("body").on('renderSuccess.ufModal', function(data) {
        var modal = $(this).ufModal('getModal');
        var form = modal.find('.js-form');


        form.find('.product-star-rating.editable').on('click', '.fa-fw', function() {
            var index = $(this).index();
            $('.product-star-rating.editable .fa-fw').removeClass('fa-star fa-star-o');
            for (var i = 0; i < 5; i++) {
                if (i <= index) {
                    $('.product-star-rating.editable .fa-fw').eq(i).addClass('fa-star');
                } else {
                    $('.product-star-rating.editable .fa-fw').eq(i).addClass('fa-star-o');
                }
            }
            $('input[name="rating"]').val(index + 1);
        });

        // Set icon when changed
        form.find('input[name=icon]').on('input change', function() {
            $(this).prev(".icon-preview").find("i").removeClass().addClass($(this).val());
        });

        // Set up the form for submission
        form.ufForm({
            validators: page.validators,
            binaryCheckboxes: false
        }).on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });
    });
}

/**
 * Link product action buttons, for example in a table or on a specific product's page.
 */
function bindGroupButtons(el) {
    /**
     * Link row buttons after table is loaded.
     */

    /**
     * Buttons that launch a modal dialog
     */

    el.find('.js-deal-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/edit-deal",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });
        attachGroupForm();
    });
    // Edit product details button
    el.find('.js-product-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    // Delete product button
    el.find('.js-product-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-product-deal-remove').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/confirm-delete-deal",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-variant-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/edit-variant",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });
        attachGroupForm();
    });

    el.find('.js-product-variant-remove').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/confirm-delete-variant",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-product-add-variant').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/add-variant",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-product-add-deal').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/add-deal",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-product-add-photos').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/add-photos",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.img-edit-details').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/edit-photo-details",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.img-remove').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/confirm-delete-photo",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });
}

$(document).on('ajaxComplete', function() {
    if ($('#product-photo-uploader').length) {
        var maxFiles = $('#product-photo-uploader').data('maxfiles');
        $('#product-photo-uploader').dropzone({
            acceptedFiles: 'image/*',
            autoProcessQueue: true,
            createImageThumbnails: true,
            dictDefaultMessage: 'Drop product images here to upload',
            maxFiles: maxFiles,
            parallelUploads: maxFiles,
            thumbnailWidth: 140,
            init: function() {
                this.on('queuecomplete', function() {
                    location.reload();
                });
            }
        });
    }
});

function bindGroupCreationButton(el) {
    // Link create button
    el.find('.js-product-create').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/products/create",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    $('form.approved').ufForm()
        .on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });

    $('.js-product-approved-submit').on('click', function() {
        $('form.approved').submit();
    });
};

function doMultipleSelect(el) {
    el.find('input[name="select-all"]').on('change', function() {
        var checked = $(this).is(':checked');
        $('input[name="mark[]"]').prop('checked', checked);
        setMultiple(el);
    });

    el.find('input[name="mark[]"]').each(function() {
        $(this).on('change', function() {
            setMultiple(el);
        });
    });
}

function setMultiple(el) {
    var products = [];

    el.find('input[name="mark[]"]:checked').each(function() {
        products.push($(this).val());
    });

    $('form.approved input[name="products"]').val(JSON.stringify(products));
}