/**
 * Groups widget.  Sets up dropdowns, modals, etc for a table of categories.
 */

/**
 * Set up the form in a modal after being successfully attached to the body.
 */
function attachGroupForm() {
    $("body").on('renderSuccess.ufModal', function(data) {
        var modal = $(this).ufModal('getModal');
        var form = modal.find('.js-form');


        form.find('.category-star-rating.editable').on('click', '.fa-fw', function() {
            var index = $(this).index();
            $('.category-star-rating.editable .fa-fw').removeClass('fa-star fa-star-o');
            for (var i = 0; i < 5; i++) {
                if (i <= index) {
                    $('.category-star-rating.editable .fa-fw').eq(i).addClass('fa-star');
                } else {
                    $('.category-star-rating.editable .fa-fw').eq(i).addClass('fa-star-o');
                }
            }
            $('input[name="rating"]').val(index + 1);
        });

        // Set icon when changed
        form.find('input[name=icon]').on('input change', function() {
            $(this).prev(".icon-preview").find("i").removeClass().addClass($(this).val());
        });

        // Set up the form for submission
        form.ufForm({
            validators: page.validators
        }).on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });
    });
}

/**
 * Link category action buttons, for example in a table or on a specific category's page.
 */
function bindGroupButtons(el) {
    /**
     * Link row buttons after table is loaded.
     */

    /**
     * Buttons that launch a modal dialog
     */
    // Edit category details button
    el.find('tr:not(.sub) .js-category-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/categories/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('tr.sub .js-category-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/categories/edit-sub",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    // Delete category button
    el.find('tr:not(.sub) .js-category-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/categories/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('tr.sub .js-category-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/categories/confirm-delete-sub",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('tr.has-subs td:first-child').on('click', function() {
        var id = $(this).parent().data('id'),
            parent = $(this).parent().data('parent-id');
        if ($('tr.sub[data-parent-id="' + id + '"]').is(':visible')) {
            $('tr.sub[data-parent-id="' + id + '"]').hide();
        } else {
            $('tr.sub[data-parent-id="' + id + '"]').show();
        }
    });
}

function bindGroupCreationButton(el) {
    // Link create button
    el.find('.js-category-create').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/categories/create",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
};