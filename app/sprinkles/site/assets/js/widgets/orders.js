/**
 * Groups widget.  Sets up dropdowns, modals, etc for a table of orders.
 */

/**
 * Set up the form in a modal after being successfully attached to the body.
 */
function attachGroupForm() {
    $("body").on('renderSuccess.ufModal', function(data) {
        var modal = $(this).ufModal('getModal');
        var form = modal.find('.js-form');

        // Set icon when changed
        form.find('input[name=icon]').on('input change', function() {
            $(this).prev(".icon-preview").find("i").removeClass().addClass($(this).val());
        });

        // Set up the form for submission
        form.ufForm({
            validators: page.validators
        }).on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });
    });
}



/**
 * Link order action buttons, for example in a table or on a specific order's page.
 */
function bindGroupButtons(el) {
    /**
     * Link row buttons after table is loaded.
     */

    /**
     * Buttons that launch a modal dialog
     */
    // Edit order details button
    el.find('.js-order-details').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/orders/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-order-item-details').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/orders/item",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-order-payment').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/orders/payment",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-order-mark-shipped').click(function() {
        var order_id = $(this).data('id');

        $('form.shipped input[name="orders"]').val('[' + order_id + ']');
        $('form.shipped').submit();
    });

    // Delete order button
    el.find('.js-order-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/orders/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });
}

function bindGroupCreationButton(el) {
    // Link create button
    el.find('.js-order-create').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/orders/create",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    $('form.shipped').ufForm()
        .on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });

    $('.js-order-shipped-submit').on('click', function() {
        var orders = $('form.shipped input[name="orders"]').val();
        if (orders !== "" && orders !== "[]") {
            $('form.shipped').submit();
        }
    });
};

function doMultipleSelect(el) {
    el.find('input[name="select-all"]').on('change', function() {
        var checked = $(this).is(':checked');
        $('input[name="mark[]"]').prop('checked', checked);
        setMultiple(el);
    });

    el.find('input[name="mark[]"]').each(function() {
        $(this).on('change', function() {
            setMultiple(el);
        });
    });
}

function setMultiple(el) {
    var orders = [];

    el.find('input[name="mark[]"]:checked').each(function() {
        orders.push($(this).val());
    });

    $('form.shipped input[name="orders"]').val(JSON.stringify(orders));
}