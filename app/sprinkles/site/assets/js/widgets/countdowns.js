/**
 * Groups widget.  Sets up dropdowns, modals, etc for a table of countdowns.
 */

/**
 * Set up the form in a modal after being successfully attached to the body.
 */
function attachGroupForm() {
    $("body").on('renderSuccess.ufModal', function(data) {
        var modal = $(this).ufModal('getModal');
        var form = modal.find('.js-form');

        // Set icon when changed
        form.find('input[name=icon]').on('input change', function() {
            $(this).prev(".icon-preview").find("i").removeClass().addClass($(this).val());
        });

        // Set up the form for submission
        form.ufForm({
            validators: page.validators,
            binaryCheckboxes: false
        }).on("submitSuccess.ufForm", function() {
            // Reload page on success
            window.location.reload();
        });
    });
}

/**
 * Link countdown action buttons, for example in a table or on a specific countdown's page.
 */
function bindGroupButtons(el) {
    /**
     * Link row buttons after table is loaded.
     */

    /**
     * Buttons that launch a modal dialog
     */
    // Edit countdown details button
    el.find('.js-countdown-edit').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/edit",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    el.find('.js-countdown-products').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/products",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });

    // Delete countdown button
    el.find('.js-countdown-delete').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/confirm-delete",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-countdown-user-remove').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/confirm-remove-access",
            ajaxParams: {
                countdown_id: $(this).data('countdown_id'),
                user_id: $(this).data('user_id')
            },
            msgTarget: $("#alerts-page")
        });

        $("body").on('renderSuccess.ufModal', function(data) {
            var modal = $(this).ufModal('getModal');
            var form = modal.find('.js-form');

            form.ufForm()
                .on("submitSuccess.ufForm", function() {
                    // Reload page on success
                    window.location.reload();
                });
        });
    });

    el.find('.js-countdown-add-user').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/add_user",
            ajaxParams: {
                id: $(this).data('id')
            },
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
}

function bindGroupCreationButton(el) {
    el.find('.js-countdown-create-bespoke').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/create-bespoke",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
    el.find('.js-countdown-create-weekly').click(function() {
        $("body").ufModal({
            sourceUrl: site.uri.public + "/modals/countdowns/create-weekly",
            msgTarget: $("#alerts-page")
        });

        attachGroupForm();
    });
};

$(window).ajaxComplete(function() {
    $('.datepicker').datepicker({
        beforeShow: function() {
            setTimeout(function() {
                $('.ui-datepicker').css('z-index', 99999999999999);
            }, 0);
        },
        dateFormat: 'dd-mm-yy'
    });
})