<?php

/**
 * en_US
 *
 * English message token translations for the site sprinkle.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 */

return [
    "@PLURAL_RULE" => 1,
    "ALL"   => "All",
    "FRONTEND" => [
        "ACCOUNT" => [
            "CANCELLED_ORDERS" => "Cancelled Orders",
            "CURRENT_ORDERS" => "Current Orders",
            "LOGOUT" => "Logout",
            "ORDER_HISTORY" => "Order History",
            "SETTINGS" => "Account Settings"
        ]
    ],
    "ORDER" => [
        "PAYMENT_STATUS"  => "Payment Status",
        "SHIPPING_STATUS" => "Shipping Status"
    ],
    "SUPPLIER_AREA" => [
        "BACK"  => [
            "ORDER" => "Back to Orders",
            "VARIANT" => "Back to Product",
        ],
        "INVENTORY" => [
            "ADD" => "Add",
            "SET" => "Set",
            "TITLE" => "Inventory"
        ],
        "ORDER" => [
            "COMPLETE"  => "Complete",
            "END_DATE" => "End date",
            "ENTER" => "Enter a name, username, or email address to search",
            "MARK_SHIPPED"  => "Mark Order as Shipped",
            "MARK_SHIPPED_DESC" => "This order has not been fulfilled yet.",
            "NONE" => "There are currently no orders to display.",
            "NUMBER" => "Order Number",
            "SAVE" => "Save Order",
            "START_DATE" => "Start date",
            "SUMMARY" => "Order Summary"
        ],
        "VARIANT_PAGE" => [
            1 => "Variant",
            2 => "Variants",
            "ADD"   => "Add Variant",
            "CHANGE_IMG" => "Change Image",
            "INVENTORY" => "Inventory",
            "OPTIONS" => "Options",
            "SAVE"  => "Save Variant",
            "UPLOAD_IMG" => "Upload image"
        ]
    ]
];
