<?php
    return [
        'address_book' => [
            'admin' => [
                'name'  => 'Tom Haddad'
            ]
        ],    
        'debug' => [
            'auth' => true,
            'smtp' => true
        ],
        'mail'    => [
            'mailer'     => 'smtp',     // Set to one of 'smtp', 'mail', 'qmail', 'sendmail'
            'host'       => 'mail.tomhaddad.com',
            'port'       => 587,
            'auth'       => true,
            'secure'     => 'tls',
            'username'   => "lleveseloya@mail.tomhaddad.com",
            'password'   => "@[;2 :@A 7hll hello",
            'smtp_debug' => 2,
            'message_options' => [
                'CharSet' => 'UTF-8',
                'isHtml' => true,
                'Timeout' => 15
            ]
        ],
        'site' => [
            'author'    =>      'Tom Haddad',
            'locales' =>  [
                // Should be ordered according to https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers,
                // with the exception of English, which as the default language comes first.
                'available' => [
                    'en_US' => 'English',
                    'es_ES' => 'Español'
                ],
                // This can be a comma-separated list, to load multiple fallback locales
                'default' => 'en_US'
            ],
            'registration' => [
                'captcha' => false,
                'user_defaults' => [
                    'locale' => 'es_ES',
                    'group' => 'customer'
                ]
            ],
            'title'     =>      'Lléveselo Ya!',
            // URLs
            'uri' => [
                'author' => 'https://tomhaddad.com'
            ]
        ],   
        'throttles' => [
            'check_email_request' => [
                'method'   => 'ip',
                'interval' => 3600,
                'delays' => [
                    40 => 1000
                ]
            ],
            'check_password_request' => [
                'method'   => 'ip',
                'interval' => 3600,
                'delays' => [
                    40 => 1000
                ]
            ],
            'check_conf_password_request' => [
                'method'   => 'ip',
                'interval' => 3600,
                'delays' => [
                    40 => 1000
                ]
            ]
        ],
        'timezone' => 'America/Costa_Rica'        
    ];