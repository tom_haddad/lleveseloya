### Add to the sprinkle list
Edit `app/sprinkles/sprinkles.json` file and add `extend-user` to the sprinkle list to enable it.

### Update the assets build & composer

- From the UserFrosting `/build` directory, run `npm run uf-assets-install`
- Run `composer update` from the `app/` directory.

### Run migration

- From the `migrations/` directory, run `php install.php`.  This should discover and run the migration in the Sprinkle, creating the new table.
