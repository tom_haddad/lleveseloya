<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * SupplierSprunje
 *
 * Implements Sprunje for the categories API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class DealSprunje extends Sprunje
{
    protected $name = 'deals';

    protected $sortable = [
        'variant_id',
        'price',
        'available'
    ];

    protected $filterable = [
        'variant_id',
        'price',
        'available'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('deal');

        return $query;
    }
}
