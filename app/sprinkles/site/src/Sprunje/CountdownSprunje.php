<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * CountdownSprunje
 *
 * Implements Sprunje for the countdowns API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class CountdownSprunje extends Sprunje
{
    protected $name = 'countdowns';

    protected $sortable = [
        'day',
        'hour',
        'date',
        'repeat',
        'active',
        'deals',
        'type'
    ];

    protected $filterable = [
        'hour',
        'date',
        'repeat',
        'active',
        'deals',
        'type'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('countdown');

        return $query;
    }
}
