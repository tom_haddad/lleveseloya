<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * ProductSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class ProductVariantSprunje extends Sprunje
{
    protected $name = 'products';

    protected $sortable = [
        'id'
    ];

    protected $filterable = [
        'id'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('product_variant');

        return $query;
    }

}
