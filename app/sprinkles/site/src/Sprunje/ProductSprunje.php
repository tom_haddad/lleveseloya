<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * ProductSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class ProductSprunje extends Sprunje
{
    protected $name = 'products';

    protected $sortable = [
        'id',
        'name',
        'approved',
        'deals',
        'variant_id',
        'price',
        'available',
        'mark'
    ];

    protected $filterable = [
        'id',
        'name',
        'approved',
        'deals',
        'variant_id',
        'price',
        'available'
    ];

    protected $listable = [
        'approved'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('product');

        return $query;
    }

    protected function sortDeals($query, $value)
    {
        return $query->orderBy('approved', $value);
    }

    protected function sortMark($query, $value)
    {
        return $query->orderBy('approved', $value);
    }

}
