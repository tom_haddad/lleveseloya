<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * OrderSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class OrderSprunje extends Sprunje
{
    protected $name = 'orders';

    protected $sortable = [
        'created_at',
        'reference',
        'product_name',
        'user_name',
        'supplier_name',
        'status',
        'mark'
    ];

    protected $filterable = [
        'created_at',
        'reference',
        'product_name',
        'user_name',
        'supplier_name',
        'status',
        'start_date',
        'end_date'
    ];

    protected $listable = [
        'status'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('order');

        return $query;
    }

    protected function filterEndDate($query, $value)
    {
        return $query->whereDate('created_at', '<=', date('Y-m-d', strtotime($value)).' 00:00:00');
    }

    protected function filterProductName($query, $value)
    {
        return $query->like('product_data', $value);
    }

    protected function filterSupplierName($query, $value)
    {
        return $query->like('supplier_data', $value);
    }

    protected function filterStartDate($query, $value)
    {
        return $query->whereDate('created_at', '>=', date('Y-m-d', strtotime($value)).' 00:00:00');
    }

    protected function filterUserName($query, $value)
    {
        return $query->like('user_data', $value);
    }

    protected function sortMark($query, $value)
    {
        return $query->orderBy('status', $value);
    }

    protected function sortProductName($query, $value)
    {
        return $query->orderBy('product_data', $value);
    }

    protected function sortSupplierName($query, $value)
    {
        return $query->orderBy('supplier_data', $value);
    }

    protected function sortUserName($query, $value)
    {
        return $query->orderBy('user_data', $value);
    }
}
