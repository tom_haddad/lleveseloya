<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Site\Sprunje\Merje;

/**
 * SupplierMerje
 *
 * Implements Merje for the categories API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class CategoryMerje extends Merje
{
    protected $name = 'categories';

    protected $sortable = [
        'id',
        'name'
    ];

    protected $filterable = [
        'id',
        'name'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('category');

        return $query;
    }
}
