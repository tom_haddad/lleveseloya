<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * SupplierUserSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class SupplierUserSprunje extends Sprunje
{
    protected $name = 'supplier_users';

    protected $sortable = [
        'supplier_id',
        'user_id'
    ];

    protected $filterable = [
        'supplier_id',
        'user_id'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('supplier_user');

        return $query;
    }
}
