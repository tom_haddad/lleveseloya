<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * ProductSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class ProductDealSprunje extends Sprunje
{
    protected $name = 'product_deals';

    protected $sortable = [
        'id',
        'variant_id',
        'price'
    ];

    protected $filterable = [
        'id',
        'variant_id',
        'price'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('product_deal');

        return $query;
    }

}
