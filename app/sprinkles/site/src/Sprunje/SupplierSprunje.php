<?php
namespace UserFrosting\Sprinkle\Site\Sprunje;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Sprinkle\Core\Sprunje\Sprunje;

/**
 * SupplierSprunje
 *
 * Implements Sprunje for the suppliers API.
 *
 * @author Tom Haddad (https://tomhaddad.com)
 */
class SupplierSprunje extends Sprunje
{
    protected $name = 'suppliers';

    protected $sortable = [
        'id',
        'name',
        'rating',
        'products'
    ];

    protected $filterable = [
        'id',
        'name',
        'rating',
        'products'
    ];

    /**
     * {@inheritDoc}
     */
    protected function baseQuery()
    {
        $query = $this->classMapper->createInstance('supplier');

        return $query;
    }

    protected function sortProducts($query, $value)
    {
        return $query->leftJoin('products', 'suppliers.id', '=', 'products.supplier_id')->groupBy('products.id');
    }
}
