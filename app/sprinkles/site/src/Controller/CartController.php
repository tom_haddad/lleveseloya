<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Image;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

use UserFrosting\Sprinkle\Site\Database\Models\Cart;
use UserFrosting\Sprinkle\Site\Database\Models\Product;

class CartController extends SiteController
{

	public function addItem($request, $response, $args) {

        $product = $this->getProductFromParams($args);

        if (!$product) {
            throw new NotFoundException($request, $response);
        }

		$authorizer = $this->ci->authorizer;
		$currentUser = $this->ci->currentUser;

		/** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
		$ms = $this->ci->alerts;

 		/** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
		$classMapper = $this->ci->classMapper;

		$return = array(
			'worked' => false,
			'action' => false
		);

		/* check if item exists in cart */
		$check = Cart::where('user_id', $currentUser->id)->where('product_id', $product->id)->first();

		if (is_object($check)) {
			$check->quantity = $check->quantity + 1;
			$check->save();
			$return['worked'] = true;
			$return['action'] = 'updated';
			$this->ci->userActivityLogger->info("User {$currentUser->user_name} updated quantity of {$product->name} in their cart.", [
				'type' => 'cart_update_quantity',
				'user_id' => $currentUser->id
			]);
		} else {
			$cart_item = Cart::create(array(
				'user_id' => $currentUser->id,
				'product_id' => $product->id,
				'quantity' => 1
			));
			$cart_item->save();
			$return['worked'] = true;
			$return['action'] = 'added';
			$this->ci->userActivityLogger->info("User {$currentUser->user_name} added {$product->name} to their cart.", [
				'type' => 'cart_add_item',
				'user_id' => $currentUser->id
			]);
		}

		$count = 0;

		foreach(Cart::where('user_id', $currentUser->id)->get() as $c) {
			$count = $count + $c->quantity;
		}

		$return['count'] = $count;

		echo json_encode($return);

		return $response->withStatus(200);
	}

	public function getCart($request, $response, $args)
    {
    	$page_uri = 'cart/page.html.twig';
    	$url = 'pages/front/index.html.twig';

    	if (isset($_GET['_'])) {
    		$url = 'pages/front/cart/page.html.twig';
    	}

    	$data = $this->data;

    	$extra = [
            'cart' => $this->cart,
            'page_uri' => $page_uri,
            'page_name' => 'Cart',
            'subtotal' => $this->getSubtotal(),
            'discount' => $this->getDiscount(),
            'tax' => $this->getTax(),
            'total' => $this->getTotal()
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function getCheckout($request, $response, $args)
    {
    	$page_uri = 'cart/checkout.html.twig';
    	$url = 'pages/front/index.html.twig';

    	if (isset($_GET['_'])) {
    		$url = 'pages/front/cart/checkout.html.twig';
    	}

    	$data = $this->data;

    	$extra = [
            'cart' => $this->cart,
            'page_uri' => $page_uri,
            'page_name' => 'Checkout',
            'subtotal' => $this->getSubtotal(),
            'discount' => $this->getDiscount(),
            'tax' => $this->getTax(),
            'total' => $this->getTotal()
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function getFigures($request, $response, $args) {
    	$return = array(
    		'subtotal' => $this->getSubtotal(),
    		'discount' => $this->getDiscount(),
    		'tax' => $this->getTax(),
    		'total' => $this->getTotal()
    	);

    	echo json_encode($return);

		return $response->withStatus(200);
    }


	protected function getProductFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $product = Product::where('id', $params['id'])->first();

        return $product;
    }

    protected function getDiscount()
    {
    	$st = $this->getSubtotal(true);
    	$total = $this->getTotalNoTax(true);

    	if ($st > 0 && $total > 0) {
        	$percentage = (100 / $st) * $total;
        } else {
        	$percentage = 0;
        }

        return 100 - ceil($percentage);
    }

    protected function getSubtotal($noformat = false)
    {
    	$st = 0;
        foreach($this->cart as $c) {
        	$st = $st + ($c->product->price * $c->quantity);
        }
        return ($noformat) ? $st : number_format($st, 2, '.', ',');
    }

    protected function getTax() {
    	$total = $this->getTotalNoTax(true);

    	$tax = ($total / 100) * 8;
    	return number_format($tax, 2, '.', ',');
    }

    protected function getTotal() {
    	$total = $this->getTotalNoTax(true);
    	$tax = $this->getTax(true);

    	return number_format(($total + $tax), 2, '.', ',');
    }

    protected function getTotalNoTax($noformat = false)
    {
    	$total = 0;
        foreach($this->cart as $c) {
        	$total = $total + ($c->product->discount_price * $c->quantity);
        }
        return ($noformat) ? $total : number_format($total, 2, '.', ',');
    }

    public function removeItem($request, $response, $args) {

        $cart_item = Cart::where('id', $args['id'])->first();

        if (!$cart_item) {
            throw new NotFoundException($request, $response);
        }

		$authorizer = $this->ci->authorizer;
		$currentUser = $this->ci->currentUser;

		/** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
		$ms = $this->ci->alerts;

 		/** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
		$classMapper = $this->ci->classMapper;

		$remove = $cart_item->delete();

		$count = 0;

		foreach(Cart::where('user_id', $currentUser->id)->get() as $c) {
			$count = $count + $c->quantity;
		}

		$return = array(
			'worked' => 1,
			'action' => 'remove',
			'count' => $count
		);

		echo json_encode($return);

		return $response->withStatus(200);
	}

	public function updateQuantity($request, $response, $args) {

        $cart_item = Cart::where('id', $args['id'])->first();
        $params = $request->getParsedBody();

        if (!$cart_item) {
            throw new NotFoundException($request, $response);
        }

		$currentUser = $this->ci->currentUser;


		$cart_item->quantity = $params['quantity'];
		$cart_item->save();

		$count = 0;

		foreach(Cart::where('user_id', $currentUser->id)->get() as $c) {
			$count = $count + $c->quantity;
		}

		$return = array(
			'worked' => 1,
			'action' => 'updated',
			'count' => $count
		);

		echo json_encode($return);

		return $response->withStatus(200);
	}
}
