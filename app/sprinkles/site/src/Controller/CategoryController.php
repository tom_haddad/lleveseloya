<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Category;
use UserFrosting\Sprinkle\Site\Database\Models\Subcategory;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

/**
 * Controller class for category-related requests, including listing categories, CRUD for categories, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class CategoryController extends SimpleController
{
    /**
     * Processes the request to create a new category.
     *
     * Processes the request from the category creation form, checking that:
     * 1. The category name and slug are not already in use;
     * 2. The user has permission to create a new category;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see getModalCreateCategory
     */
    public function create($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_category')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/category/create.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if ($classMapper->staticMethod('category', 'where', 'name', $data['name'])->first()) {
            $ms->addMessageTranslated('danger', 'CATEGORY.NAME_IN_USE', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        $sub = false;

        if (isset($data['parent_id']) && !empty($data['parent_id'])) {
            $sub = true;
            if (strpos($data['parent_id'], 'cat-') !== false) {
                $data['category_id'] = str_replace('cat-', '', $data['parent_id']);
                unset($data['parent_id']);
            } else {
                $mainCat = Subcategory::where('id', $data['parent_id'])->first()->category_id;
                $data['category_id'] = $mainCat;
            }
        }

        // All checks passed!  log events/activities and create category
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser, $sub) {
            // Create the category
            $append = "";
            if ($sub) {
                $category = $classMapper->createInstance('subcategory', $data);
                $append = "sub";
            } else {
                $category = $classMapper->createInstance('category', $data);
            }

            // Store new category to database
            $category->save();


            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created ".$append."category {$category->name}.", [
                'type' => $append.'category_create',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'CATEGORY.CREATION_SUCCESSFUL', $data);
        });

        return $response->withStatus(200);
    }

    /**
     * Processes the request to delete an existing category.
     *
     * Deletes the specified category.
     * Before doing so, checks that:
     * 1. The user has permission to delete this category;
     * 2. The category is not currently set as the default for new users;
     * 3. The category is empty (does not have any users);
     * 4. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: DELETE
     */
    public function delete($request, $response, $args)
    {
        $category = $this->getCategoryFromParams($args);

        // If the category doesn't exist, return 404
        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_category')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $categoryName = $category->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($category, $categoryName, $currentUser) {
            $category->delete();
            unset($category);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted category {$categoryName}.", [
                'type' => 'category_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'CATEGORY.DELETION_SUCCESSFUL', [
            'name' => $categoryName
        ]);

        return $response->withStatus(200);
    }

    /**
     * Returns a list of Categories
     *
     * Generates a list of categories, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */
    public function getList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_categories')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('category_merje', $classMapper, $params);

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getModalConfirmDelete($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $category = $this->getCategoryFromParams($params);

        // If the category no longer exists, forward to main category listing page
        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_category', [
            'category' => $category
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this category
        // $countCategoryUsers = CategoryUser::where('category_id', $category->id)->count();
        // if ($countCategoryUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('CATEGORY.NOT_EMPTY', $category->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-category.html.twig', [
            'category' => $category,
            'form' => [
                'action' => "api/categories/g/{$category->id}",
            ]
        ]);
    }
    /**
     * Renders the modal form for creating a new category.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalCreate($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_category')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy category to prepopulate fields
        $category = $classMapper->createInstance('category', []);

        $category->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/category/create.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/category.html.twig', [
            'category' => $category,
            'form' => [
                'action' => 'api/categories',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'category_list' => Category::getListForModal()
        ]);
    }

    /**
     * Renders the modal form for editing an existing category.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalEdit($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $category = $this->getCategoryFromParams($params);

        // If the category doesn't exist, return 404
        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['name', 'slug', 'icon', 'description'];
        if (!$authorizer->checkAccess($currentUser, 'uri_category')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/category/edit-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/category.html.twig', [
            'category' => $category,
            'form' => [
                'action' => "api/categories/g/{$category->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'category_list' => Category::getListForModal()
        ]);
    }

    public function getModalEditSub($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $category = $this->getSubcategoryFromParams($params);

        // If the category doesn't exist, return 404
        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['name', 'slug', 'icon', 'description'];
        if (!$authorizer->checkAccess($currentUser, 'uri_category')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/category/edit-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/category.html.twig', [
            'category' => $category,
            'form' => [
                'action' => "api/categories/g/{$category->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'category_list' => Category::getListForModal()
        ]);
    }

    public function getSubDropdown($request, $response, $args)
    {
        if ($args['is_sub'] == 0) {
            $category = $this->getCategoryFromParams($args);
            $cats = Subcategory::where('category_id', $category->id)->where('parent_id', null)->get();
        } else {
            $category = $this->getSubcategoryFromParams($args);
            $cats = Subcategory::where('parent_id', $category->id)->get();
        }

        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if (count($cats) > 0) {
            return $this->ci->view->render($response, 'pages/front/partials/subcategory_dropdown.html.twig', [
                'subcategory_list' => $cats
            ]);
        } else {
            return false;
        }
    }

    /**
     * Renders the category listing page.
     *
     * This page renders a table of categories, with dropdown menus for admin actions for each category.
     * Actions typically include: edit category, delete category.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageList($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_categories')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'pages/categories.html.twig');
    }

     /**
     * Processes the request to update an existing category's details.
     *
     * Processes the request from the category update form, checking that:
     * 1. The category name/slug are not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: PUT
     * @see getModalCategoryEdit
     */
    public function updateInfo($request, $response, $args)
    {
        $params = $request->getParsedBody();
        // Get the category based on slug in URL
        $map = "category";
        if (isset($params['parent_id']) && !empty($params['parent_id'])) {
            $map = "subcategory";
            $category = $this->getSubcategoryFromParams($args);
        } else {
            $category = $this->getCategoryFromParams($args);
        }

        if (!$category) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/category/edit-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this category
        if (!$authorizer->checkAccess($currentUser, 'uri_category', [
            'category' => $category,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if (
            isset($data['name']) &&
            $data['name'] != $category->name &&
            $classMapper->staticMethod($map, 'where', 'name', $data['name'])->first()
        ) {
            $ms->addMessageTranslated('danger', 'CATEGORY.NAME.IN_USE', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $category, $currentUser) {
            if ($map == "subcategory") {
                if (strpos($data['parent_id'], 'cat-') !== false) {
                    $category->category_id = str_replace('cat-', '', $data['parent_id']);
                    unset($data['parent_id']);
                } else {
                    $mainCat = Subcategory::where('id', $data['parent_id'])->first()->category_id;
                    $data['category_id'] = $mainCat;
                }
            }
            // Update the category and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $category->$name) {
                    $category->$name = $value;
                }
            }

            $category->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for category {$category->name}.", [
                'type' => 'category_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'CATEGORY.UPDATE', [
            'name' => $category->name
        ]);

        return $response->withStatus(200);
    }

    protected function getCategoryFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $category = Category::where('id', $params['id'])->first();

        return $category;
    }

    protected function getSubcategoryFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $category = Subcategory::where('id', $params['id'])->first();

        return $category;
    }
}
