<?php

namespace UserFrosting\Sprinkle\Site\Controller;

require_once('app/sprinkles/site/lib/qr/qrlib.php');

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;
use QRcode;

use UserFrosting\Sprinkle\Site\Database\Models\Cart;
use UserFrosting\Sprinkle\Site\Database\Models\Order;
use UserFrosting\Sprinkle\Site\Database\Models\OrderItem;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Site\Database\Models\UserMeta;

/**
 * Controller class for order-related requests, including listing orders, CRUD for orders, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class OrderController extends SimpleController
{
    /**
     * Processes the request to create a new order.
     *
     * Processes the request from the order creation form, checking that:
     * 1. The order name and slug are not already in use;
     * 2. The user has permission to create a new order;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see getModalCreateOrder
     */
    public function create($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_order')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/order/create.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;


        if ($error) {
            return $response->withStatus(400);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // All checks passed!  log events/activities and create order
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser) {
            // Create the order
            $order = $classMapper->createInstance('order', $data);

            // Store new order to database
            $order->save();


            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created order {$order->name}.", [
                'type' => 'order_create',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'ORDER.CREATION_SUCCESSFUL', $data);
        });

        return $response->withStatus(200);
    }

    /**
     * Processes the request to delete an existing order.
     *
     * Deletes the specified order.
     * Before doing so, checks that:
     * 1. The user has permission to delete this order;
     * 2. The order is not currently set as the default for new users;
     * 3. The order is empty (does not have any users);
     * 4. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: DELETE
     */
    public function delete($request, $response, $args)
    {
        $order = $this->getOrderFromParams($args);

        // If the order doesn't exist, return 404
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_order')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $orderName = $order->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($order, $orderName, $currentUser) {
            $order->delete();
            unset($order);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted order {$orderName}.", [
                'type' => 'order_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'ORDER.DELETION_SUCCESSFUL', [
            'name' => $orderName
        ]);

        return $response->withStatus(200);
    }

    public function getItems($request, $response, $args)
    {
        $order = $this->getOrderFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_order')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('order_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) use ($order) {
            return $order->items();
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }


    /**
     * Returns a list of Orders
     *
     * Generates a list of orders, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */
    public function getList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_orders')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('order_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) {
            return $query->with('payment')->with('items');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getModalConfirmDelete($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $order = $this->getOrderFromParams($params);

        // If the order no longer exists, forward to main order listing page
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_order', [
            'order' => $order
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this order
        // $countOrderUsers = OrderUser::where('order_id', $order->id)->count();
        // if ($countOrderUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('ORDER.NOT_EMPTY', $order->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-order.html.twig', [
            'order' => $order,
            'form' => [
                'action' => "api/orders/g/{$order->id}",
            ]
        ]);
    }
    /**
     * Renders the modal form for creating a new order.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalCreate($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_order')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy order to prepopulate fields
        $order = $classMapper->createInstance('order', []);

        $order->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/order/create.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/order.html.twig', [
            'order' => $order,
            'form' => [
                'action' => 'api/orders',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    /**
     * Renders the modal form for editing an existing order.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalEdit($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $order = $this->getOrderFromParams($params);

        // If the order doesn't exist, return 404
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_order')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/order/edit-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/order.html.twig', [
            'order' => $order,
            'form' => [
                'action' => "api/orders/g/{$order->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalItem($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $item = OrderItem::where('id', $params['id'])->first();

        // If the order doesn't exist, return 404
        if (!$item) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_order')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/order/item.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/order/item.html.twig', [
            'item' => $item,
            'form' => [
                'action' => "api/orders/item/{$item->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalPayment($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $order = $this->getOrderFromParams($params);

        // If the order doesn't exist, return 404
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_order')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'modals/order/payment.html.twig', [
            'order' => $order
        ]);
    }

    public function getQRCode($request, $response, $args) {

        $order = OrderItem::where('reference', $args['reference'])->first();

        // If the order doesn't exist, return 404
        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        if (!$authorizer->checkAccess($currentUser, 'uri_order')) {
            throw new NotFoundException($request, $response);
        }

        QRcode::png($order->reference);
        die();
    }

    public function markShipped($request, $response, $args)
    {

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/order/mark-shipped.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this order
        $auth = true;
        if (!$authorizer->checkAccess($currentUser, 'modify_orders')) {
            $orders = json_decode($data['orders']);
            foreach($orders as $o) {
                $item = OrderItem::where('id', $o)->first();
                if (is_object($item->supplier)) {
                    if (!$item->supplier->hasUser($currentUser->id)) {
                        $auth = false;
                    }
                }
            }
        }

        if (!$auth) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;


        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $currentUser) {
            $orders = json_decode($data['orders']);
            // Update the order and generate success messages
            foreach ($orders as $o) {
                $order = OrderItem::where('id', $o)->first();
                if (is_object($order)) {
                    $order->status = "shipped";
                    $order->save();
                }
                $this->ci->userActivityLogger->info("User {$currentUser->user_name} marked item {$order->product_data->name} from order ID {$order->id} as shipped.", [
                    'type' => 'order_item_update_status',
                    'user_id' => $currentUser->id
                ]);
            }
        });

        $ms->addMessageTranslated('success', 'ORDER.STATUS_UPDATE');

        return $response->withStatus(200);
    }

    public function pageInfo($request, $response, $args)
    {
        $order = $this->getOrderFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$order) {
            $redirectPage = $this->ci->router->pathFor('uri_orders');
            return $response->withRedirect($redirectPage, 404);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_order', [
                'order' => $order
            ])) {
            throw new ForbiddenException();
        }

        // Determine fields that currentUser is authorized to view
        $fieldNames = ['name', 'slug', 'icon', 'description'];

        // Generate form
        $fields = [
            'hidden' => []
        ];

        foreach ($fieldNames as $field) {
            if (!$authorizer->checkAccess($currentUser, 'view_order_field', [
                'order' => $order,
                'property' => $field
            ])) {
                $fields['hidden'][] = $field;
            }
        }

        // Determine buttons to display
        $editButtons = [
            'hidden' => []
        ];

        if (!$authorizer->checkAccess($currentUser, 'update_order_field', [
            'order' => $order,
            'fields' => ['name', 'slug', 'icon', 'description']
        ])) {
            $editButtons['hidden'][] = 'edit';
        }

        if (!$authorizer->checkAccess($currentUser, 'delete_order', [
            'order' => $order
        ])) {
            $editButtons['hidden'][] = 'delete';
        }

        return $this->ci->view->render($response, 'pages/order.html.twig', [
            'order' => $order,
            'fields' => $fields,
            'tools' => $editButtons
        ]);
    }

    /**
     * Renders the order listing page.
     *
     * This page renders a table of orders, with dropdown menus for admin actions for each order.
     * Actions typically include: edit order, delete order.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageList($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_orders')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'pages/orders.html.twig', [
            'start_date' => (isset($_GET['start_date'])) ? $_GET['start_date'] : false,
            'end_date' => (isset($_GET['end_date'])) ? $_GET['end_date'] : false
        ]);
    }

    public function processOrder($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        $error = false;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        $cart = Cart::where('user_id', $currentUser->id)->get();

        $user_data = array(
            'user_name' => $currentUser->user_name,
            'email' => $currentUser->email,
            'first_name' => $currentUser->first_name,
            'last_name' => $currentUser->last_name,
            'address_line_1' => ((is_object(UserMeta::where('user_id', $currentUser->id)->where('name', 'address_line_1')->first())) ? UserMeta::where('user_id', $currentUser->id)->where('name', 'address_line_1')->first()->value : ''),
            'address_line_2' => ((is_object(UserMeta::where('user_id', $currentUser->id)->where('name', 'address_line_2')->first())) ? UserMeta::where('user_id', $currentUser->id)->where('name', 'address_line_2')->first()->value : ''),
            'town_city' => ((is_object(UserMeta::where('user_id', $currentUser->id)->where('name', 'town_city')->first())) ? UserMeta::where('user_id', $currentUser->id)->where('name', 'town_city')->first()->value : ''),
            'state' => ((is_object(UserMeta::where('user_id', $currentUser->id)->where('name', 'state')->first())) ? UserMeta::where('user_id', $currentUser->id)->where('name', 'state')->first()->value : ''),
            'postcode' => ((is_object(UserMeta::where('user_id', $currentUser->id)->where('name', 'postcode')->first())) ? UserMeta::where('user_id', $currentUser->id)->where('name', 'postcode')->first()->value : '')
        );

        $data = array(
            'user_id' => $currentUser->id,
            'user_data' => $user_data,
            'status' => 'incomplete'
        );

        $items = [];

        foreach($cart as $c) {
            $product_data = array(
                'name' => $c->product->name,
                'original_price' => $c->product->price,
                'variant_name' => (is_object($c->variant)) ? $c->variant->name : '',
                'discount_price' => $c->product->discount_price
            );

            $supplier_data = array(
                'name' => $c->product->supplier->name
            );

            $items[] = array(
                'product_id' => $c->product_id,
                'supplier_id' => $c->product->supplier_id,
                'variant_id' => $c->variant_id,
                'product_data' => $product_data,
                'supplier_data' => $supplier_data,
                'quantity' => $c->quantity,
                "status" => "not_shipped"
            );
        }

        Capsule::transaction( function() use ($classMapper, $currentUser, $data, $items) {
            // Create the order
            $order = $classMapper->createInstance('order', $data);

            // Store new order to database
            $order->save();

            foreach($items as $i) {
                $item = $classMapper->createInstance('order_item', $i);
                $item->order_id = $order->id;
                $item->save();
            }

            /* remove */       
            $paymentdata = array(
                'user_id' => $currentUser->id,
                'amount_paid' => $order->total,
                'paypal_data' => array(
                    'transactionId' => 'TEST',
                    'status' => "SUCCESS"
                )
            ); 
            
            $payment = $classMapper->createInstance('payment', $paymentdata);
            $payment->save();
            $order->payment_id = $payment->id;
            $order->status = "paid";
            $order->save();
            /* end remove */

            // Create activity record
            $this->ci->userActivityLogger->info("Order ID #{$order->id} created for user {$currentUser->user_name} (incomplete).", [
                'type' => 'order_create_incomplete',
                'user_id' => $currentUser->id
            ]);

            $return = array(
                'worked' => 1,
                'order_id' => $order->id
            );

            echo json_encode($return);
        });

        return $response->withStatus(200);
    }

     /**
     * Processes the request to update an existing order's details.
     *
     * Processes the request from the order update form, checking that:
     * 1. The order name/slug are not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: PUT
     * @see getModalOrderEdit
     */
    public function updateInfo($request, $response, $args)
    {
        // Get the order based on slug in URL
        $order = $this->getOrderFromParams($args);

        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/order/edit-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this order
        if (!$authorizer->checkAccess($currentUser, 'uri_order', [
            'order' => $order,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if (
            isset($data['name']) &&
            $data['name'] != $order->name &&
            $classMapper->staticMethod('order', 'where', 'name', $data['name'])->first()
        ) {
            $ms->addMessageTranslated('danger', 'ORDER.NAME.IN_USE', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $order, $currentUser) {
            // Update the order and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $order->$name) {
                    $order->$name = $value;
                }
            }

            $order->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for order {$order->name}.", [
                'type' => 'order_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'ORDER.UPDATE', [
            'name' => $order->name
        ]);

        return $response->withStatus(200);
    }

    protected function getOrderFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $order = Order::where('id', $params['id'])->first();

        return $order;
    }

    protected function getOrderFromParamsByRef($params)
    {
        $classMapper = $this->ci->classMapper;

        $order = Order::where('reference', $params['reference'])->first();

        return $order;
    }
}
