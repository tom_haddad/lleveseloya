<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

use UserFrosting\Sprinkle\Site\Database\Models\Cart;
use UserFrosting\Sprinkle\Site\Database\Models\Category;
use UserFrosting\Sprinkle\Site\Database\Models\Product;
use UserFrosting\Sprinkle\Site\Database\Models\ProductCategory;
use UserFrosting\Sprinkle\Site\Database\Models\ProductMeta;
use UserFrosting\Sprinkle\Site\Database\Models\ProductPhoto;
use UserFrosting\Sprinkle\Site\Database\Models\ProductVariant;
use UserFrosting\Sprinkle\Site\Database\Models\ProductView;
use UserFrosting\Sprinkle\Site\Database\Models\Setting;
use UserFrosting\Sprinkle\Site\Database\Models\Subcategory;
use UserFrosting\Sprinkle\Site\Database\Models\Supplier;
use UserFrosting\Sprinkle\Site\Database\Models\SupplierUser;
use UserFrosting\Sprinkle\Account\Database\Models\User;


/**
 * Controller class for product-related requests, including listing products, CRUD for products, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class ProductController extends SiteController
{
    public function addVariant($request, $response, $args)
    {
        // Get the product based on slug in URL
        $product = $this->getProductFromParams($args);

        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/create-variant.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this supplier
        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if product already has variant with this name
        if (
            isset($data['title']) &&
            ProductVariant::where('title', $data['title'])->where('product_id', $product->id)->first()
        ) {
            $ms->addMessageTranslated('danger', 'VARIANT_NAME_EXISTS', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $product, $classMapper, $currentUser, $ms) {

            $variant = $classMapper->createInstance('product_variant', $data);

            $variant->product_id = $product->id;

            // Store new product to database
            $variant->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created variant {$variant->title} for product {$product->name}.", [
                'type' => 'product_add_variant',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'PRODUCT.VARIANT_ADDED');
        });

        return $response->withStatus(200);
    }

    public function checkStock($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $product = $this->getProductFromParams($args);

        // If the product doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        $available = $product->stock_available;

        $max = false;

        $currentUser = $this->ci->currentUser;

        $cart = Cart::where('user_id', $currentUser->id)->where('product_id', $product->id)->first();

        if (is_object($cart) && $cart->quantity == $available) {
            $max = true;
        }

        $return = array(
            'available' => ($available > 0),
            'remaining' => $available,
            'max' => $max
        );

        echo json_encode($return);die();

        return $response->withStatus(200);
    }

    public function checkStockMultiple($request, $response, $args)
    {
        // GET parameters
        $params = $request->getParsedBody();

        $soldout = [];
        $lessquantity = [];

        $currentUser = $this->ci->currentUser;   

        $ids = json_decode($params['ids']);

        if (count($ids) > 0) {
            foreach($ids as $i) {
                $product = Product::where('id', $i)->first();

                if (!$product) {
                    throw new NotFoundException($request, $response);
                }

                $available = $product->stock_available;

                if ($available <= 0) {
                    $soldout[] = $i;
                } else {
                    $cart = Cart::where('user_id', $currentUser->id)->where('product_id', $product->id)->first();
                    if ($cart->quantity > $available) {
                        $cart->quantity = $available;
                        $cart->save();
                        $lessquantity[] = array(
                            'id' => $i,
                            'amt' => $available
                        );
                    }
                }
            }
        }

        $return = array(
            'soldout' => $soldout,
            'lessquantity' => $lessquantity
        );

        echo json_encode($return);die();

        return $response->withStatus(200);
    }


    /**
     * Processes the request to create a new product.
     *
     * Processes the request from the product creation form, checking that:
     * 1. The product name and slug are not already in use;
     * 2. The user has permission to create a new product;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see getModalCreateProduct
     */
    public function create($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/create.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        // if ($classMapper->staticMethod('product', 'where', 'name', $data['name'])->first()) {
        //     $ms->addMessageTranslated('danger', 'PRODUCT.NAME.IN_USE', $data);
        //     $error = true;
        // }

        if ($error) {
            return $response->withStatus(400);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // All checks passed!  log events/activities and create product
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser) {
            // Create the product
            $product = $classMapper->createInstance('product', $data);

            // Store new product to database
            $product->save();

            foreach ($data as $name => $value) {
                if ($name == "category" && !empty($v)) {
                    ProductCategory::where('product_id', $product->id)->delete();
                    foreach($value as $v) {
                        if (!empty($v)) {
                            if (!is_numeric($v)) {
                                $newcat = Category::create(array(
                                    'name' => $v
                                ));
                                $newcat->save();
                                $v = $newcat->id;
                            }   

                            $cat = array(
                                'product_id' => $product->id,
                                'category_id' => $v
                            );
                            $category = $classMapper->createInstance('product_category', $cat);
                            $category->save();
                        }
                    }
                } else if (!isset($product->$name)) {
                    //$product->setMeta($name, $value);
                }
            }

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created product {$product->name}.", [
                'type' => 'product_create',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'PRODUCT.CREATION_SUCCESSFUL', $data);
        });

        return $response->withStatus(200);
    }

    /**
     * Processes the request to delete an existing product.
     *
     * Deletes the specified product.
     * Before doing so, checks that:
     * 1. The user has permission to delete this product;
     * 2. The product is not currently set as the default for new users;
     * 3. The product is empty (does not have any users);
     * 4. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: DELETE
     */
    public function delete($request, $response, $args)
    {
        $product = $this->getProductFromParams($args);

        // If the product doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $productName = $product->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($product, $productName, $currentUser) {
            $product->delete();
            unset($product);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted product {$productName}.", [
                'type' => 'product_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'PRODUCT.DELETION_SUCCESSFUL', [
            'name' => $productName
        ]);

        return $response->withStatus(200);
    }

    public function deletePhoto($request, $response, $args)
    {
        $photo = ProductPhoto::where('id', $args['id'])->first();

        // If the product doesn't exist, return 404
        if (!$photo) {
            throw new NotFoundException($request, $response);
        }

        $product = $photo->product;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $photoId = $photo->id;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($photo, $photoId, $currentUser) {
            $photo->delete();
            unset($photo);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted product photo ID {$photoId}.", [
                'type' => 'product_photo_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'PRODUCT.PHOTO_DELETION_SUCCESSFUL');

        return $response->withStatus(200);
    }

    public function deleteVariant($request, $response, $args)
    {
        $variant = ProductVariant::where('id', $args['id'])->first();

        // If the product doesn't exist, return 404
        if (!$variant) {
            throw new NotFoundException($request, $response);
        }

        $product = $variant->product;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $photoId = $variant->id;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($variant, $photoId, $currentUser) {
            $variant->delete();
            unset($variant);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted product variant ID {$photoId}.", [
                'type' => 'product_variant_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'PRODUCT.VARIANT_DELETION_SUCCESSFUL');

        return $response->withStatus(200);
    }

    /**
     * Returns a list of Products
     *
     * Generates a list of products, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */
    public function getList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_products')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('product_sprunje', $classMapper, $params);
        $sprunje->extendQuery(function ($query) {
            return $query->with('supplier');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getModalConfirmDelete($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $product = $this->getProductFromParams($params);

        // If the product no longer exists, forward to main product listing page
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_product', [
            'product' => $product
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this product
        // $countProductUsers = ProductUser::where('product_id', $product->id)->count();
        // if ($countProductUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('PRODUCT.NOT_EMPTY', $product->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-product.html.twig', [
            'product' => $product,
            'form' => [
                'action' => "api/products/g/{$product->id}",
            ]
        ]);
    }

    public function getModalConfirmDeletePhoto($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $photo = ProductPhoto::where('id', $params['id'])->first();

        // If the photo doesn't exist, return 404
        if (!$photo) {
            throw new NotFoundException($request, $response);
        }

        $product = $photo->product;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_product', [
            'product' => $product
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this product
        // $countProductUsers = ProductUser::where('product_id', $product->id)->count();
        // if ($countProductUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('PRODUCT.NOT_EMPTY', $product->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/product/confirm-delete-photo.html.twig', [
            'photo' => $photo,
            'form' => [
                'action' => "api/products/g/delete-photo/{$photo->id}",
            ]
        ]);
    }

    public function getModalConfirmDeleteVariant($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $variant = ProductVariant::where('id', $params['id'])->first();

        // If the variant doesn't exist, return 404
        if (!$variant) {
            throw new NotFoundException($request, $response);
        }

        $product = $variant->product;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'update_product_settings', [
            'product' => $product
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this product
        // $countProductUsers = ProductUser::where('product_id', $product->id)->count();
        // if ($countProductUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('PRODUCT.NOT_EMPTY', $product->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-variant.html.twig', [
            'variant' => $variant,
            'form' => [
                'action' => "api/products/g/delete-variant/{$variant->id}",
            ]
        ]);
    }

    /**
     * Renders the modal form for creating a new product.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalCreate($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_product')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy product to prepopulate fields
        $product = $classMapper->createInstance('product', []);

        $product->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/create.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/product.html.twig', [
            'product' => $product,
            'form' => [
                'action' => 'api/products',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'supplier_list' => Supplier::get(),
            'category_list' => Category::getListForModal()
        ]);
    }

    /**
     * Renders the modal form for editing an existing product.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalEdit($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $product = $this->getProductFromParams($params);

        // If the product doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['name', 'slug', 'icon', 'description'];
        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/edit-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/product.html.twig', [
            'product' => $product,
            'form' => [
                'action' => "api/products/g/{$product->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'meta' => $product->getMeta(),
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'supplier_list' => Supplier::get(),
            'category_list' => Category::getListForModal()
        ]);
    }

    public function getModalEditPhoto($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $photo = ProductPhoto::where('id', $params['id'])->first();

        // If the product doesn't exist, return 404
        if (!$photo) {
            throw new NotFoundException($request, $response);
        }

        $product = $photo->product;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['caption', 'variant_id', 'highlight'];
        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/edit-photo-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/product/photo.html.twig', [
            'photo' => $photo,
            'form' => [
                'action' => "api/products/g/edit-photo/{$photo->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'meta' => $product->getMeta(),
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'variant_list' => ProductVariant::where('product_id', $product->id)->get()
        ]);
    }

    public function getModalEditVariant($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $variant = ProductVariant::where('id', $params['id'])->first();

        // If the product doesn't exist, return 404
        if (!$variant) {
            throw new NotFoundException($request, $response);
        }


        $product = $variant->product;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['caption', 'variant_id', 'highlight'];
        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/create-variant.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/product/variant.html.twig', [
            'variant' => $variant,
            'form' => [
                'action' => "api/products/g/edit-variant/{$variant->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalPhotosAdd($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $product = $this->getProductFromParams($params);

        // If the supplier doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/create-photos.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        $photos = count($product->photos);

        if ($photos < 12) {
            $maxfiles = 12 - $photos;
        } else {
            $maxfiles = 0;
        }

        return $this->ci->view->render($response, 'modals/product/photos.html.twig', [
            'product' => $product,
            'form' => [
                'action' => "api/images/upload/ProductPhoto/_/image_id/{$product->id}/product_id",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('DONE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'maxfiles' => $maxfiles
        ]);
    }

    public function getModalPhotoModify($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $photo = ProductPhoto::where('id', $params['photo_id'])->first();

        // If the supplier doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/update-photo.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/product/photo.html.twig', [
            'product' => $product,
            'form' => [
                'action' => "api/images/upload/ProductPhoto/_/image_id/{$product->id}/product_id",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('DONE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalVariantCreate($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $product = $this->getProductFromParams($params);

        // If the supplier doesn't exist, return 404
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/product/create-variant.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/product/variant.html.twig', [
            'product' => $product,
            'form' => [
                'action' => "api/products/g/add-variant/{$product->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('PRODUCT.ADD_VARIANT')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getProduct($request, $response, $args)
    {
        $product = $this->getFrontProduct($args);

        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        $product->addView();

        $page_uri = 'listings/product.html.twig';
        $url = 'pages/front/index.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/listings/product.html.twig';
        }

        $data = $this->data;

        $extra = [
            'cart' => $this->cart,
            'page_uri' => $page_uri,
            'page_name' => $product->name,
            'product' => $product
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function getVariants($request, $response, $args)
    {
        $product = $this->getProductFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_product')) {
            throw new ForbiddenException();
        }

        // $checkLink = SupplierUser::where('user_id', $currentUser->id)->where('supplier_id', $suppier->id)->first();

        // var_dump($checkLink);die();

        // if (!is_object($checkLink)) {
        //     throw new ForbiddenException();
        // }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('product_variant_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) use ($product) {
            return $query->where('product_id', $product->id);
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Renders a page displaying a product's information, in read-only mode.
     *
     * This checks that the currently logged-in user has permission to view the requested product's info.
     * It checks each field individually, showing only those that you have permission to view.
     * This will also try to show buttons for deleting, and editing the product.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageInfo($request, $response, $args)
    {
        $product = $this->getProductFromParams($args);

        // If the product no longer exists, forward to main product listing page
        if (!$product) {
            $redirectPage = $this->ci->router->pathFor('uri_products');
            return $response->withRedirect($redirectPage, 404);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_product', [
                'product' => $product
            ])) {
            throw new ForbiddenException();
        }

        // Determine fields that currentUser is authorized to view
        $fieldNames = ['name', 'slug', 'icon', 'description'];

        // Generate form
        $fields = [
            'hidden' => []
        ];

        foreach ($fieldNames as $field) {
            if (!$authorizer->checkAccess($currentUser, 'view_product_field', [
                'product' => $product,
                'property' => $field
            ])) {
                $fields['hidden'][] = $field;
            }
        }

        // Determine buttons to display
        $editButtons = [
            'hidden' => []
        ];

        if (!$authorizer->checkAccess($currentUser, 'update_product_field', [
            'product' => $product,
            'fields' => ['name', 'slug', 'icon', 'description']
        ])) {
            $editButtons['hidden'][] = 'edit';
        }

        if (!$authorizer->checkAccess($currentUser, 'delete_product', [
            'product' => $product
        ])) {
            $editButtons['hidden'][] = 'delete';
        }

        return $this->ci->view->render($response, 'pages/product.html.twig', [
            'product' => $product,
            'fields' => $fields,
            'tools' => $editButtons
        ]);
    }

    public function markApproved($request, $response, $args)
    {

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/mark-approved.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this order
        $auth = true;
        if (!$authorizer->checkAccess($currentUser, 'modify_products')) {
            $products = json_decode($data['products']);
            foreach($products as $p) {
                $product = Product::where('id', $p)->first();
                if (is_object($p->supplier)) {
                    if (!$p->supplier->hasUser($currentUser->id)) {
                        $auth = false;
                    }
                }
            }
        }

        if (!$auth) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;


        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $currentUser) {
            $products = json_decode($data['products']);
            // Update the order and generate success messages
            foreach ($products as $p) {
                $product = Product::where('id', $p)->first();
                if (is_object($product)) {
                    $product->approved = 1;
                    $product->save();
                }
                $this->ci->userActivityLogger->info("User {$currentUser->user_name} marked product ID {$product->id} as approved.", [
                    'type' => 'order_product_status',
                    'user_id' => $currentUser->id
                ]);
            }
        });

        $ms->addMessageTranslated('success', 'PRODUCT.STATUS_UPDATE');

        return $response->withStatus(200);
    }

    /**
     * Renders the product listing page.
     *
     * This page renders a table of products, with dropdown menus for admin actions for each product.
     * Actions typically include: edit product, delete product.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageList($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_products')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'pages/products.html.twig');
    }

    /**
     * Processes the request to update an existing product's details.
     *
     * Processes the request from the product update form, checking that:
     * 1. The product name/slug are not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: PUT
     * @see getModalProductEdit
     */
    public function updateInfo($request, $response, $args)
    {
        // Get the product based on slug in URL
        $product = $this->getProductFromParams($args);

        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/edit-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this product
        if (!$authorizer->checkAccess($currentUser, 'uri_product', [
            'product' => $product,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if (
            isset($data['name']) &&
            $data['name'] != $product->name &&
            $classMapper->staticMethod('product', 'where', 'name', $data['name'])->first()
        ) {
            $ms->addMessageTranslated('danger', 'PRODUCT.NAME.IN_USE', $data);
            $error = true;
        }

        if (!isset($data['subcategory_id'])) {
            $data['subcategory_id'] = null;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $product, $currentUser, $classMapper) {
            // Update the product and generate success messages
            foreach ($data as $name => $value) {
                if ($name == "category") {
                    ProductCategory::where('product_id', $product->id)->delete();
                    foreach($value as $v) {
                        if (!empty($v)) {
                            if (!is_numeric($v)) {
                                $newcat = Category::create(array(
                                    'name' => $v
                                ));
                                $newcat->save();
                                $v = $newcat->id;
                            }   

                            $cat = array(
                                'product_id' => $product->id,
                                'category_id' => $v
                            );
                            $category = $classMapper->createInstance('product_category', $cat);
                            $category->save();
                        }
                    }
                } else if ($product->hasAttribute($name)) {
                    if ($value != $product->$name) {
                        $product->$name = $value;
                    }
                } else {
                   // $product->setMeta($name, $value);
                }
            }

            $product->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for product {$product->name}.", [
                'type' => 'product_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'PRODUCT.UPDATE', [
            'name' => $product->name
        ]);

        return $response->withStatus(200);
    }

    public function updatePhotoInfo($request, $response, $args)
    {
        $photo = ProductPhoto::where('id', $args['id'])->first();

        // If the product doesn't exist, return 404
        if (!$photo) {
            throw new NotFoundException($request, $response);
        }

        $product = $photo->product;

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/edit-photo-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this product
        if (!$authorizer->checkAccess($currentUser, 'uri_product', [
            'product' => $product,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $product, $currentUser, $photo) {
            // Update the product and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $photo->$name) {
                    $photo->$name = $value;
                }
            }

            $photo->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for photo of {$product->name}.", [
                'type' => 'product_update_photo_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'PRODUCT.UPDATE_PHOTO', [
            'name' => $product->name
        ]);

        return $response->withStatus(200);
    }

    public function updatePhotoInfoFront($request, $response, $args)
    {
        $photo = ProductPhoto::where('id', $args['id'])->first();

        // If the product doesn't exist, return 404
        if (!$photo) {
            throw new NotFoundException($request, $response);
        }

        $product = $photo->product;

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/edit-photo-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this product
        if (!$authorizer->checkAccess($currentUser, 'uri_product', [
            'product' => $product,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $product, $currentUser, $photo) {
            // Update the product and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $photo->$name) {
                    $photo->$name = $value;
                }
            }

            $photo->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for photo of {$product->name}.", [
                'type' => 'product_update_photo_info',
                'user_id' => $currentUser->id
            ]);
        });

        // $ms->addMessageTranslated('success', 'PRODUCT.UPDATE_PHOTO', [
        //     'name' => $product->name
        // ]);

        return $response->withStatus(200);
    }

    public function updateVariantInfo($request, $response, $args)
    {
        $variant = ProductVariant::where('id', $args['id'])->first();

        // If the product doesn't exist, return 404
        if (!$variant) {
            throw new NotFoundException($request, $response);
        }

        $product = $variant->product;

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/product/create-variant.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this product
        if (!$authorizer->checkAccess($currentUser, 'uri_product', [
            'product' => $product,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $variant, $currentUser) {
            // Update the product and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $variant->$name) {
                    $variant->$name = $value;
                }
            }

            $variant->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for product variant ID {$variant->id}.", [
                'type' => 'product_update_variant_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'PRODUCT.UPDATE_VARIANT', [
            'name' => $product->name
        ]);

        return $response->withStatus(200);
    }

    protected function getProductFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $product = Product::where('id', $params['id'])->first();

        return $product;
    }

    protected function getFrontProduct($params)
    {
        $classMapper = $this->ci->classMapper;

        $product = Product::where('id', $params['id'])->where('url', $params['name'])->first();

        if ($params['category'] !== "all") {
            $category = Category::where('url', $params['category'])->first();
        } else {
            $category = true;
        }

        if (!$product || !$category) {
            return false;
        }

        if ($params['category'] !== "all" && $product->category_id !== $category->id) {
            return false;
        }

        return $product;
    }

    public function frontCreate($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!(count(SupplierUser::where('user_id', $this->current_user->id)->get()) > 0 || $this->current_user->group_id == 3)) {
            throw new ForbiddenException();
        }

        if (!isset($params['supplier_id'])) {
            if (count(SupplierUser::where('user_id', $this->current_user->id)->get()) > 0) {
                $params['supplier_id'] = SupplierUser::where('user_id', $this->current_user->id)->first()->supplier_id;
            } else if ($this->current_user->group_id == 3) {
                /* REMOVE BEFORE LAUNCH */
                $params['supplier_id'] = Supplier::first()->id;
            }
        }

        if (isset($params['pd_subcategory_id']) && empty($params['pd_subcategory_id'])) {
            unset($params['pd_subcategory_id']);
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $newParams = [];

        foreach($params as $k => $v) {
            $str = str_replace('pd_', '', $k);

            $newParams[$str] = $v;
        }

        $params = $newParams;

        $required = ['name', 'category_id', 'price', 'discount_price', 'description', 'photos', 'quantity'];

        $errors = [];


        foreach($params as $k => $v) {
            $v = trim($v);
            $params[$k] = $v;
            if (in_array($k, $required) && empty($v)) {
                switch($k) {
                    case 'photos':
                        $errors[] = array(
                            'field' => $k,
                            'message' => 'VALIDATE.PHOTOS_REQUIRED'
                        );
                        break;
                    case 'quantity':
                        if (empty($params['variant_detail'])) {
                            $errors[] = array(
                                'field' => 'total_quantity',
                                'message' => 'VALIDATE.QUANTITY_REQUIRED'
                            );
                        }
                        break;
                    default:
                        $errors[] = array(
                            'field' => $k,
                            'message' => 'VALIDATE.REQUIRED'
                        );
                        break;
                }
            } else {
                switch($k) {
                    case 'discount_price':
                        if (isset($params['price']) && $params['price'] < $v) {
                            $errors[] = array(
                                'field' => $k,
                                'message' => 'VALIDATE.DISCOUNT_INVALID'
                            );
                        }
                        break;
                }
            }
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        // if ($classMapper->staticMethod('product', 'where', 'name', $data['name'])->first()) {
        //     $ms->addMessageTranslated('danger', 'PRODUCT.NAME.IN_USE', $data);
        //     $error = true;
        // }

        if (count($errors) > 0) {

            $returner = array(
                'worked' => 0,
                'errors' => $errors
            );

            echo json_encode($returner);

            return $response->withStatus(200);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        $product_keys = ['name', 'description', 'price', 'discount_price', 'quantity', 'category_id', 'subcategory_id', 'supplier_id', 'featured'];

        $meta_keys = ['sku', 'barcode'];
        
        $product_data = [];

        $meta_data = [];

        $data = $params;

        foreach($data as $k => $v) {
            if (in_array($k, $product_keys)) {
                $product_data[$k] = $v;
            } else if (in_array($k, $meta_keys)) {
                $meta_data[$k] = $v;
            }
        }

        $updating = false;

        if (!isset($data['product_id']) || empty($data['product_id'])) {
            $product = $classMapper->createInstance('product', $product_data);
        } else {
            $product = Product::where('id', $data['product_id'])->first();
            $updating = true;
            $su = SupplierUser::where('user_id', $this->current_user->id)->get();

            $go = false;
            foreach($su as $u) {
                if ($u->supplier_id == $product->supplier_id) {
                    $go = true;
                }
            }
            if ($go || $this->current_user->group_id == 3) {
                foreach($product_data as $k => $v) {
                    if ($product->{$k} !== $v) {
                        $product->{$k} = $v;
                    }
                }
            } else {
                if ($this->current_user->group_id == 4) {
                    $returner = array(
                        'worked' => 0,
                        'product_id' => $product->id,
                        'updating' => 1,
                        'approved' => $product->approved,
                        'disabled' => true
                    );

                    echo json_encode($returner);
                    return $response->withStatus(200);
                }
            }
        }

        $product->draft = 0;
        $product->save();

        foreach($meta_data as $k => $v) {
            $check = ProductMeta::where('product_id', $product->id)->where('name', $k)->first();

            if (is_object($check)) {
                if ($check->value !== $v) {
                    $check->value = $v;
                    $check->save();
                }
            } else {
                $meta = array(
                    'product_id' => $product->id,
                    'name' => $k,
                    'value' => $v
                );
                $pm = $classMapper->createInstance('product_meta', $meta);
                $pm->save();
            }
        }


        /* Save admin fee and decide whether product needs admin approval */
        $fee = $product->getFeeInitial();

        $product->admin_fee = $fee;

        $minFee = Setting::where('name', 'feeMinimum')->first()->value;

        if ($fee <= $minFee || $product->featured == 1) {
            $product->approved = 0;
        } else {
            $product->approved = 1;
        }

        $product->save();

        if (!empty($data['photos'])) {
            $photos = json_decode($data['photos']);

            if (!empty($photos)) {
                $curArray = [];
                $curPhotos = ProductPhoto::where('product_id', $product->id)->get();
                $newArray = [];
                foreach($curPhotos as $p) {
                    $curArray[] = $p->image_id;
                }
                foreach($photos as $p) {
                    $newArray[] = $p;
                }
                foreach($curArray as $p) {
                    if (!in_array($p, $newArray)) {
                        $photo = ProductPhoto::where('image_id', $p)->where('product_id', $product->id)->first();
                        $photo->delete();
                    }
                }
                foreach($photos as $p) {
                    $check = ProductPhoto::where('image_id', $p)->where('product_id', $product->id)->first();
                    if (is_object($check)) {

                    } else {
                        $photo = $classMapper->createInstance('product_photo', array(
                            'image_id' => $p,
                            'product_id' => $product->id
                        ));
                        $photo->save();
                    }
                }
            }
        }

        if (!empty($data['variant_detail'])) {
            $variants = json_decode($data['variant_detail']);

            if (!empty($variants)) {
                $curArray = [];
                $curVariants = ProductVariant::where('product_id', $product->id)->get();
                $newArray = [];
                foreach($curVariants as $v) {
                    $curArray[] = $v->data;
                }
                foreach($variants as $v) {
                    $newArray[] = json_encode($v->data);
                }
                foreach($curArray as $v) {
                    if (!in_array($v, $newArray)) {
                        $variant = ProductVariant::where('data', $v)->where('product_id', $product->id)->first();
                        $variant->delete();
                    }
                }
                foreach($variants as $v) {
                    $check = ProductVariant::where('product_id', $product->id)->where('data', json_encode($v->data))->first();

                    if (is_object($check)) { 
                        $check->quantity = $v->quantity;
                        $check->sku = $v->sku;
                        $check->barcode = $v->barcode;
                        $check->save();
                    } else {
                        $variant = $classMapper->createInstance('product_variant', array(
                            'product_id' => $product->id,
                            'data' => json_encode($v->data),
                            'sku' => $v->sku,
                            'barcode' => $v->barcode,
                            'quantity' => $v->quantity
                        ));
                        $variant->save();
                    }
                }
            }
        }


        // Create activity record
        $this->ci->userActivityLogger->info("User {$currentUser->user_name} created product {$product->name}.", [
            'type' => 'product_create',
            'user_id' => $currentUser->id
        ]);

        $returner = array(
            'worked' => 1,
            'product_id' => $product->id,
            'updating' => ($updating) ? 1 : 0,
            'approved' => $product->approved
        );

        echo json_encode($returner);
        return $response->withStatus(200);
    }

    public function frontGetFee($request, $response, $args) {
        $params = $request->getParsedBody();

        $accepted = ['pd_featured', 'pd_price', 'pd_discount_price', 'pd_category_id', 'pd_subcategory_id'];

        $new = [];

        foreach($params as $k => $v) {
            if (in_array($k, $accepted)) {
                $new[$k] = $v;
            }
        }

        $returner = array(
            'worked' => 0
        );

        if (isset($new['pd_price']) && isset($new['pd_discount_price'])) {
            $price = intval($new['pd_price']);
            $discount = intval($new['pd_discount_price']);

            if ($price > $discount) {
                if (isset($new['pd_subcategory_id']) && !empty($new['pd_subcategory_id'])) {
                    $checkCategory = Subcategory::where('id', $new['pd_subcategory_id'])->first();
                } else if (isset($new['pd_category_id']) && !empty($new['pd_category_id'])) {
                    $checkCategory = Category::where('id', $new['pd_category_id'])->first();
                }
                if (is_object($checkCategory)) {
                    $fee = $checkCategory->fee;
                    $admin_fee = (($discount / 100) * $fee);
                    $featured_fee = 0;
                    $customer = $discount + $admin_fee;
                    if (isset($new['pd_featured']) && $new['pd_featured'] > 0) {
                        $featured_fee = Setting::where('name', 'featuredFee')->first()->value;
                    }
                    $returner = array(
                        'worked' => 1,
                        'featured_fee' => number_format($featured_fee, 2),
                        'admin_fee' => number_format($admin_fee, 2),
                        'customer' => number_format($customer, 2)
                    );
                }
            } else {
                $returner['error'] = 'Price smaller than discount';
            }
        } else {
            $returner['error'] = 'Prices not set correctly';
        }

        echo json_encode($returner);
        return $response->withStatus(200);
    }
    
    public function frontSaveVariant($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!(count(SupplierUser::where('user_id', $this->current_user->id)->get()) > 0 || $this->current_user->group_id == 3)) {
            throw new ForbiddenException();
        }

        $product = Product::where('id', $args['product_id'])->first();

        if (!is_object($product)) {
            throw new ForbiddenException();
        }

        /* check product belongs to user or user is admin */
        $supplier = Supplier::where('id', $product->supplier_id)->first();

        $checkAccess = SupplierUser::where('user_id', $this->current_user->id)->where('supplier_id', $supplier->id)->first();

        if (!is_object($checkAccess) && $this->current_user->group_id !== 3) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $newParams = [];

        foreach($params as $k => $v) {
            $str = str_replace('vd_', '', $k);

            $newParams[$str] = $v;
        }

        $params = $newParams;

        unset($params['csrf_name']);
        unset($params['csrf_value']);

        $required = ['option'];

        $errors = [];

        foreach($params as $k => $v) {
            $v = trim($v);
            $params[$k] = $v;
            if (strpos($k, 'option_') !== false) {
                $orig = $k;
                $k = "option";
            }
            if (in_array($k, $required) && empty($v)) {
                switch($k) {
                    default:
                        $errors[] = array(
                            'field' => $orig,
                            'message' => 'VALIDATE.REQUIRED'
                        );
                        break;
                }
            }
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        // if ($classMapper->staticMethod('product', 'where', 'name', $data['name'])->first()) {
        //     $ms->addMessageTranslated('danger', 'PRODUCT.NAME.IN_USE', $data);
        //     $error = true;
        // }

        if (count($errors) > 0) {

            $returner = array(
                'worked' => 0,
                'errors' => $errors
            );

            echo json_encode($returner);

            return $response->withStatus(200);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;
       
        $data2json = [];

        $data2db = [];

        $data2db['product_id'] = $product->id;

        $data = $params;

        foreach($data as $k => $v) {
            if (strpos($k, 'option_') !== false) {
                $orig = str_replace('option_', '', $k);
                $k = "option";
            }
            switch($k) {
                case 'option':
                    $data2json[$orig] = $v;
                    break;
                default:
                    $data2db[$k] = $v;
                    break;
            }
        }

        $array2json = [];

        foreach($data2json as $k => $v) {
            $array2json[] = array(
                'type' => $k,
                'name' => $v
            );
        }

        $json = json_encode($array2json);

        $data2db['data'] = $json;

        $updating = false;

        if (!isset($data2db['variant_id']) || empty($data2db['variant_id'])) {
            unset($data2db['variant_id']);
            $variant = $classMapper->createInstance('product_variant', $data2db);
        } else {
            $variant = ProductVariant::where('id', $data2db['variant_id'])->first();
            $updating = true;
            unset($data2db['variant_id']);
            foreach($data2db as $k => $v) {
                if ($variant->{$k} !== $v) {
                    $variant->{$k} = $v;
                }
            }
        }

        $variant->save();

        // Create activity record
        $this->ci->userActivityLogger->info("User {$currentUser->user_name} ".(($updating !== false) ? 'updated' : 'created')." product variant for {$product->name} ({$product->id}).", [
            'type' => 'product_variant_save',
            'user_id' => $currentUser->id
        ]);

        $returner = array(
            'worked' => 1,
            'product_id' => $product->id,
            'variant_id' => $variant->id,
            'updating' => ($updating) ? 1 : 0
        );

        echo json_encode($returner);
        return $response->withStatus(200);
    }

    public function frontUpdateInventory($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!(count(SupplierUser::where('user_id', $this->current_user->id)->get()) > 0 || $this->current_user->group_id == 3)) {
            throw new ForbiddenException();
        }

        $product = Product::where('id', $args['product_id'])->first();

        if (!is_object($product)) {
            throw new ForbiddenException();
        }

        /* check product belongs to user or user is admin */
        $supplier = Supplier::where('id', $product->supplier_id)->first();

        $checkAccess = SupplierUser::where('user_id', $this->current_user->id)->where('supplier_id', $supplier->id)->first();

        if (!is_object($checkAccess) && $this->current_user->group_id !== 3) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        if(empty($params['newQuantity'])) {
            $params['newQuantity'] = 0;
        }
       
        if (isset($params['variant_id']) && !empty($params['variant_id'])) {
            $variant = ProductVariant::where('id', $params['variant_id'])->first();
            $variant->quantity = $params['newQuantity'];
            $variant->save();
            $msg = "User {$currentUser->user_name} updated quantity of {$variant->name} in product {$product->name} ({$product->id}).";
        } else {
            $product->quantity = $params['newQuantity'];
            $product->save();
            $msg = "User {$currentUser->user_name} updated quantity of product {$product->name} ({$product->id}).";
        }

        // Create activity record
        $this->ci->userActivityLogger->info($msg, [
            'type' => 'product_quantity_update',
            'user_id' => $currentUser->id
        ]);

        $returner = array(
            'worked' => 1,
            'quantity' => $params['newQuantity']
        );

        echo json_encode($returner);
        return $response->withStatus(200);
    }
}
