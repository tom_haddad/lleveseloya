<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Countdown;
use UserFrosting\Sprinkle\Site\Database\Models\CountdownProduct;
use UserFrosting\Sprinkle\Site\Database\Models\Deal;
use UserFrosting\Sprinkle\Site\Database\Models\ProductDeal;
use UserFrosting\Sprinkle\Site\Database\Models\Setting;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

/**
 * CronController Class
 *
 * Automates everything
 */
class CronController extends SimpleController
{
    public function checkCountdowns() 
    {
        $activeCountdowns = Countdown::where('active', 1)->get();
        $curDay = date('w');
        $curHour = ltrim(date('H'), '0');
        $curMin = ltrim(date('i'), '0');
        $curDate = date('Y-m-d');
        $isSiteLive = false;
        $nextCountdown = false;
        $prevCountdown = false;
        $secsTilNext = 0;

        $activeWeeklyCountdowns = Countdown::where('type', 'weekly')->where('active', 1)->get();

        foreach($activeWeeklyCountdowns as $w) {
            if (!$isSiteLive) {
                $curTimeStr = strtotime('01/01/1970 '.date('H:i:s'));
                $startTimeStr =  strtotime('01/01/1970 '.$w->hour.':'.sprintf('%02d',$w->minute).':00');
                $endTimeStr = strtotime('01/01/1970 '.($w->hour + $w->length).':'.sprintf('%02d',$w->minute).':00');
                
                if ($w->day == $curDay && $curTimeStr >= $startTimeStr && $curTimeStr < $endTimeStr) {
                    /* Site is live */
                    $nextCountdown = $w;
                    $isSiteLive = true;
                    $secsTilNext = strtotime($curDate.' '.($w->hour + $w->length).':'.$w->minute.':00') - time();
                } else {
                    /* check if countdown is next one in line */
                    if (!($w->day == $curDay && $curHour > $w->hour)) {
                        $checkSecs = $this->getSeconds($w);
                        if ($checkSecs < $secsTilNext || $secsTilNext == 0) {
                            /* countdown is closest to current time */
                            $nextCountdown = $w;
                            $secsTilNext = $checkSecs;
                        }
                    }
                }
            }
        }

        if (!$isSiteLive) {
            /* site isn't live */
            $activeBespokeCountdowns = Countdown::where('type', 'bespoke')->where('active', 1)->get();
            foreach($activeBespokeCountdowns as $w) {
                if (!$isSiteLive) {
                    /* ensure site live hasn't been set to save time */
                    $toCheck = false;
                    $repeatDay = false;
                    if (date('Y-m-d', strtotime($w->date)) == $curDate) {
                        /* if bespoke is set to todays date */
                        $toCheck = $w;
                    } else {
                        /* check repeating bespoke */
                        if ($w->repeat == "monthly" && $curDay == date('w', strtotime($w->date))) {
                            /* repeating monthly falls on this date */
                            $toCheck = $w;
                            $repeatDay = date('w', $w->date);
                        } else if ($w->repeat == "yearly" && $curDay == date('w', strtotime($w->date)) && $curMonth) {
                            /* repeating annual falls on this date */
                            $toCheck = $w;
                            $repeatDay = date('w', $w->date);
                        }
                    }

                    if (is_object($toCheck)) {
                        $w = $toCheck;
                        if ($curHour >= $w->hour && $curHour + $w->length <= $w->hour + $w->length) {
                            /* Site is live */
                            $nextCountdown = $w;
                            $isSiteLive = true;
                            $secsTilNext = strtotime($curDate.' '.($w->hour + $w->length).':00') - time();
                        } else {
                            if ($curHour < $w->hour) {
                                /* check if countdown is next one in line */
                                $checkSecs = $this->getSeconds($w, date('w', strtotime($w->date)));
                                if ($checkSecs < $secsTilNext || $secsTilNext == 0) {
                                    /* countdown is closest to current time */
                                    $nextCountdown = $w;
                                    $secsTilNext = $checkSecs;
                                }
                            }
                        }
                    }
                }
            }
        }

        $liveDb = Setting::where('name', 'live')->first();
        $next = Setting::where('name', 'nextCountdown')->first();

        $liveDb->value = (($isSiteLive) ? 1 : 0);
        $liveDb->save();

        $next->value = $nextCountdown->id;
        $next->save();

        return true;
    }

    public function checkDeals($siteLive = false, $cd = false) {
        $returner = [];
        if ($siteLive == true) {
            $returner = CountdownProduct::where('countdown_id', $cd->id)->get();
            foreach($returner as $r) {
                if ($r->active !== 1) {
                    $r->active = 1;
                    $r->save();
                }
            }
        }
        return $returner;
    }

    public function doAllChecks()
    {
        $cd = $this->checkCountdowns();
        $d = $this->checkDeals($cd['isSiteLive'], $cd['nextCountdown']);
        $this->removeOldCountdownDeals($cd['nextCountdown']);
        //$em = $this->sendEmails();
        echo 'Successful cron';
        return array(
            'countdown' => $cd,
            'deals' => $d,
            'live' => $cd['isSiteLive']
        );
    }

    public function getSeconds($w, $day = false) {
        $curDay = date('w');
        $curHour = ltrim(date('H'), '0');
        $curMin = ltrim(date('i'), '0');
        $curSec = ltrim(date('s'), '0');

        $checkSecs = 0;

        if (!$day) {
            $day = $w->day;
        }
        if ($curDay <= $day) {
            $dayDiff = $day - $curDay;
        } else {
            $dayDiff = 7 - $curDay + $day;
        }

        $checkSecs = 86400 * $dayDiff;

        if ($curHour <= $w->hour) {
            $hourDiff = $w->hour - $curHour - 1;
        } else {
            $hourDiff = 23 - $curHour + $w->hour;
        }

        $checkSecs = $checkSecs + ($hourDiff * 60 * 60);

        $minDiff = 59 - $curMin;

        $checkSecs = $checkSecs + ($minDiff * 60);

        $secDiff = 59 - $curSec;

        $checkSecs = $checkSecs + $secDiff;

        //echo $dayDiff.' days, '.$hourDiff.' hours, '.$minDiff.' mins, '.$secDiff.' seconds.<br />';

        return $checkSecs;
    }

    public function removeOldCountdownDeals($cd = false) {
        if (is_object($cd)) {
            $removes = CountdownProduct::where('countdown_id', '!=', $cd->id)->where('active', 1)->delete();
        }
        return true;
    }

    public function sendEmails() 
    {

    }
}
