<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

use UserFrosting\Sprinkle\Site\Database\Models\Cart;
use UserFrosting\Sprinkle\Site\Database\Models\Category;
use UserFrosting\Sprinkle\Site\Database\Models\Countdown;
use UserFrosting\Sprinkle\Site\Database\Models\CountdownProduct;
use UserFrosting\Sprinkle\Site\Database\Models\Image;
use UserFrosting\Sprinkle\Site\Database\Models\Order;
use UserFrosting\Sprinkle\Site\Database\Models\OrderItem;
use UserFrosting\Sprinkle\Site\Database\Models\Product;
use UserFrosting\Sprinkle\Site\Database\Models\ProductPhoto;
use UserFrosting\Sprinkle\Site\Database\Models\ProductVariant;
use UserFrosting\Sprinkle\Site\Database\Models\Setting;
use UserFrosting\Sprinkle\Site\Database\Models\Subcategory;
use UserFrosting\Sprinkle\Site\Database\Models\Supplier;
use UserFrosting\Sprinkle\Site\Database\Models\SupplierUser;

/**
 * SiteController Class
 *
 * Implements some common sitewide routes.
 * @author Alex Weissman (https://alexanderweissman.com)
 * @see http://www.userfrosting.com/navigating/#structure
 */
class SiteController extends SimpleController
{
    /**
     * Renders the default home page for UserFrosting.
     *
     * By default, this is the page that non-authenticated users will first see when they navigate to your website's root.
     * Request type: GET
     */

    public function __construct($ci) 
    { 
        parent::__construct($ci);

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $this->current_user = $this->ci->currentUser;

        $config = $this->ci->config;

        $this->cart = [];

        // Access-controlled page
        if (is_object($this->current_user)) {
            $this->cart = Cart::where('user_id', $this->current_user->id)->get();
        }

        $count = 0;
        foreach($this->cart as $c) {
            $count = $count + $c->quantity;
        }
        $data['cart_count'] = $count;

        //$this->isSiteLive = Setting::where('name', 'live')->first()->value;
        $this->isSiteLive = 1;
        $this->nextId = Setting::where('name', 'nextCountdown')->first()->value;

        $cd = Countdown::where('id', $this->nextId)->first();

        if ($cd->type == "weekly") {
            $secsTilNext = $cd->getSeconds();
        } else {
            $secsTilNext = $cd->getSeconds(date('w', strtotime($cd->date)));
        }

        $secsTilNext = 36000;

        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$secsTilNext");

        /* check supplier access -  maybe move this elsewhere */
        $this->supplier_access = (count(SupplierUser::where('user_id', $this->current_user->id)->get()) > 0 || $this->current_user->group_id == 3);

        $this->data = array(
            'cart_count' => $count,
            'countdown' => array(
                'secsTilNext' => $secsTilNext,
                'hours' => sprintf("%02d",$dtF->diff($dtT)->format('%h') + floor($dtF->diff($dtT)->format('%a') * 24)),
                'minutes' => sprintf("%02d",$dtF->diff($dtT)->format('%i')),
                'seconds' => sprintf("%02d",$dtF->diff($dtT)->format('%s'))
            ),
            'supplier_access' => $this->supplier_access
        );
    } 

    public function checkConfPassword($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // GET parameters
        $params = $request->getQueryParams();

        // Load request schema
        $schema = new RequestSchema("schema://requests/check-conf-password.yaml");

         /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        // Validate, and halt on validation errors.
        $validator = new ServerSideValidator($schema, $this->ci->translator);

        if (!$validator->validate($data)) {
            $error = reset(reset($validator->errors()));
            $error = $translator->translate($error, $data);
            return $response->write($error)->withStatus(200);
        }

        /** @var UserFrosting\Sprinkle\Core\Throttle\Throttler $throttler */
        $throttler = $this->ci->throttler;
        $delay = $throttler->getDelay('check_confirm_password_request');

        // Throttle requests
        if ($delay > 0) {
            return $response->withStatus(429);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Log throttleable event
        $throttler->logEvent('check_confirm_password_request');
        
        return $response->write('true')->withStatus(200);

    }

    public function checkEmail($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // GET parameters
        $params = $request->getQueryParams();

        // Load request schema
        $schema = new RequestSchema("schema://requests/check-email.yaml");

         /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        // Validate, and halt on validation errors.
        $validator = new ServerSideValidator($schema, $this->ci->translator);

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $message = $translator->translate('EMAIL.INVALID', $data);
            return $response->write($message)->withStatus(200);
        }

        if (!$validator->validate($data)) {
            // TODO: encapsulate the communication of error messages from ServerSideValidator to the BadRequestException
            $e = new BadRequestException("Missing or malformed request data!");
            foreach ($validator->errors() as $idx => $field) {
                foreach($field as $eidx => $error) {
                    $e->addUserMessage($error);
                }
            }
            throw $e;
        }

        /** @var UserFrosting\Sprinkle\Core\Throttle\Throttler $throttler */
        $throttler = $this->ci->throttler;
        $delay = $throttler->getDelay('check_email_request');

        // Throttle requests
        if ($delay > 0) {
            return $response->withStatus(429);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Log throttleable event
        $throttler->logEvent('check_email_request');



        if ($classMapper->staticMethod('user', 'exists', $data['email'], 'email')) {
            $message = $translator->translate('EMAIL.NOT_AVAILABLE', $data);
            return $response->write($message)->withStatus(200);
        } else {
            return $response->write('true')->withStatus(200);
        }
    }

    public function checkPassword($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // GET parameters
        $params = $request->getQueryParams();

        // Load request schema
        $schema = new RequestSchema("schema://requests/check-password.yaml");

         /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        // Validate, and halt on validation errors.
        $validator = new ServerSideValidator($schema, $this->ci->translator);

        if (!$validator->validate($data)) {
            $error = reset(reset($validator->errors()));
            $error = $translator->translate($error, $data);
            return $response->write($error)->withStatus(200);
        }

        /** @var UserFrosting\Sprinkle\Core\Throttle\Throttler $throttler */
        $throttler = $this->ci->throttler;
        $delay = $throttler->getDelay('check_password_request');

        // Throttle requests
        if ($delay > 0) {
            return $response->withStatus(429);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Log throttleable event
        $throttler->logEvent('check_password_request');
        
        return $response->write('true')->withStatus(200);

    }

    public function getModal($request, $response, $args) {
        $params = $request->getQueryParams();

        $modal = $args['name'];

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        $forms = ['login', 'signup'];

        // Load validation rules
        if (in_array($modal, $forms)) {
            $ext = '.json';
            switch($modal) {
                case 'login':
                    $ext = '.yaml';
                    break;
                case 'signup':
                    $ext = '.yaml';
                    break;
            }
            $schema = new RequestSchema('schema://requests/front/modal/'.$modal.$ext);
            $validator = new JqueryValidationAdapter($schema, $translator);
        }

        $data = [];

        switch($modal) {
            case 'success':
                if (isset($args['id'])) {
                    $order = Order::where('id', $args['id'])->first();
                    if ($order->user_id !== $currentUser->id) {
                        throw new NotFoundException($request, $response);
                    }
                    $data = array(
                        'order' => $order
                    );
                    foreach($this->cart as $c) {
                        $c->delete();
                    }
                }
                break;
            case 'image':
                if (isset($args['id'])) {
                    $id = $args['id'];
                    $product_image = false;
                    if (strpos($id, 'product-') === false) {
                        $image = Image::where('id', $id)->first();
                    } else {
                        $id = str_replace('product-', '', $id);
                        $product_image = ProductPhoto::where('id', $id)->first();
                        if (!is_object($product_image)) {
                            throw new NotFoundException($request, $response);
                        }
                        $image = Image::where('id', $product_image->image_id)->first();
                    }
                    if (!is_object($image)) {
                        throw new NotFoundException($request, $response);
                    }
                    $data = array(
                        'image' => $image,
                        'product_image' => $product_image
                    );
                }
                break;
        }

        return $this->ci->view->render($response, 'modals/front/'.$modal.'.html.twig', [
            'page' => [
                'validators' => (isset($validator)) ? $validator->rules('json', false) : false
            ],
            'data' => $data
        ]);
    }

    public function getSideModal($request, $response, $args) {
        $params = $request->getQueryParams();

        $modal = $args['name'];

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        $forms = ['login', 'signup'];

        // Load validation rules
        if (in_array($modal, $forms)) {
            $ext = '.json';
            switch($modal) {
                case 'login':
                    $ext = '.yaml';
                    break;
                case 'signup':
                    $ext = '.yaml';
                    break;
            }
            $schema = new RequestSchema('schema://requests/front/modal/'.$modal.$ext);
            $validator = new JqueryValidationAdapter($schema, $translator);
        }

        $data = [];

        switch($modal) {
            case 'success':
                if (isset($args['id'])) {
                    $order = Order::where('id', $args['id'])->first();
                    if ($order->user_id !== $currentUser->id) {
                        throw new NotFoundException($request, $response);
                    }
                    $data = array(
                        'order' => $order
                    );
                    foreach($this->cart as $c) {
                        $c->delete();
                    }
                }
                break;
        }

        return $this->ci->view->render($response, 'modals/front/side/'.$modal.'.html.twig', [
            'page' => [
                'validators' => (isset($validator)) ? $validator->rules('json', false) : false
            ],
            'data' => $data
        ]);
    }

    public function pageIndex($request, $response, $args)
    {

        if ($this->isSiteLive && is_object($this->current_user)) {
            $page_uri = 'listings/page.html.twig';
            $url = "pages/front/index.html.twig";
            if (isset($_GET['_'])) {
                 $url = "pages/front/listings/page.html.twig";
            }
            $data = $this->data;
            $items = CountdownProduct::where('countdown_id', $this->nextId)->get();

            $data['featured_items'] = [];
            $data['items'] = [];
            $data['cart'] = $this->cart;
            $data['page_uri'] = $page_uri;
            $data['page_name'] = 'Listings';

            foreach($items as $i) {
                if ($i->product->draft !== 0) {
                    if ($i->product->featured == 1) {
                        $data['featured_items'][] = $i;
                    } else {
                        $data['items'][] = $i;
                    }
                }
            }
            return $this->ci->view->render($response, $url, $data);
        } else {
            $data = $this->data;
            $schema = new RequestSchema('schema://requests/front/modal/signup.yaml');
            $translator = $this->ci->translator;
            $validator = new JqueryValidationAdapter($schema, $translator);
            $data['page']['validators'] = $validator->rules('json', false);
            $data['page']['pw_mismatch'] = $this->ci->translator->translate("PASSWORD_MISMATCH");
            return $this->ci->view->render($response, 'pages/front/timer.html.twig', $data);
        }
    }

    /**
     * Renders a sample "about" page for UserFrosting.
     *
     * Request type: GET
     */
    public function pageAbout($request, $response, $args)
    {
        return $this->ci->view->render($response, 'pages/about.html.twig');
    }

    /**
     * Renders terms of service page.
     *
     * Request type: GET
     */
    public function pageLegal($request, $response, $args)
    {
        return $this->ci->view->render($response, 'pages/legal.html.twig');
    }

    /**
     * Renders privacy page.
     *
     * Request type: GET
     */
    public function pagePrivacy($request, $response, $args)
    {
        return $this->ci->view->render($response, 'pages/privacy.html.twig');
    }

    public function load($request, $response, $args) {

    }

    /**
     * Render the alert stream as a JSON object.
     *
     * The alert stream contains messages which have been generated by calls to `MessageStream::addMessage` and `MessageStream::addMessageTranslated`.
     * Request type: GET
     */
    public function jsonAlerts($request, $response, $args)
    {
        return $response->withJson($this->ci->alerts->getAndClearMessages());
    }

    /**
     * Handle all requests for raw assets.
     * Request type: GET
     */
    public function getAsset($request, $response, $args)
    {
        // By starting this service, we ensure that the timezone gets set.
        $config = $this->ci->config;

        $assetLoader = $this->ci->assetLoader;

        if (!$assetLoader->loadAsset($args['url'])) {
            throw new NotFoundException($request, $response);
        }

        return $response
            ->withHeader('Content-Type', $assetLoader->getType())
            ->withHeader('Content-Length', $assetLoader->getLength())
            ->write($assetLoader->getContent());
    }

    public function supplierIndex($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/index.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Supplier Area'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierProducts($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/products/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/index.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Products'
            // 'products' => $this->user_products
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierProductTable($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $params = $request->getQueryParams();

        $url = 'pages/front/supplier/products/components/table.html.twig';

        $user_products = array();

        if ($this->current_user->group_id == 3) {
            $us = Product::get();
            foreach($us as $u) {
                $user_products[] = array(
                    'name' => $u->name,
                    'inventory' => $u->stock_available,
                    'supplier' => $u->supplier->name,
                    'status' => ($u->approved == 1) ? 'Approved' : 'Pending Approval',
                    'created_at' => strtotime($u->created_at),
                    'product' => $u
                );
            }
        } else {
            if ($this->supplier_access) {
                $su = SupplierUser::where('user_id', $this->current_user->id)->get();
                $sups = array();
                foreach($su as $s) {
                    $sups[] = Supplier::where('id', $s->supplier_id)->first();
                }

                foreach($sups as $s) {
                    foreach($s->products as $p) {
                        $user_products[] = array(
                            'name' => $p->name,
                            'inventory' => $p->stock_available,
                            'supplier' => $p->supplier->name,
                            'status' => ($p->approved == 1) ? 'Approved' : 'Pending Approval',
                            'created_at' => strtotime($u->created_at),
                            'product' => $p
                        );
                    }
                }
            }
        }

        $field = "";
        $value = "";

        usort($user_products, function ($item1, $item2) {
            if ($item1['created_at'] == $item2['created_at']) return 0;
            return $item1['created_at'] > $item2['created_at'] ? -1 : 1;
        });

        foreach($params as $k => $v) {
            switch($k) {
                case 'name':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_products, function($a, $b) {
                            $num = strcmp($a["name"], $b["name"]);
                            if ($num == -1) {
                                return 1;
                            } else if ($num == 0) {
                                return 0;
                            } else {
                                return -1;
                            }
                        });
                    } else {
                        usort($user_products, function($a, $b) {
                            return strcmp($a["name"], $b["name"]);
                        });
                    }
                    break;
                case 'inventory':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_products, function ($item1, $item2) {
                            if ($item1['inventory'] == $item2['inventory']) return 0;
                            return $item1['inventory'] > $item2['inventory'] ? -1 : 1;
                        });
                    } else {
                        usort($user_products, function ($item1, $item2) {
                            if ($item1['inventory'] == $item2['inventory']) return 0;
                            return $item1['inventory'] < $item2['inventory'] ? -1 : 1;
                        });
                    }
                    break;
                case 'supplier':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_products, function($a, $b) {
                            $num = strcmp($a["supplier"], $b["supplier"]);
                            if ($num == -1) {
                                return 1;
                            } else if ($num == 0) {
                                return 0;
                            } else {
                                return -1;
                            }
                        });
                    } else {
                        usort($user_products, function($a, $b) {
                            return strcmp($a["supplier"], $b["supplier"]);
                        });
                    }
                    break;
                case 'search':
                    if (!empty($v)) {
                        $field = $k;
                        $value = $v;
                        $i = 0;
                        foreach($user_products as $p) {
                            $name = strtolower($p['name']);
                            $v = strtolower($v);
                            if (strpos($name, $v) === false) {
                                unset($user_products[$i]);
                            }
                            $i++;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        $page = (isset($params['page'])) ? intval($params['page']) : 1;
        $perpage = 8;
        $total = count($user_products);
        $totalPages = ceil($total / $perpage);
        $page = max($page, 1);
        $page = min($page, $totalPages);

        $offset = ($page - 1) * $perpage;

        if ($offset < 0) $offset = 0;

        $user_products = array_slice($user_products, $offset, $perpage);


        $data = $this->data;

        $extra = [
            'products' => $user_products,
            'prev' => ($page > 1) ? $page - 1 : 0,
            'next' => ($page < $totalPages) ? $page + 1 : 0,
            'cur' => $page,
            'total' => $totalPages,
            'field' => $field,
            'value' => $value
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierInventory($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/products/inventory.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/inventory.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Product Inventory'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierInventoryTable($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $params = $request->getQueryParams();

        $url = 'pages/front/supplier/products/components/inventory_table.html.twig';

        $user_variants = array();

        if ($this->current_user->group_id == 3) {
            $us = Product::get();
            foreach($us as $u) {
                if (count($u->variants) > 0) {
                    foreach($u->variants as $v) {
                        $user_variants[] = array(
                            'name' => $u->name,
                            'quantity' => $v->quantity,
                            'sku' => $v->sku,
                            'created_at' => strtotime($v->created_at),
                            'product' => $u,
                            'variant_name' => $v->html_title,
                            'variant' => $v
                        );
                    }
                } else {
                    $user_variants[] = array(
                        'name' => $u->name,
                        'quantity' => $u->quantity,
                        'sku' => $u->sku,
                        'created_at' => strtotime($u->created_at),
                        'product' => $u,
                        'variant_name' => false,
                        'variant' => false
                    );
                }
            }
        } else {
            if ($this->supplier_access) {
                $su = SupplierUser::where('user_id', $this->current_user->id)->get();
                $sups = array();
                foreach($su as $s) {
                    $sups[] = Supplier::where('id', $s->supplier_id)->first();
                }

                foreach($sups as $s) {
                    foreach($s->products as $p) {
                        if (count($p->variants) > 0) {
                            foreach($p->variants as $v) {
                                $user_variants[] = array(
                                    'name' => $u->name,
                                    'quantity' => $v->quantity,
                                    'sku' => $v->sku,
                                    'created_at' => strtotime($v->created_at),
                                    'product' => $p,
                                    'variant_name' => $v->html_title,
                                    'variant' => $v
                                );
                            }
                        } else {
                            $user_variants[] = array(
                                'name' => $p->name,
                                'quantity' => $p->quantity,
                                'sku' => $p->sku,
                                'created_at' => strtotime($p->created_at),
                                'product' => $p,
                                'variant_name' => false,
                                'variant' => false
                            );
                        }
                    }
                }
            }
        }

        $field = "";
        $value = "";

        usort($user_variants, function ($item1, $item2) {
            if ($item1['created_at'] == $item2['created_at']) return 0;
            return $item1['created_at'] > $item2['created_at'] ? -1 : 1;
        });

        foreach($params as $k => $v) {
            switch($k) {
                case 'name':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_variants, function($a, $b) {
                            $num = strcmp($a["name"], $b["name"]);
                            if ($num == -1) {
                                return 1;
                            } else if ($num == 0) {
                                return 0;
                            } else {
                                return -1;
                            }
                        });
                    } else {
                        usort($user_products, function($a, $b) {
                            return strcmp($a["name"], $b["name"]);
                        });
                    }
                    break;
                case 'quantity':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_variants, function ($item1, $item2) {
                            if ($item1['quantity'] == $item2['quantity']) return 0;
                            return $item1['quantity'] > $item2['quantity'] ? -1 : 1;
                        });
                    } else {
                        usort($user_products, function ($item1, $item2) {
                            if ($item1['quantity'] == $item2['quantity']) return 0;
                            return $item1['quantity'] < $item2['quantity'] ? -1 : 1;
                        });
                    }
                    break;
                case 'search':
                    if (!empty($v)) {
                        $field = $k;
                        $value = $v;
                        $i = 0;
                        foreach($user_variants as $p) {
                            $name = strtolower($p['name']);
                            $vname = strtolower($p['variant_name']);
                            $v = strtolower($v);
                            if (strpos($name, $v) === false && strpos($vname, $v) === false) {
                                unset($user_products[$i]);
                            }
                            $i++;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        $page = (isset($params['page'])) ? intval($params['page']) : 1;
        $perpage = 8;
        $total = count($user_variants);
        $totalPages = ceil($total / $perpage);
        $page = max($page, 1);
        $page = min($page, $totalPages);

        $offset = ($page - 1) * $perpage;

        if ($offset < 0) $offset = 0;

        $user_variants = array_slice($user_variants, $offset, $perpage);


        $data = $this->data;

        $extra = [
            'products' => $user_variants,
            'prev' => ($page > 1) ? $page - 1 : 0,
            'next' => ($page < $totalPages) ? $page + 1 : 0,
            'cur' => $page,
            'total' => $totalPages,
            'field' => $field,
            'value' => $value
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierAddProduct($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/products/single/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/single/index.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Products',
            'category_list' => Category::get()
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierAddVariant($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $url = 'pages/front/supplier/products/single/components/new_variant.html.twig';

        return $this->ci->view->render($response, $url);
    }

    public function supplierAddVariants($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $url = 'pages/front/supplier/products/single/components/new_variants.html.twig';

        return $this->ci->view->render($response, $url);
    }

    public function supplierGetProduct($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $product = $this->getProductFromParams($args);

        if (!$product) {
            throw new NotFoundException($request, $response);
        }

        $checkSupplierUser = SupplierUser::where('supplier_id', $product->supplier_id)->where('user_id', $this->current_user->id)->first();

        if ($this->current_user->group_id !== 3 && !is_object($checkSupplierUser)) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'supplier/products/single/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/single/index.html.twig';
        }

        $data = $this->data;

        $categoryarray = [];

        if (!empty($product->subcategory_id)) {
            $sub = Subcategory::where('id', $product->subcategory_id)->first();
            if (is_object($sub) && !empty($sub->parent_id)) {
                $sub = Subcategory::where('id', $sub->parent_id)->first();
                $categoryarray[] = $sub;
                if (is_object($sub) && !empty($sub->parent_id)) {
                    $sub = Subcategory::where('id', $sub->parent_id)->first();
                    $categoryarray[] = $sub;
                    if (is_object($sub) && !empty($sub->parent_id)) {
                        $sub = Subcategory::where('id', $sub->parent_id)->first();
                        $categoryarray[] = $sub;
                        if (is_object($sub) && !empty($sub->parent_id)) {
                            $sub = Subcategory::where('id', $sub->parent_id)->first();
                            $categoryarray[] = $sub;
                            if (is_object($sub) && !empty($sub->parent_id)) {
                                $sub = Subcategory::where('id', $sub->parent_id)->first();
                                $categoryarray[] = $sub;
                                if (is_object($sub) && !empty($sub->parent_id)) {
                                    $sub = Subcategory::where('id', $sub->parent_id)->first();
                                    $categoryarray[] = $sub;
                                }
                            }
                        }
                    }
                }
            }
            $cat = Category::where('id', $product->category_id)->first();
            $categoryarray[] = $cat;
        }

        $categoryarray = array_reverse($categoryarray);

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Products',
            'category_list' => Category::get(),
            'product' => $product,
            'category_array' => $categoryarray,
            'disabled_fields' => ($this->current_user->group_id == 4)
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierGetProductVariants($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/products/single/variants.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/single/variants.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Products'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierGetProductVariant($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $variant = false;

        if (isset($args['id']) && !empty($args['id']) && isset($args['variant_id']) && !empty($args['variant_id']) && $args['variant_id'] !== "new") {
            $variant = ProductVariant::where('id', $args['variant_id'])->where('product_id', $args['id'])->first();
        }

        $checkSupplierUser = SupplierUser::where('supplier_id', $variant->product->supplier_id)->where('user_id', $this->current_user->id)->first();

        if ($this->current_user->group_id !== 3 && !is_object($checkSupplierUser)) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'supplier/products/single/variant.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/products/single/variant.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Variant',
            'variant' => $variant,
            'variant_list' => ProductVariant::where('product_id', $args['id'])->get()
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierOrders($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }
        $page_uri = 'supplier/orders/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/orders/index.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Orders'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierOrderTable($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $params = $request->getQueryParams();

        $url = 'pages/front/supplier/orders/table.html.twig';

        $user_products = array();

        if ($this->current_user->group_id == 3) {
            $us = Order::get();
            foreach($us as $u) {
                $user_data = (object) $u->user_data;
                $user_orders[] = array(
                    'username' => $user_data->user_name,
                    'email' => $user_data->email,
                    'name' => $user_data->first_name.' '.$user_data->last_name,
                    'items' => count($u->items),
                    'shipping_status' => ($u->status == "complete") ? 'Complete' : 'Incomplete',
                    'payment_status' => ($u->status == "paid" || $u->status == "complete") ? 'Paid' : 'Unpaid',
                    'created_at' => $u->created_at,
                    'total' => $u->getSupplierTotals()['total'],
                    'id' => $u->id
                );
            }
        } else {
            if ($this->supplier_access) {
                $su = SupplierUser::where('user_id', $this->current_user->id)->get();
                $sups = array();
                foreach($su as $s) {
                    $sups[] = $s->supplier_id;
                }

                $order_items = OrderItem::whereIn('supplier_id', $sups)->get();

                $os = [];
                $orders = [];

                foreach($order_items as $i) {
                    if (!in_array($i->order_id, $os)) {
                        $os[] = $i->order_id;
                        $thing = [
                            'order' => $i->order,
                            'supplier_id' => $i->supplier_id
                        ];
                        $orders[] = (Object)$thing;
                    }
                }

                foreach($orders as $u) {
                    $user_data = json_decode($u->user_data);
                    $supplier_id = $u->supplier_id;
                    $u = $u->order;
                    $user_orders[] = array(
                        'username' => $user_data->user_name,
                        'email' => $user_data->email,
                        'name' => $user_data->first_name.' '.$user_data->last_name,
                        'items' => count($u->items),
                        'shipping_status' => ($u->status == "complete") ? 'Complete' : 'Incomplete',
                        'payment_status' => ($u->status == "paid") ? 'Paid' : 'Unpaid',
                        'created_at' => $u->created_at,
                        'total' => $u->getSupplierTotals($supplier_id)['total'],
                        'id' => $u->id
                    );
                }
            }
        }

        $field = "";
        $value = "";

        usort($user_orders, function ($item1, $item2) {
            if ($item1['created_at'] == $item2['created_at']) return 0;
            return $item1['created_at'] > $item2['created_at'] ? -1 : 1;
        });

        foreach($params as $k => $v) {
            switch($k) {
                case 'user':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_orders, function($a, $b) {
                            $num = strcmp($a["username"], $b["username"]);
                            if ($num == -1) {
                                return 1;
                            } else if ($num == 0) {
                                return 0;
                            } else {
                                return -1;
                            }
                        });
                    } else {
                        usort($user_orders, function($a, $b) {
                            return strcmp($a["username"], $b["username"]);
                        });
                    }
                    break;
                case 'items':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_orders, function ($item1, $item2) {
                            if ($item1['items'] == $item2['items']) return 0;
                            return $item1['items'] > $item2['items'] ? -1 : 1;
                        });
                    } else {
                        usort($user_orders, function ($item1, $item2) {
                            if ($item1['items'] == $item2['items']) return 0;
                            return $item1['items'] < $item2['items'] ? -1 : 1;
                        });
                    }
                    break;
                case 'total':
                    $field = $k;
                    $value = $v;
                    if ($v == "desc") {
                        usort($user_orders, function ($item1, $item2) {
                            if ($item1['total'] == $item2['total']) return 0;
                            return $item1['total'] > $item2['total'] ? -1 : 1;
                        });
                    } else {
                        usort($user_orders, function ($item1, $item2) {
                            if ($item1['total'] == $item2['total']) return 0;
                            return $item1['total'] < $item2['total'] ? -1 : 1;
                        });
                    }
                    break;
                case 'from':
                    if (!empty($v)) {
                        $field = $k;
                        $value = $v;
                        $i = 0;
                        foreach($user_orders as $p) {
                            if (strtotime($p['created_at']) < strtotime($v)) {
                                unset($user_orders[$i]);
                            }
                            $i++;
                        }
                    }
                    break;
                case 'to':
                    if (!empty($v)) {
                        $field = $k;
                        $value = $v;
                        $i = 0;
                        foreach($user_orders as $p) {
                            if (strtotime($p['created_at']) > strtotime($v)) {
                                unset($user_orders[$i]);
                            }
                            $i++;
                        }
                    }
                    break;
                case 'search':
                    if (!empty($v)) {
                        $field = $k;
                        $value = $v;
                        $i = 0;
                        foreach($user_orders as $p) {
                            $name = strtolower($p['name']);
                            $email = strtolower($p['email']);
                            $username = strtolower($p['username']);
                            $v = strtolower($v);
                            if (strpos($name, $v) === false && strpos($email, $v) === false && strpos($username, $v) === false) {
                                unset($user_orders[$i]);
                            }
                            $i++;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        $page = (isset($params['page'])) ? intval($params['page']) : 1;
        $perpage = 8;
        $total = count($user_orders);
        $totalPages = ceil($total / $perpage);
        $page = max($page, 1);
        $page = min($page, $totalPages);

        $offset = ($page - 1) * $perpage;

        if ($offset < 0) $offset = 0;

        $user_orders = array_slice($user_orders, $offset, $perpage);


        $data = $this->data;

        $extra = [
            'orders' => $user_orders,
            'prev' => ($page > 1) ? $page - 1 : 0,
            'next' => ($page < $totalPages) ? $page + 1 : 0,
            'cur' => $page,
            'total' => $totalPages,
            'field' => $field,
            'value' => $value
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function supplierGetOrder($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $order = Order::where('id', $args['id'])->first();

        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        if ($this->current_user->group_id == 3) {
            $items = OrderItem::where('order_id', $order->id)->get();
        } else {
            $su = SupplierUser::where('user_id', $this->current_user->id)->get();

            $sups = array();
            foreach($su as $s) {
                $sups[] = $s->supplier_id;
            }

            $items = OrderItem::whereIn('supplier_id', $sups)->where('order_id', $order->id)->get();
        }

        if (count($items) == 0 && $this->current_user->group_id !== 3) {
            throw new NotFoundException($request, $response);
        }

        $checkSupplierUser = SupplierUser::where('supplier_id', $product->supplier_id)->where('user_id', $this->current_user->id)->first();

        if ($this->current_user->group_id !== 3 && !is_object($checkSupplierUser)) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'supplier/orders/single/index.html.twig';
        $url = 'pages/front/supplier.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/supplier/orders/single/index.html.twig';
        }

        $data = $this->data;

        $order->subtotal = number_format($order->getSupplierTotals()['subtotal'], 2);
        $order->tax = number_format($order->getSupplierTotals()['tax'], 2);
        $order->total = number_format($order->getSupplierTotals()['total'], 2);


        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Order #'.$order->id.' Details',
            'order' => $order,
            'items' => $items
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

   public function supplierMarkShipped($request, $response, $args)
    {
        if (!$this->supplier_access) {
            throw new NotFoundException($request, $response);
        }

        $order = Order::where('id', $args['id'])->first();

        if (!$order) {
            throw new NotFoundException($request, $response);
        }

        if ($this->current_user->group_id !== 3) {
            $su = SupplierUser::where('user_id', $this->current_user->id)->get();
            $sups = array();
            foreach($su as $s) {
                $sups[] = $s->supplier_id;
            }

            $items = OrderItem::whereIn('supplier_id', $sups)->where('order_id', $order->id)->get();

            if (count($items) == 0) {
                throw new NotFoundException($request, $response);
            }

            $checkSupplierUser = SupplierUser::where('supplier_id', $product->supplier_id)->where('user_id', $this->current_user->id)->first();

            if ($this->current_user->group_id !== 3 && !is_object($checkSupplierUser)) {
                throw new NotFoundException($request, $response);
            }

        }

        $order->status = "complete";
        $order->save();

        $returner = array(
            'worked' => 1,
            'order_id' => $order->id
        );

        echo json_encode($returner);die();
    }

    protected function getProductFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $product = Product::where('id', $params['id'])->first();

        return $product;
    }


    public function accountSettings($request, $response, $args)
    {
        if (!$this->current_user) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'account/index.html.twig';
        $url = 'pages/front/account.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/account/index.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Account'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function accountHistory($request, $response, $args)
    {
        if (!$this->current_user) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'account/history.html.twig';
        $url = 'pages/front/account.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/account/history.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Account'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }
    public function accountCancelled($request, $response, $args)
    {
        if (!$this->current_user) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'account/cancelled.html.twig';
        $url = 'pages/front/account.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/account/cancelled.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Account'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }

    public function accountOrders($request, $response, $args)
    {
        if (!$this->current_user) {
            throw new NotFoundException($request, $response);
        }

        $page_uri = 'account/orders.html.twig';
        $url = 'pages/front/account.html.twig';

        if (isset($_GET['_'])) {
            $url = 'pages/front/account/orders.html.twig';
        }

        $data = $this->data;

        $extra = [
            'page_uri' => $page_uri,
            'page_name' => 'Manage Account'
        ];

        $data = array_merge($data, $extra);

        return $this->ci->view->render($response, $url, $data);
    }
    
}
