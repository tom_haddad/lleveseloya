<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Image;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

class ImageController extends SimpleController
{
	public function get($request, $response, $args) {
		$file = explode('-', $args['file_name']);

		$alias = $file[0];

		if (isset($file[1])) {
			$size = explode('x', $file[1]);
			$scalex = $size[0];
			$scaley = $size[1];
		}

		$original_id = getIdFromAlias($alias);

		$dbrecord = Image::where('id', $original_id)->first();

		$directory = "app/sprinkles/site/uploads/images/";
		$extension = $args['ext'];

		if (!file_exists($directory.$alias.".jpg")) {
			if (!file_exists($directory.$alias.".png")) {
				throw new \Exception();
			} else {
				$originalextension = "png";
			}
		} elseif (!file_exists($directory.$alias.".png")) {
			if (!file_exists($directory.$alias.".jpg")) {
				throw new \Exception();
			} else {
				$originalextension = "jpg";
			}
		}

		list($width, $height) = getimagesize($directory.$alias.'.'.$extension);

		$proportional = false;
		if (!isset($scalex) || $scalex == 0) {
			$scalex = intval($width);
			$proportional = true;
		}

		if (!isset($scaley) || $scaley == 0) {
			$scaley = intval($height);
			$proportional = true;
		}

		$filename = $directory.$args['file_name'].'.'.$args['ext'];

		$new_file = true;

		if (!file_exists($filename)) {
			$new_file = smart_resize_image($directory.$alias.'.'.$extension, null, $scalex, $scaley, $proportional, $filename, false);
		}

		if ($new_file) {
			switch ($extension) {
				case "jpg":
				header('Content-Type: image/jpeg');
				break;
				case "png":
				header('Content-Type: image/png');
				break;
			}
		}

		header("Content-Disposition: inline; filename=".$args['file_name'].'.'.$args['ext']);

		readfile($filename);

		die();
	}

	public function getModal($request, $response, $args) {
        $url = 'components/images/modal.html.twig';

        $data = [
            'image' => Image::where('id', $args['id'])->first()
        ];

        return $this->ci->view->render($response, $url, $data);
	}


	public function upload($request, $response, $args)
	{
		if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
			throw new \Exception('Invalid parameters.');
		}

				// Check $_FILES['file']['error'] value.
		switch ($_FILES['file']['error']) {
			case UPLOAD_ERR_OK:
			break;
			case UPLOAD_ERR_NO_FILE:
			throw new \Exception('No file sent.');
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
			throw new \Exception('Exceeded filesize limit.');
			default:
			throw new \Exception('Unknown errors.');
		}

		$allowed = array(
			'jpg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif',
		);

		$finfo = new \finfo(FILEINFO_MIME_TYPE);

		if (false === $ext = array_search($finfo->file($_FILES['file']['tmp_name']), $allowed, true)) {
			throw new \Exception('Invalid file format.');
		}

		$classMapper = $this->ci->classMapper;

		if (isset($args['model']) && !empty($args['model'])) {

			$modelName = "UserFrosting\Sprinkle\Site\Database\Models\\".$args['model'];

			if (isset($args['action'])) {
				$object = new $modelName();
				$object->{$args['field']} = $args['id'];
				$object->{$args['field_2']} = $args['id_2'];
			} else {

				$object = $modelName::where('id', $args['id'])->first();

				if (!isset($args['field_2'])) {
					$object = $modelName::where('id', $args['id'])->first();
				} else {
					if (!is_numeric($args['id'])) {
						/* photo object doesn't exist */
						$object = new $modelName();
						$object->{$args['field_2']} = $args['id_2'];
					} else {
						$object = $modelName::where('id', $args['id'])->first();
					}
				}
			}
		}

		if ((isset($object) && is_object($object)) || !(isset($args['model']) && !empty($args['model']))) {

			$authorizer = $this->ci->authorizer;
			$currentUser = $this->ci->currentUser;

			if (!(isset($object) && is_object($object))) {
				$object = false;
			}

			// Access-controlled page
			if ($currentUser->group_id !== 3 && $currentUser->group_id !== 4) {
				throw new ForbiddenException();
			}

			$filename = $_FILES['file']['name'];
			$uploaddir = "app/sprinkles/site/uploads/images";

			$data = array(
				'tmp_name' => $_FILES['file']['tmp_name'],
				'filename' => $filename,
				'extension' => $ext,
			);


			/** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
			$ms = $this->ci->alerts;

			/** @var UserFrosting\Config\Config $config */
			$config = $this->ci->config;

			$returner = [];

						// All checks passed!  log events/activities and create image
						// Begin transaction - DB will be rolled back if an exception occurs
			//Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser, $data, $uploaddir, $ext, $object, $args) {
								// Create the image
				$image = $classMapper->createInstance('image', $data);

								// Store new image to database
				$image->save();

				$alias = $image->alias();

				if (!move_uploaded_file($data['tmp_name'], $uploaddir.'/'.$alias.'.'.$ext)) {
					$image->delete();
					throw new \Exception('Failed to upload file.');
				}

				if (is_object($object)) {
					if (isset($args['action'])) {
						$object->{$args['action']} = $image->id;
					} else {
						$object->{$args['field']} = $image->id;
					}

					$object->save();
				}

				$html = '<div class="image-grid-btn new-img">
					<img src="'.$this->ci->config['site.uri.public'].'/'.$image->getURLBySize(140,140).'" />
					<div class="img-grid-ctrl" style="width:70px;">
						<i class="fa fa-fw fa-search img-zoom" data-id="'.$image->id.'"></i>
						<i class="fa fa-fw fa-trash img-remove" data-id="'.$image->id.'"></i>
					</div>
				</div>';

				$returner = array(
					'id' => $image->id,
					'html' => $html
				);

								// Create activity record
				$this->ci->userActivityLogger->info("User {$currentUser->user_name} uploaded image {$image->filename}.", [
					'type' => 'image_create',
					'user_id' => $currentUser->id
				]);

				$ms->addMessageTranslated('success', 'IMAGE.UPLOAD_SUCCESSFUL', $data);
			//});

			if (is_object($object)) {
				echo json_encode($returner);
				return $response->withStatus(200);
			} else {
				return json_encode($returner);
			}
		} else {
			return $response->withStatus(400);
		}
	}
}

function getIdFromAlias($str) {
	$base = 31;
	$chars = '0123456789bcdfghjklmnpqrstvwxyz';
	$len = strlen($str);
	$val = 0;
	$arr = array_flip(str_split($chars));
	for ($i = 0; $i < $len; ++$i) {
		$val = $val + ($arr[$str[$i]] * pow($base, $len-$i-1));
	}
	$val = $val - 9929;
	if ($val > 2147473718) {
		throw new \Exception("Unable to basedecode a value greater than 2f09zd1 for science reasons.");
	}
	return $val;
}

/**
 * easy image resize function
 * @param  $file - file name to resize
 * @param  $string - The image data, as a string
 * @param  $width - new image width
 * @param  $height - new image height
 * @param  $proportional - keep image proportional, default is no
 * @param  $output - name of the new file (include path if needed)
 * @param  $delete_original - if true the original image will be deleted
 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
 * @param  $quality - enter 1-100 (100 is best quality) default is 100
 * @param  $grayscale - if true, image will be grayscale (default is false)
 * @return boolean|resource
 * smart_resize_image($directory.$alias.'.'.$extension, null, $scalex, $scaley, false, $filename, false);
 */
function smart_resize_image($file,
	$string             = null,
	$width              = 0, 
	$height             = 0, 
	$proportional       = false, 
	$output             = 'file', 
	$delete_original    = true, 
	$use_linux_commands = false,
	$quality            = 100,
	$grayscale          = false
) {
	
	if ( $height <= 0 && $width <= 0 ) return false;
	if ( $file === null && $string === null ) return false;
		# Setting defaults and meta
	$info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
	$image                        = '';
	$final_width                  = 0;
	$final_height                 = 0;
	list($width_old, $height_old) = $info;
	$cropHeight = $cropWidth = 0;
		# Calculating proportionality
	if ($proportional) {
		if      ($width  == 0)  $factor = $height/$height_old;
		elseif  ($height == 0)  $factor = $width/$width_old;
		else                    $factor = min( $width / $width_old, $height / $height_old );
		$final_width  = round( $width_old * $factor );
		$final_height = round( $height_old * $factor );
	}
	else {
		$final_width = ( $width <= 0 ) ? $width_old : $width;
		$final_height = ( $height <= 0 ) ? $height_old : $height;
		$widthX = $width_old / $width;
		$heightX = $height_old / $height;

		$x = min($widthX, $heightX);
		$cropWidth = ($width_old - $width * $x) / 2;
		$cropHeight = ($height_old - $height * $x) / 2;
	}
		# Loading image to memory according to type
	switch ( $info[2] ) {
		case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
		case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
		case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
		default: return false;
	}
	
		# Making the image grayscale, if needed
	if ($grayscale) {
		imagefilter($image, IMG_FILTER_GRAYSCALE);
	}    
	
		# This is the resizing/resampling/transparency-preserving magic
	$image_resized = imagecreatetruecolor( $final_width, $final_height );
	if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
		$transparency = imagecolortransparent($image);
		$palletsize = imagecolorstotal($image);
		if ($transparency >= 0 && $transparency < $palletsize) {
			$transparent_color  = imagecolorsforindex($image, $transparency);
			$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
			imagefill($image_resized, 0, 0, $transparency);
			imagecolortransparent($image_resized, $transparency);
		}
		elseif ($info[2] == IMAGETYPE_PNG) {
			imagealphablending($image_resized, false);
			$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
			imagefill($image_resized, 0, 0, $color);
			imagesavealpha($image_resized, true);
		}
	}
	imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


		# Taking care of original, if needed
	if ( $delete_original ) {
		if ( $use_linux_commands ) exec('rm '.$file);
		else @unlink($file);
	}
		# Preparing a method of providing result
	switch ( strtolower($output) ) {
		case 'browser':
		$mime = image_type_to_mime_type($info[2]);
		header("Content-type: $mime");
		$output = NULL;
		break;
		case 'file':
		$output = $file;
		break;
		case 'return':
		return $image_resized;
		break;
		default:
		break;
	}

		# Writing image according to type to the output destination and image quality
	switch ( $info[2] ) {
		case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
		case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
		case IMAGETYPE_PNG:
		$quality = 9 - (int)((0.9*$quality)/10.0);
		imagepng($image_resized, $output, $quality);
		break;
		default: return false;
	}
	return true;
}