<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Countdown;
use UserFrosting\Sprinkle\Site\Database\Models\CountdownProduct;
use UserFrosting\Sprinkle\Site\Database\Models\Product;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

/**
 * Controller class for countdown-related requests, including listing countdowns, CRUD for countdowns, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class CountdownController extends SimpleController
{

    /**
     * Processes the request to create a new countdown.
     *
     * Processes the request from the countdown creation form, checking that:
     * 1. The countdown name and slug are not already in use;
     * 2. The user has permission to create a new countdown;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see getModalCreateCountdown
     */
    public function create($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_countdown')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/countdown/create-weekly.json');
        if (isset($params['date'])) {
            $schema = new RequestSchema('schema://requests/countdown/create-bespoke.json');
        }


        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // All checks passed!  log events/activities and create countdown
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser) {
            // Create the countdown
            $countdown = $classMapper->createInstance('countdown', $data);

            if (isset($data['day'])) {
                $countdown->type = "weekly";
            } else {
                $countdown->type = "bespoke";
                $countdown->date = date('Y-m-d', strtotime($data['date']));
            }

            if ($data['pm'] == 1) {
                $data['hour'] == $data['hour'] + 12;
            }

            // Store new countdown to database
            $countdown->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created {$countdown->type} countdown {$countdown->id}.", [
                'type' => 'countdown_create',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'COUNTDOWN.CREATION_SUCCESSFUL', $data);
        });

        return $response->withStatus(200);
    }

    /**
     * Processes the request to delete an existing countdown.
     *
     * Deletes the specified countdown.
     * Before doing so, checks that:
     * 1. The user has permission to delete this countdown;
     * 2. The countdown is not currently set as the default for new users;
     * 3. The countdown is empty (does not have any users);
     * 4. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: DELETE
     */
    public function delete($request, $response, $args)
    {
        $countdown = $this->getCountdownFromParams($args);

        // If the countdown doesn't exist, return 404
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_countdown')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $countdownName = $countdown->id;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($countdown, $countdownName, $currentUser) {
            $countdown->delete();
            unset($countdown);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted countdown {$countdownName}.", [
                'type' => 'countdown_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'COUNTDOWN.DELETION_SUCCESSFUL', [
            'name' => $countdownName
        ]);

        return $response->withStatus(200);
    }

    /**
     * Returns a list of Countdowns
     *
     * Generates a list of countdowns, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */
    public function getList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdowns')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('countdown_sprunje', $classMapper, $params);

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getBespokeList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        if (!isset($params['filters'])) {
            $params['filters'] = [];
        }

        if (!isset($params['filters']['type'])) {
            $params['filters']['type'] = "bespoke";
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdowns')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('countdown_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) {
            return $query->with('products');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getWeeklyList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        if (!isset($params['filters'])) {
            $params['filters'] = [];
        }

        if (!isset($params['filters']['type'])) {
            $params['filters']['type'] = "weekly";
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdowns')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('countdown_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) {
            return $query->with('products');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getModalAddUser($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $countdown = $this->getCountdownFromParams($params);

        // If the countdown doesn't exist, return 404
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_countdown')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/countdown/add-user.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/countdown-user.html.twig', [
            'countdown' => $countdown,
            'user_list' => User::get(),
            'form' => [
                'action' => "api/countdowns/g/add-user/{$countdown->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('COUNTDOWN.ADD_USER')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }


    public function getModalConfirmDelete($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $countdown = $this->getCountdownFromParams($params);

        // If the countdown no longer exists, forward to main countdown listing page
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_countdown', [
            'countdown' => $countdown
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this countdown
        // $countCountdownUsers = CountdownUser::where('countdown_id', $countdown->id)->count();
        // if ($countCountdownUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('COUNTDOWN.NOT_EMPTY', $countdown->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-countdown.html.twig', [
            'countdown' => $countdown,
            'form' => [
                'action' => "api/countdowns/g/{$countdown->id}",
            ]
        ]);
    }

    public function getModalConfirmRevoke($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $countdown = Countdown::where('id', $params['countdown_id'])->first();

        $user = User::where('id', $params['user_id'])->first();

        // If the countdown no longer exists, forward to main countdown listing page
        if (!$countdown || !$user) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_countdown')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this countdown
        // $countCountdownUsers = CountdownUser::where('countdown_id', $countdown->id)->count();
        // if ($countCountdownUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('COUNTDOWN.NOT_EMPTY', $countdown->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-remove-access.html.twig', [
            'countdown' => $countdown,
            'user' => $user,
            'form' => [
                'action' => "api/countdowns/g/access/{$countdown->id}/{$user->id}",
            ]
        ]);
    }

    public function getModalCreateBespoke($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_countdown')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy countdown to prepopulate fields
        $countdown = $classMapper->createInstance('countdown', []);

        $countdown->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/countdown/create-bespoke.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/countdown/bespoke.html.twig', [
            'countdown' => $countdown,
            'form' => [
                'action' => 'api/countdowns',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalCreateWeekly($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_countdown')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy countdown to prepopulate fields
        $countdown = $classMapper->createInstance('countdown', []);

        $countdown->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/countdown/create-weekly.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/countdown/weekly.html.twig', [
            'countdown' => $countdown,
            'form' => [
                'action' => 'api/countdowns',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getModalProducts($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $countdown = $this->getCountdownFromParams($params);

        // If the countdown doesn't exist, return 404
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_countdown')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/countdown/products.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/countdown/products.html.twig', [
            'countdown' => $countdown,
            'form' => [
                'action' => "api/countdowns/g/{$countdown->id}/products",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ],
            'products' => Product::where('draft', 0)->where('approved', 1)->get()
        ]);
    }

    public function getModalEdit($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $countdown = $this->getCountdownFromParams($params);

        // If the countdown doesn't exist, return 404
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['name', 'slug', 'icon', 'description'];
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/countdown/create-'.$countdown->type.'.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/countdown/'.$countdown->type.'.html.twig', [
            'countdown' => $countdown,
            'form' => [
                'action' => "api/countdowns/g/{$countdown->id}/".$countdown->type,
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getUsers($request, $response, $args)
    {
        $countdown = $this->getCountdownFromParams($args);

        // If the countdown no longer exists, forward to main countdown listing page
        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown')) {
            throw new ForbiddenException();
        }

        // $checkLink = CountdownUser::where('user_id', $currentUser->id)->where('countdown_id', $suppier->id)->first();

        // var_dump($checkLink);die();

        // if (!is_object($checkLink)) {
        //     throw new ForbiddenException();
        // }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('countdown_sprunje', $classMapper, $params);
        $sprunje->extendQuery(function ($query) use ($countdown) {
            return $countdown->users()->with('user');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Renders a page displaying a countdown's information, in read-only mode.
     *
     * This checks that the currently logged-in user has permission to view the requested countdown's info.
     * It checks each field individually, showing only those that you have permission to view.
     * This will also try to show buttons for deleting, and editing the countdown.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageInfo($request, $response, $args)
    {
        $countdown = $this->getCountdownFromParams($args);

        // If the countdown no longer exists, forward to main countdown listing page
        if (!$countdown) {
            $redirectPage = $this->ci->router->pathFor('uri_countdowns');
            return $response->withRedirect($redirectPage, 404);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown', [
                'countdown' => $countdown
            ])) {
            throw new ForbiddenException();
        }

        // Determine fields that currentUser is authorized to view
        $fieldNames = ['name', 'slug', 'icon', 'description'];

        // Generate form
        $fields = [
            'hidden' => []
        ];

        foreach ($fieldNames as $field) {
            if (!$authorizer->checkAccess($currentUser, 'view_countdown_field', [
                'countdown' => $countdown,
                'property' => $field
            ])) {
                $fields['hidden'][] = $field;
            }
        }

        // Determine buttons to display
        $editButtons = [
            'hidden' => []
        ];

        if (!$authorizer->checkAccess($currentUser, 'update_countdown_field', [
            'countdown' => $countdown,
            'fields' => ['name', 'slug', 'icon', 'description']
        ])) {
            $editButtons['hidden'][] = 'edit';
        }

        if (!$authorizer->checkAccess($currentUser, 'delete_countdown', [
            'countdown' => $countdown
        ])) {
            $editButtons['hidden'][] = 'delete';
        }

        return $this->ci->view->render($response, 'pages/countdown.html.twig', [
            'countdown' => $countdown,
            'fields' => $fields,
            'tools' => $editButtons
        ]);
    }

    /**
     * Renders the countdown listing page.
     *
     * This page renders a table of countdowns, with dropdown menus for admin actions for each countdown.
     * Actions typically include: edit countdown, delete countdown.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageList($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_countdowns')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'pages/countdowns.html.twig');
    }

    public function updateProducts($request, $response, $args)
    {
        // Get the countdown based on slug in URL
        $countdown = $this->getCountdownFromParams($args);

        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/countdown/products.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this countdown
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown', [
            'countdown' => $countdown,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $countdown, $currentUser, $classMapper) {

            CountdownProduct::where('countdown_id', $countdown->id)->delete();

            foreach ($data['products'] as $product) {
                $d = array(
                    'countdown_id' => $countdown->id,
                    'product_id' => $product,
                    'active' => 0
                );

                $product = $classMapper->createInstance('countdown_product', $d);
                $product->save();    
            }

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated products on countdown ID {$countdown->id}.", [
                'type' => 'countdown_update_products',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'COUNTDOWN.UPDATE', [
            'name' => $countdown->name
        ]);

        return $response->withStatus(200);
    }


    public function updateInfoBespoke($request, $response, $args)
    {
        // Get the countdown based on slug in URL
        $countdown = $this->getCountdownFromParams($args);

        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/countdown/create-bespoke.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this countdown
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown', [
            'countdown' => $countdown,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $countdown, $currentUser) {
            // Update the countdown and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $countdown->$name) {
                    if ($name == "hour") {
                        if ($data['pm'] == 1) {
                            $countdown->$name = $value + 12;
                        } else {
                            $countdown->$name = $value;
                        }
                    } else if ($name == "date") {
                        $countdown->date = date('Y-m-d', strtotime($value));
                    } else {
                        $countdown->$name = $value;
                    }
                }
            }

            $countdown->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for countdown {$countdown->id}.", [
                'type' => 'countdown_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'COUNTDOWN.UPDATE', [
            'name' => $countdown->name
        ]);

        return $response->withStatus(200);
    }

    public function updateInfoWeekly($request, $response, $args)
    {
        // Get the countdown based on slug in URL
        $countdown = $this->getCountdownFromParams($args);

        if (!$countdown) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/countdown/create-weekly.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this countdown
        if (!$authorizer->checkAccess($currentUser, 'uri_countdown', [
            'countdown' => $countdown,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $countdown, $currentUser) {
            // Update the countdown and generate success messages
            foreach ($data as $name => $value) {
                if ($name == "hour") {
                    if ($data['pm'] == 1) {
                        $countdown->$name = $value + 12;
                    } else {
                        $countdown->$name = $value;
                    }
                } else if ($value != $countdown->$name) {
                    $countdown->$name = $value;
                }
            }

            $countdown->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for countdown {$countdown->id}.", [
                'type' => 'countdown_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'COUNTDOWN.UPDATE', [
            'name' => $countdown->name
        ]);

        return $response->withStatus(200);
    }

    protected function getCountdownFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $countdown = Countdown::where('id', $params['id'])->first();

        return $countdown;
    }
}
