<?php
namespace UserFrosting\Sprinkle\Site\Controller;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\NotFoundException;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Sprinkle\Site\Database\Models\Supplier;
use UserFrosting\Sprinkle\Site\Database\Models\SupplierUser;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Facades\Debug;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\HttpException;

/**
 * Controller class for supplier-related requests, including listing suppliers, CRUD for suppliers, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class SupplierController extends SimpleController
{

    public function addUser($request, $response, $args)
    {
        // Get the supplier based on slug in URL
        $supplier = $this->getSupplierFromParams($args);

        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/supplier/add-user.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        $user = User::where('id', $data['user_id'])->first();

        if (!$user) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this supplier
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check user already exists in supplier
        $check = SupplierUser::where('user_id', $data['user_id'])->where('supplier_id', $supplier->id)->first();

        if (is_object($check)) {
            $ms->addMessageTranslated('danger', 'SUPPLIER.USER_EXISTS', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $supplier, $currentUser, $user, $classMapper) {
            // Update the supplier and generate success messages
            $supplier_user = $classMapper->createInstance('supplier_user', $data);
            $supplier_user->supplier_id = $supplier->id;
            $supplier_user->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} granted {$user->user_name} access to {$supplier->name}.", [
                'type' => 'supplier_add_user',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'SUPPLIER.USER_ADDED', [
            'name' => $supplier->name,
            'user' => $user->user_name
        ]);

        return $response->withStatus(200);
    }

    /**
     * Processes the request to create a new supplier.
     *
     * Processes the request from the supplier creation form, checking that:
     * 1. The supplier name and slug are not already in use;
     * 2. The user has permission to create a new supplier;
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: POST
     * @see getModalCreateSupplier
     */
    public function create($request, $response, $args)
    {
        // Get POST parameters: name, slug, icon, description
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/supplier/create.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if ($classMapper->staticMethod('supplier', 'where', 'name', $data['name'])->first()) {
            $ms->addMessageTranslated('danger', 'SUPPLIER.NAME.IN_USE', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // All checks passed!  log events/activities and create supplier
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($classMapper, $data, $ms, $config, $currentUser) {
            // Create the supplier
            $supplier = $classMapper->createInstance('supplier', $data);

            // Store new supplier to database
            $supplier->save();

            foreach ($data as $name => $value) {
                if (!isset($supplier->$name)) {
                    $supplier->setMeta($name, $value);
                }
            }

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created supplier {$supplier->name}.", [
                'type' => 'supplier_create',
                'user_id' => $currentUser->id
            ]);

            $ms->addMessageTranslated('success', 'SUPPLIER.CREATION_SUCCESSFUL', $data);
        });

        return $response->withStatus(200);
    }

    /**
     * Processes the request to delete an existing supplier.
     *
     * Deletes the specified supplier.
     * Before doing so, checks that:
     * 1. The user has permission to delete this supplier;
     * 2. The supplier is not currently set as the default for new users;
     * 3. The supplier is empty (does not have any users);
     * 4. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: DELETE
     */
    public function delete($request, $response, $args)
    {
        $supplier = $this->getSupplierFromParams($args);

        // If the supplier doesn't exist, return 404
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $supplierName = $supplier->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($supplier, $supplierName, $currentUser) {
            $supplier->delete();
            unset($supplier);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted supplier {$supplierName}.", [
                'type' => 'supplier_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'SUPPLIER.DELETION_SUCCESSFUL', [
            'name' => $supplierName
        ]);

        return $response->withStatus(200);
    }

    /**
     * Returns a list of Suppliers
     *
     * Generates a list of suppliers, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     */
    public function getList($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_suppliers')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('supplier_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) {
            return $query->with('products');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getModalAddUser($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $supplier = $this->getSupplierFromParams($params);

        // If the supplier doesn't exist, return 404
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        if (!$authorizer->checkAccess($currentUser, 'uri_supplier')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/supplier/add-user.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/supplier-user.html.twig', [
            'supplier' => $supplier,
            'user_list' => User::get(),
            'form' => [
                'action' => "api/suppliers/g/add-user/{$supplier->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('SUPPLIER.ADD_USER')
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }


    public function getModalConfirmDelete($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $supplier = $this->getSupplierFromParams($params);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_supplier', [
            'supplier' => $supplier
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this supplier
        // $countSupplierUsers = SupplierUser::where('supplier_id', $supplier->id)->count();
        // if ($countSupplierUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('SUPPLIER.NOT_EMPTY', $supplier->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-delete-supplier.html.twig', [
            'supplier' => $supplier,
            'form' => [
                'action' => "api/suppliers/g/{$supplier->id}",
            ]
        ]);
    }

    public function getModalConfirmRevoke($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $supplier = Supplier::where('id', $params['supplier_id'])->first();

        $user = User::where('id', $params['user_id'])->first();

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier || !$user) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if there are any users in this supplier
        // $countSupplierUsers = SupplierUser::where('supplier_id', $supplier->id)->count();
        // if ($countSupplierUsers > 0) {
        //     $e = new BadRequestException();
        //     $e->addUserMessage('SUPPLIER.NOT_EMPTY', $supplier->toArray());
        //     throw $e;
        // }

        return $this->ci->view->render($response, 'modals/confirm-remove-access.html.twig', [
            'supplier' => $supplier,
            'user' => $user,
            'form' => [
                'action' => "api/suppliers/g/access/{$supplier->id}/{$user->id}",
            ]
        ]);
    }

    /**
     * Renders the modal form for creating a new supplier.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalCreate($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'create_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Create a dummy supplier to prepopulate fields
        $supplier = $classMapper->createInstance('supplier', []);

        $supplier->icon = 'fa fa-user';

        $fieldNames = ['name'];
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/supplier/create.json');
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/supplier.html.twig', [
            'supplier' => $supplier,
            'form' => [
                'action' => 'api/suppliers',
                'method' => 'POST',
                'fields' => $fields,
                'submit_text' => $translator->translate("CREATE")
            ],
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    /**
     * Renders the modal form for editing an existing supplier.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     */
    public function getModalEdit($request, $response, $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $supplier = $this->getSupplierFromParams($params);

        // If the supplier doesn't exist, return 404
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var UserFrosting\I18n\MessageTranslator $translator */
        $translator = $this->ci->translator;

        $fieldNames = ['name', 'slug', 'icon', 'description'];
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier')) {
            throw new ForbiddenException();
        }

        // Generate form
        $fields = [
            'hidden' => [],
            'disabled' => []
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/supplier/edit-info.json');
        $validator = new JqueryValidationAdapter($schema, $translator);

        return $this->ci->view->render($response, 'modals/supplier.html.twig', [
            'supplier' => $supplier,
            'form' => [
                'action' => "api/suppliers/g/{$supplier->id}",
                'method' => 'PUT',
                'fields' => $fields,
                'submit_text' => $translator->translate('UPDATE')
            ],
            'meta' => $supplier->getMeta(),
            'page' => [
                'validators' => $validator->rules('json', false)
            ]
        ]);
    }

    public function getProducts($request, $response, $args)
    {
        $supplier = $this->getSupplierFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier')) {
            throw new ForbiddenException();
        }

        // $checkLink = SupplierUser::where('user_id', $currentUser->id)->where('supplier_id', $suppier->id)->first();

        // var_dump($checkLink);die();

        // if (!is_object($checkLink)) {
        //     throw new ForbiddenException();
        // }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('supplier_sprunje', $classMapper, $params);
        $sprunje->extendQuery(function ($query) use ($supplier) {
            return $supplier->products()->with('supplier');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    public function getUsers($request, $response, $args)
    {
        $supplier = $this->getSupplierFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier')) {
            throw new ForbiddenException();
        }

        // $checkLink = SupplierUser::where('user_id', $currentUser->id)->where('supplier_id', $suppier->id)->first();

        // var_dump($checkLink);die();

        // if (!is_object($checkLink)) {
        //     throw new ForbiddenException();
        // }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('supplier_sprunje', $classMapper, $params);
        $sprunje->extendQuery(function ($query) use ($supplier) {
            return $supplier->users()->with('user');
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Renders a page displaying a supplier's information, in read-only mode.
     *
     * This checks that the currently logged-in user has permission to view the requested supplier's info.
     * It checks each field individually, showing only those that you have permission to view.
     * This will also try to show buttons for deleting, and editing the supplier.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageInfo($request, $response, $args)
    {
        $supplier = $this->getSupplierFromParams($args);

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier) {
            $redirectPage = $this->ci->router->pathFor('uri_suppliers');
            return $response->withRedirect($redirectPage, 404);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier', [
                'supplier' => $supplier
            ])) {
            throw new ForbiddenException();
        }

        // Determine fields that currentUser is authorized to view
        $fieldNames = ['name', 'slug', 'icon', 'description'];

        // Generate form
        $fields = [
            'hidden' => []
        ];

        foreach ($fieldNames as $field) {
            if (!$authorizer->checkAccess($currentUser, 'view_supplier_field', [
                'supplier' => $supplier,
                'property' => $field
            ])) {
                $fields['hidden'][] = $field;
            }
        }

        // Determine buttons to display
        $editButtons = [
            'hidden' => []
        ];

        if (!$authorizer->checkAccess($currentUser, 'update_supplier_field', [
            'supplier' => $supplier,
            'fields' => ['name', 'slug', 'icon', 'description']
        ])) {
            $editButtons['hidden'][] = 'edit';
        }

        if (!$authorizer->checkAccess($currentUser, 'delete_supplier', [
            'supplier' => $supplier
        ])) {
            $editButtons['hidden'][] = 'delete';
        }

        return $this->ci->view->render($response, 'pages/supplier.html.twig', [
            'supplier' => $supplier,
            'fields' => $fields,
            'tools' => $editButtons
        ]);
    }

    /**
     * Renders the supplier listing page.
     *
     * This page renders a table of suppliers, with dropdown menus for admin actions for each supplier.
     * Actions typically include: edit supplier, delete supplier.
     * This page requires authentication.
     * Request type: GET
     */
    public function pageList($request, $response, $args)
    {
        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_suppliers')) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'pages/suppliers.html.twig');
    }

     /**
     * Revoke user's access to supplier products
     */
    public function revokeAccess($request, $response, $args)
    {
        $supplier = Supplier::where('id', $args['supplier_id'])->first();
        $user = User::where('id', $args['user_id'])->first();

        // If the supplier no longer exists, forward to main supplier listing page
        if (!$supplier || !$user) {
            throw new NotFoundException($request, $response);
        }

        $link = SupplierUser::where('user_id', $user->id)->where('supplier_id', $supplier->id)->first();

        if (!$link) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'delete_supplier')) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $supplierName = $supplier->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($supplier, $supplierName, $currentUser, $link, $user) {
            $link->delete();
            unset($link);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} removed {$user->user_name}'s access to {$supplierName}.", [
                'type' => 'supplier_user_delete',
                'user_id' => $currentUser->id
            ]);
        });

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'SUPPLIER.REMOVE_ACCESS_SUCCESSFUL', [
            'name' => $supplierName,
            'user' => $user->user_name
        ]);

        return $response->withStatus(200);
    }

    /**
     * Processes the request to update an existing supplier's details.
     *
     * Processes the request from the supplier update form, checking that:
     * 1. The supplier name/slug are not already in use;
     * 2. The user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication (and should generally be limited to admins or the root user).
     * Request type: PUT
     * @see getModalSupplierEdit
     */
    public function updateInfo($request, $response, $args)
    {
        // Get the supplier based on slug in URL
        $supplier = $this->getSupplierFromParams($args);

        if (!$supplier) {
            throw new NotFoundException($request, $response);
        }

        /** @var UserFrosting\Config\Config $config */
        $config = $this->ci->config;

        // Get PUT parameters: (name, slug, icon, description)
        $params = $request->getParsedBody();

        /** @var UserFrosting\Sprinkle\Core\MessageStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/supplier/edit-info.json');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            $fieldNames[] = $name;
        }

        /** @var UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var UserFrosting\Sprinkle\Account\Database\Models\User $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit submitted fields for this supplier
        if (!$authorizer->checkAccess($currentUser, 'uri_supplier', [
            'supplier' => $supplier,
            'fields' => array_values(array_unique($fieldNames))
        ])) {
            throw new ForbiddenException();
        }

        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Check if name or slug already exists
        if (
            isset($data['name']) &&
            $data['name'] != $supplier->name &&
            $classMapper->staticMethod('supplier', 'where', 'name', $data['name'])->first()
        ) {
            $ms->addMessageTranslated('danger', 'SUPPLIER.NAME.IN_USE', $data);
            $error = true;
        }

        if ($error) {
            return $response->withStatus(400);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction( function() use ($data, $supplier, $currentUser) {
            // Update the supplier and generate success messages
            foreach ($data as $name => $value) {
                if (isset($supplier->$name)) {
                    if ($value != $supplier->$name) {
                        $supplier->$name = $value;
                    }
                } else {
                    $supplier->setMeta($name, $value);
                }
            }

            $supplier->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated details for supplier {$supplier->name}.", [
                'type' => 'supplier_update_info',
                'user_id' => $currentUser->id
            ]);
        });

        $ms->addMessageTranslated('success', 'SUPPLIER.UPDATE', [
            'name' => $supplier->name
        ]);

        return $response->withStatus(200);
    }

    protected function getSupplierFromParams($params)
    {
        $classMapper = $this->ci->classMapper;

        $supplier = Supplier::where('id', $params['id'])->first();

        return $supplier;
    }
}
