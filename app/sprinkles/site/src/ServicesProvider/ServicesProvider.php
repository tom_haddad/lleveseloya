<?php
/**
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @copyright Copyright (c) 2013-2016 Alexander Weissman
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/licenses/UserFrosting.md (MIT License)
 */
namespace UserFrosting\Sprinkle\Site\ServicesProvider;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;

class ServicesProvider
{
    /**
     * Register extended user fields services.
     *
     * @param Container $container A DI container implementing ArrayAccess and container-interop.
     */
    public function register($container)
    {
        /**
         * Extend the 'classMapper' service to register model classes.
         *
         * Mappings added: OwlerUser
         */
        $container->extend('classMapper', function ($classMapper, $c) {
            $classMapper->setClassMapping('category_merje', 'UserFrosting\Sprinkle\Site\Sprunje\CategoryMerje');
            $classMapper->setClassMapping('category', 'UserFrosting\Sprinkle\Site\Database\Models\Category');
            $classMapper->setClassMapping('subcategory', 'UserFrosting\Sprinkle\Site\Database\Models\Subcategory');
            $classMapper->setClassMapping('countdown', 'UserFrosting\Sprinkle\Site\Database\Models\Countdown');
            $classMapper->setClassMapping('countdown_product', 'UserFrosting\Sprinkle\Site\Database\Models\CountdownProduct');
            $classMapper->setClassMapping('countdown_sprunje', 'UserFrosting\Sprinkle\Site\Sprunje\CountdownSprunje');
            $classMapper->setClassMapping('image', 'UserFrosting\Sprinkle\Site\Database\Models\Image');
            //$classMapper->setClassMapping('user', 'UserFrosting\Sprinkle\Site\Database\Models\UsermetaUser');
            $classMapper->setClassMapping('supplier_sprunje', 'UserFrosting\Sprinkle\Site\Sprunje\SupplierSprunje');
            $classMapper->setClassMapping('supplier', 'UserFrosting\Sprinkle\Site\Database\Models\Supplier');
            $classMapper->setClassMapping('supplier_user', 'UserFrosting\Sprinkle\Site\Database\Models\SupplierUser');

            $classMapper->setClassMapping('product_sprunje', 'UserFrosting\Sprinkle\Site\Sprunje\ProductSprunje');
            $classMapper->setClassMapping('product_variant_sprunje', 'UserFrosting\Sprinkle\Site\Sprunje\ProductVariantSprunje');
            $classMapper->setClassMapping('product', 'UserFrosting\Sprinkle\Site\Database\Models\Product');
            //$classMapper->setClassMapping('product_category', 'UserFrosting\Sprinkle\Site\Database\Models\ProductCategory');
            $classMapper->setClassMapping('product_meta', 'UserFrosting\Sprinkle\Site\Database\Models\ProductMeta');
            $classMapper->setClassMapping('product_photo', 'UserFrosting\Sprinkle\Site\Database\Models\ProductPhoto');
            $classMapper->setClassMapping('product_variant', 'UserFrosting\Sprinkle\Site\Database\Models\ProductVariant');
            $classMapper->setClassMapping('product_view', 'UserFrosting\Sprinkle\Site\Database\Models\ProductView');

            $classMapper->setClassMapping('order', 'UserFrosting\Sprinkle\Site\Database\Models\Order');
            $classMapper->setClassMapping('order_item', 'UserFrosting\Sprinkle\Site\Database\Models\OrderItem');
            $classMapper->setClassMapping('order_sprunje', 'UserFrosting\Sprinkle\Site\Sprunje\OrderSprunje');
            $classMapper->setClassMapping('payment', 'UserFrosting\Sprinkle\Site\Database\Models\Payment');
            return $classMapper;
        });
        
        /**
         * Initialize Eloquent Capsule, which provides the database layer for UF.
         *
         * @todo construct the individual objects rather than using the facade
         */
        $container['db'] = function ($c) {
            $config = $c->config;
            $capsule = new Capsule;
            foreach ($config['db'] as $name => $dbConfig) {
                $capsule->addConnection($dbConfig, $name);
            }
            $capsule->setEventDispatcher(new Dispatcher(new Container));
            // Register as global connection
            $capsule->setAsGlobal();
            // Start Eloquent
            $capsule->bootEloquent();
            return $capsule;
        };
    }
}