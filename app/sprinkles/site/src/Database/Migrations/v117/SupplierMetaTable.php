<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Supplier Meta Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class SupplierMetaTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('supplier_meta')) {
            $this->schema->create('supplier_meta', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('supplier_id')->unsigned();
                $table->string('name', 255)->nullable();
                $table->string('value', 2555)->nullable();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
                $table->index('supplier_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('supplier_meta');
    }
}
