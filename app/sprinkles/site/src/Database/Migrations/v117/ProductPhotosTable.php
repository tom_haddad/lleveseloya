<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Product Photos Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class ProductPhotosTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('product_photos')) {
            $this->schema->create('product_photos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('image_id')->unsigned()->nullable();
                $table->integer('product_id')->unsigned()->nullable();
                $table->integer('variant_id')->unsigned()->nullable();
                $table->string('caption', 255)->nullable();
                $table->integer('highlight')->nullable();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->index('image_id');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->index('product_id');
                $table->foreign('variant_id')->references('id')->on('product_variants')->onDelete('cascade');
                $table->index('variant_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('product_photos');
    }
}
