<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Countdowns Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class CountdownsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('countdowns')) {
            $this->schema->create('countdowns', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('day');
                $table->integer('hour');
                $table->integer('minute');
                $table->integer('pm');
                $table->datetime('date')->nullable();
                $table->string('repeat', 255)->nullable();
                $table->string('type', 255);
                $table->integer('active');

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('countdowns');
    }
}
