<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Cart Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class CartTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('carts')) {
            $this->schema->create('carts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->integer('variant_id')->unsigned();
                $table->integer('quantity');
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->index('product_id');
                $table->foreign('variant_id')->references('id')->on('product_variants')->onDelete('cascade');
                $table->index('variant_id');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->index('user_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('carts');
    }
}
