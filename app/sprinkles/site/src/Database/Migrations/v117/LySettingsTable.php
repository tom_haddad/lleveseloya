<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Settings Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class LySettingsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('ly_settings')) {
            $this->schema->create('ly_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->varchar('name', 45)->unsigned();
                $table->varchar('value', 45)->unsigned();
                $table->string('status', 255);

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('ly_settings');
    }
}
