<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Images Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class ImagesTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('images')) {
            $this->schema->create('images', function (Blueprint $table) {
                $table->increments('id');
                $table->string('filename', 255)->nullable();
                $table->string('extension', 255)->nullable();
                $table->integer('height')->nullable();
                $table->integer('width')->nullable();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
            });
        } 
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('images');
    }
}
