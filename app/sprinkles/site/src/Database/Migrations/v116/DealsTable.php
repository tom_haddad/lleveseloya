<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Deals Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class DealsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('deals')) {
            $this->schema->create('deals', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->integer('variant_id')->unsigned()->nullable();
                $table->integer('quantity');
                $table->decimal('price', 16, 2);
                $table->decimal('percentage', 4, 2)->nullable();
                $table->integer('active');
                $table->integer('featured');

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->index('product_id');
                $table->foreign('variant_id')->references('id')->on('product_variants')->onDelete('cascade');
                $table->index('product_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('deals');
    }
}
