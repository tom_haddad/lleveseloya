<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Products Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class ProductsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('products')) {
            $this->schema->create('products', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('supplier_id')->unsigned();
                $table->string('name', 255)->nullable();
                $table->string('url', 255)->nullable();
                $table->text('description')->nullable();
                $table->decimal('price', 16, 2);
                $table->integer('draft')->nullable();
                $table->integer('approved')->nullable();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
                $table->index('supplier_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('products');
    }
}
