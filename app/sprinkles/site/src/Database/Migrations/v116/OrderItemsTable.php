<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Order Items Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class OrderItemsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('order_items')) {
            $this->schema->create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('deal_id')->unsigned()->nullable();
                $table->integer('product_id')->unsigned()->nullable();
                $table->integer('supplier_id')->unsigned()->nullable();
                $table->integer('order_id')->unsigned();
                $table->integer('reference');
                $table->text('deal_data');
                $table->text('product_data');
                $table->text('supplier_data');
                $table->decimal('amount_paid', 16, 2);
                $table->string('status', 255);

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';

                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('set null');
                $table->index('deal_id');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
                $table->index('product_id');
                $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('set null');
                $table->index('supplier_id');
                $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
                $table->index('order_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('order_items');
    }
}
