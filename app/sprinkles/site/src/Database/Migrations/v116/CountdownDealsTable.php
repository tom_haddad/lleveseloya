<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Countdown deals Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class CountdownDealsTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('countdown_deals')) {
            $this->schema->create('countdown_deals', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('deal_id')->unsigned();
                $table->integer('countdown_id')->unsigned();
                $table->integer('active');

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';

                $table->foreign('deal_id')->references('id')->on('deals')->onDelete('cascade');
                $table->index('deal_id');
                $table->foreign('countdown_id')->references('id')->on('countdowns')->onDelete('cascade');
                $table->index('countdown_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('countdown_deals');
    }
}
