<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Suppliers Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class SuppliersTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('suppliers')) {
            $this->schema->create('suppliers', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('image_id')->unsigned()->nullable();
                $table->string('name', 255)->nullable();
                $table->integer('rating')->nullable();
                $table->string('type', 45)->nullable();
                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';
                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->index('image_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('suppliers');
    }
}
