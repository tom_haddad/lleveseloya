<?php

namespace UserFrosting\Sprinkle\Site\Database\Migrations\v116;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;
use UserFrosting\System\Bakery\Migration;

/**
 * Orders Table
 * Version 1.1.6
 *
 * @extends Migration
 * @author Tom Haddad
 */
class OrdersTable extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('orders')) {
            $this->schema->create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned()->nullable();
                $table->integer('payment_id')->unsigned();
                $table->string('status', 255);

                $table->timestamps();

                $table->engine = 'InnoDB';
                $table->collation = 'utf8_unicode_ci';
                $table->charset = 'utf8';

                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
                $table->index('user_id');
                $table->foreign('payment_id')->references('id')->on('payments')->onDelete('set null');
                $table->index('payment_id');
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public function down()
    {
        $this->schema->drop('orders');
    }
}
