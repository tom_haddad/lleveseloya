<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

class Usermeta extends Model {

    public $timestamps = true;

    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "user_meta";

    protected $fillable = [
        "user_id",
        "name",
        "value"
    ];

    /**
     * Directly joins the related user, so we can do things like sort, search, paginate, etc.
     */
    public function scopeJoinUser($query)
    {
        $query = $query->select('user_meta.*');

        $query = $query->leftJoin('users', 'user_meta.user_id', '=', 'users.id');

        return $query;
    }

    /**
     * Get the user associated with this owler.
     */
    public function user()
    {
        /** @var UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = static::$ci->classMapper;

        return $this->belongsTo($classMapper->getClassMapping('user'), 'user_id');
    }
}