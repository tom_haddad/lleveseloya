<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Supplier User Class
 *
 * Links suppliers to users.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class SupplierUser extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "supplier_users";

    protected $fillable = [
        "supplier_id",
        "user_id"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this supplier from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the supplier
        $result = parent::delete();

        return $result;
    }

    /**
     * Get supplier object
     */
    public function supplier()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Supplier');
    }

    /**
     * Get user object
     */
    public function user()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Account\Database\Models\User');
    }
}
