<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use UserFrosting\Sprinkle\Site\Database\Models\SupplierMeta;
use UserFrosting\Sprinkle\Account\Database\Models\User;

/**
 * Supplier Class
 *
 * Represents a supplier object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Supplier extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "suppliers";

    protected $fillable = [
        "name",
        "logo",
        "rating"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this supplier from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the supplier
        $result = parent::delete();

        return $result;
    }

    public function getLogo($width = 140, $height = 140) {
        if (!empty($this->image_id)) {
            $img = Image::where('id', $this->image_id)->first();
            if (!is_object($img)) {
                return false;
            }
            return $img->getURLBySize($width, $height);
        } else {
            return false;
        }
    }

    public function getUsers($supplier_id) {
        $users = SupplierUser::where('supplier_id', $supplier_id)->get();

        $returner = [];

        foreach($users as $u) {
            $returner[] = User::where('id', $u->user_id)->first();
        }

        return new Collection($returner);
    }

    public function getMeta() {
        $returner = [];

        foreach($this->meta as $m) {
            $returner[$m->name] = $m->value;
        }
        
        return (object)$returner;
    }

    /* return meta data based on query */

    public function meta() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\SupplierMeta');
    }

    public function products()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }

    public function setMeta($name, $value) {
        $check = SupplierMeta::where('supplier_id', $this->id)->where('name', $name)->first();

        if (!is_object($check)) {
            $check = new SupplierMeta();
            $check->supplier_id = $this->id;
            $check->name = $name;
        }

        $check->value = $value;
        $check->save();
        return true;
    }

    /**
     * Load all users with access to this supplier / products
     */
    public function users()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\SupplierUser');
    }
}
