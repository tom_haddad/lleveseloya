<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Order Class
 *
 * Represents a order object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 */
class Order extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "orders";

    protected $appends = ['user_name', 'subtotal', 'tax', 'total', 'supplier_totals'];

    protected $casts = [
        'user_data' => 'array'
    ];

    protected $fillable = [
        "user_id",
        "user_data",
        "status"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        // self::creating(function($model){
        //     $model->reference = $model->supplier_id.$model->product_id.rand(100,999).$model->id;
        // });

        // self::updating(function($model){
        //     $model->url = urlEncode($model->name);
        // });
    }

    public function getSubtotalAttribute($no_format = false) {
        $order_id = $this->attributes['id'];
        $subtotal = 0;

        $items = OrderItem::where('order_id', $order_id)->get();

        foreach($items as $i) {
            $quantity = $i->quantity;
            $price = $i->product_data['discount_price'];

            $subtotal = $subtotal + ($quantity * $price);
        }

        if ($no_format) {
            return $subtotal;
        } else {
            return number_format($subtotal, 2,'.',',');
        }
    }

    public function getSupplierTotals($supplier_id = false) {
        $order_id = $this->id;

        $subtotal = 0;
        $tax = 0;
        $total = 0;

        if ($supplier_id !== false) {
            $items = OrderItem::where('order_id', $order_id)->where('supplier_id', $supplier_id)->get();
        } else {
            $items = OrderItem::where('order_id', $order_id)->get();
        }

        foreach($items as $i) {
            $quantity = $i->quantity;
            $price = $i->product_data['discount_price'];

            $subtotal = $subtotal + ($quantity * $price);
        }

        $tax = ($subtotal / 100) * 8;

        $total = $subtotal + $tax;

        return array(
            'subtotal' => number_format($subtotal, 2,'.',','),
            'tax' => number_format($tax, 2,'.',','),
            'total' => number_format($total, 2,'.',',')
        );
    }

    public function getTaxAttribute($no_format = false) {
        $tax = ($this->getSubtotalAttribute(true) / 100) * 8;

        if ($no_format) {
            return $tax;
        } else {
            return number_format($tax, 2,'.',',');
        }
    }

    public function getTotalAttribute() {
        $total = $this->getSubtotalAttribute(true) + $this->getTaxAttribute(true);

        return number_format($total, 2,'.',',');
    }

    public function getUserNameAttribute()
    {
        return json_decode($this->attributes['user_data'])->user_name;
    }


    /**
     * Delete this order from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the order
        $result = parent::delete();

        return $result;
    }

    public function items() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\OrderItem', 'order_id', 'id');
    }

    public function payment()
    {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Payment', 'id', 'payment_id');
    }

    public function user()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Account\Database\Models\User');
    }
}