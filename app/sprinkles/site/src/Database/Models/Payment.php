<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Payment Class
 *
 * Represents a payment object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 */
class Payment extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "payments";

    protected $appends = ['amount_paid_formatted'];

    protected $casts = [
        'paypal_data' => 'array'
    ];

    protected $fillable = [
        "amount_paid",
        "paypal_data"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        // self::creating(function($model){
        //     $model->url = urlEncode($model->name);
        // });

        // self::updating(function($model){
        //     $model->url = urlEncode($model->name);
        // });
    }

    /**
     * Delete this payment from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the payment
        $result = parent::delete();

        return $result;
    }

    public function getAmountPaidFormattedAttribute()
    {
        $price = $this->attributes['amount_paid'];

        return number_format($price, 2, '.', ',');
    }

    public function order()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Order', 'payment_id', 'id');
    }
}