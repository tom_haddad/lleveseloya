<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use QRcode;

/**
 * Order Item Class
 *
 * Represents a order object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 */
class OrderItem extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "order_items";

    protected $appends = ['product_name','supplier_name', 'amount_paid'];

    protected $casts = [
        'product_data' => 'array',
        'supplier_data' => 'array'
    ];

    protected $fillable = [
        "product_id",
        "supplier_id",
        "variant_id",
        "product_data",
        "supplier_data",
        "quantity",
        "status"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->reference = $model->supplier_id.'-'.$model->product_id.rand(100,999).'-'.$model->order->user_id.'-'.$model->order->id;
        });

        self::updated(function($model){
            if ($model->status == "shipped") {
                $check = OrderItem::where('order_id', $model->order_id)->where('status', 'not_shipped')->get();
                if (count($check) == 0) {
                    $model->order->status = "complete";
                    $model->order->save();
                }
            }
        });
    }

    public function getAmountPaidAttribute() {
        $quantity = $this->attributes['quantity'];
        $price = json_decode($this->attributes['product_data'])->discount_price;

        return number_format($quantity * $price, 2,'.',',');
    }

    public function getProductNameAttribute()
    {
        return json_decode($this->attributes['product_data'])->name;
    }

    public function getSupplierNameAttribute()
    {
        return json_decode($this->attributes['supplier_data'])->name;
    }

    /**
     * Delete this order from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the order
        $result = parent::delete();

        return $result;
    }

    public function order()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Order', 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Product', 'product_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Supplier', 'supplier_id', 'id');
    }
}