<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Subcategory Class
 *
 * Represents a subcategory object as stored in the database - can go as many levels deep as we like!
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Subcategory extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "category_subs";

    protected $fillable = [
        "name",
        "category_id",
        "parent_id",
        "fee"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->url = urlEncode($model->name);
        });

        self::updating(function($model){
            $model->url = urlEncode($model->name);
        });
    }

    /**
     * Delete this category from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the category
        $result = parent::delete();

        return $result;
    }

    /**
     * Load all products associated with this category
     */
    public function products()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }

    public function subcategories()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\Subcategory', 'parent_id', 'id');
    }
}
