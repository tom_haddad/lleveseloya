<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Settings Class
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 */
class Setting extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "ly_settings";

    protected $fillable = [
        "name",
        "value"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this supplier from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the supplier
        $result = parent::delete();

        return $result;
    }
}
