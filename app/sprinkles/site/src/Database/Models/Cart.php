<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Cart Class
 *
 * Represents a shopping cart item object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Cart extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "carts";

    protected $fillable = [
        "user_id",
        "product_id",
        "variant_id",
        "quantity"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Delete this category from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the category
        $result = parent::delete();

        return $result;
    }

    public function product()
    {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Product', 'id', 'product_id');
    }
    
    public function variant()
    {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\ProductVariant', 'id', 'variant_id');
    }

    public function user()
    {
        return $this->hasOne('UserFrosting\Sprinkle\Account\Database\Models\User', 'id', 'user_id');
    }
}