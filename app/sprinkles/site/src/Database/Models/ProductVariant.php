<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use UserFrosting\Sprinkle\Site\Database\Models\Product;

/**
 * Product Variant Class
 *
 * Links products to relevant variant.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class ProductVariant extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "product_variants";

    protected $appends = ['stock_remaining', 'variant_array', 'html_title', 'img'];

    protected $fillable = [
        "product_id",
        "data",
        "description",
        "sku",
        "barcode",
        "quantity"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this product from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the product
        $result = parent::delete();

        return $result;
    }

    public function getImgAttribute() {
        $check = ProductPhoto::where('product_id', $this->product->id)->where('variant_id', $this->id)->first();

        return (is_object($check)) ? $check : false;
    }

    public function getHtmlTitleAttribute()
    {
        $data = json_decode($this->attributes['data']);

        $str = "";

        $i = 1;

        if (is_array($data)) {
            foreach($data as $d) {
                $str .= $d->name;
                if ($i < count($data)) {
                    $str .= " <span>&bull;</span> ";
                }
                $i++;
            }
        } else {
            return false;
        }

        return $str;
    }

    public function getStockRemainingAttribute()
    {
        $total = $this->attributes['quantity'];

        $id = $this->attributes['id'];

        $the_items = OrderItem::where('variant_id', $id)->get();

        $items = 0;

        foreach($the_items as $i) {
            $items = $items + $i->quantity;
        }

        return $total - $items;
    }

    public function getThumbnail($width = 140, $height = 140) {
        $check = ProductPhoto::where('product_id', $this->product->id)->where('variant_id', $this->id)->first();
        if (is_object($check)) {
            $img = Image::where('id', $check->image_id)->first();
            if (!is_object($img)) {
                return '/assets-raw/site/assets/img/placeholder.png';
            }
            return $img->getURLBySize($width, $height);
        } else {
            return '/assets-raw/site/assets/img/placeholder.png';
        }
    }

    public function getVariantArrayAttribute()
    {
        return json_decode($this->attributes['data']);
    }

    /**
     * Get product object
     */
    public function product()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }
}
