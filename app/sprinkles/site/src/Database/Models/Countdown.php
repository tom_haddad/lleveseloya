<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Countdown Class
 *
 * Represents a countdown object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Countdown extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "countdowns";

    protected $fillable = [
        "day",
        "hour",
        "date",
        "repeat",
        "active"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        // self::creating(function($model){
        //     $model->url = urlEncode($model->name);
        // });

        // self::updating(function($model){
        //     $model->url = urlEncode($model->name);
        // });
    }

    public function hasProduct($id)
    {
        return (is_object(CountdownProduct::where('countdown_id', $this->id)->where('product_id', $id)->first()));
    }

    /**
     * Delete this countdown from the database
     *
     */
    public function delete()
    {
        // Delete the countdown
        $result = parent::delete();

        return $result;
    }

    public function getSeconds($day = false) {
        $curDay = date('w');
        $curHour = ltrim(date('H'), '0');
        $curMin = ltrim(date('i'), '0');
        $curSec = ltrim(date('s'), '0');

        $checkSecs = 0;

        if (!$day) {
            $day = $this->day;
        }
        if ($curDay <= $day) {
            $dayDiff = $day - $curDay;
        } else {
            $dayDiff = 7 - $curDay + $day;
        }

        $checkSecs = 86400 * $dayDiff;

        if ($curHour <= $this->hour) {
            $hourDiff = $this->hour - $curHour - 1;
        } else {
            $hourDiff = 23 - $curHour + $this->hour;
        }

        $checkSecs = $checkSecs + ($hourDiff * 60 * 60);

        $minDiff = 59 - $curMin;

        $checkSecs = $checkSecs + ($minDiff * 60);

        $secDiff = 59 - $curSec;

        $checkSecs = $checkSecs + $secDiff;

        return $checkSecs;
    }

    /**
     * Load all deals associated with this countdown
     */
    public function products()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\CountdownProduct');
    }
}
