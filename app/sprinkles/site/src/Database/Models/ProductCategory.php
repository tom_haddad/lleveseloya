<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Product Category Class
 *
 * Links products to categories.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class ProductCategory extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "product_categories";

    protected $fillable = [
        "product_id",
        "category_id"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this product from the database, along with any category associations
     *
     */
    public function delete()
    {
        // Delete the product
        $result = parent::delete();

        return $result;
    }

    public function category()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Category', 'category_id', 'id');
    }

    /**
     * Get product object
     */
    public function product()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Product', 'product_id', 'id');
    }
}
