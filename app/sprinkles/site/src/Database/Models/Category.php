<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Category Class
 *
 * Represents a category object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Category extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "categories";

    protected $fillable = [
        "name",
        "description",
        "fee"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->url = urlEncode($model->name);
        });

        self::updating(function($model){
            $model->url = urlEncode($model->name);
        });
    }

    /**
     * Delete this category from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the category
        $result = parent::delete();

        return $result;
    }

    public function getListForModal() {
        $maincat = Category::get();
        $returner = [];
        foreach($maincat as $k => $c) {
            $returner[] = array(
                'id' => 'cat-'.$c->id,
                'name' => $c->name,
                'subs' => getSubs($c->id, 1)
            );
        }

        return $returner;
    }
    /**
     * Load all products associated with this category
     */
    public function products()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }

    public function subcategories()
    {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\Subcategory', 'category_id', 'id')->whereNull('parent_id');
    }
}

function getSubs($id, $level) {
    if ($level == 1) {
        $check = Subcategory::where('category_id', $id)->where('parent_id', NULL)->get();
    } else {
        $check = Subcategory::where('parent_id', $id)->get();
    }
    $returner = array();
    if (count($check) > 0) {
        foreach($check as $c) {
            $returner[] = array(
                'id' => $c->id,
                'name' => $c->name,
                'subs' => getSubs($c->id, ++$level)
            );
        }
    }
    return $returner;
}

function urlEncode($val) {
    $val = preg_replace("/[^A-Za-z0-9 ]/", '', $val);
    $val = strtolower($val);
    $val = str_replace(' ','-', $val);
    return $val;
}
