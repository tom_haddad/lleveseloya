<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use UserFrosting\Sprinkle\Site\Database\Models\Product;

/**
 * Product Meta Class
 *
 * Links products to relevant meta.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class ProductMeta extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "product_meta";

    protected $fillable = [
        "product_id",
        "name",
        "value"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    /**
     * Delete this product from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the product
        $result = parent::delete();

        return $result;
    }

    /**
     * Get product object
     */
    public function product()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }
}
