<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use UserFrosting\Sprinkle\Site\Database\Models\ImageMeta;

/**
 * Image Class
 *
 * Represents an image object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string filename
 * @property string extension
 * @property int height
 * @property int width
 */
class Image extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "images";

    protected $fillable = [
        "filename",
        "extension",
        "height",
        "width"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    public function alias() {
        $val = $this->id;
        if ($val > 2147473718) {
            throw new \Exception("Unable to baseencode a number greater than 2147473718 for science reasons.");
        }
        $val = $val + 9929;
        $base = 31;
        $chars = '0123456789bcdfghjklmnpqrstvwxyz';
        $str = "";
        do {
            $m = $val % $base;
            $str = $chars[$m] . $str;
            $val = ($val - $m) / $base;
        } while ($val > 0);
        return $str;
    }

    public function getURLBySize($scalex = 0, $scaley = 0) {
        $alias = $this->alias();
        $extension = $this->extension;

        if ($scalex == 0 && $scaley == 0) {
            return '/get_image/'.$alias.'.'.$extension;
        } else {
            return '/get_image/'.$alias.'-'.$scalex.'x'.$scaley.'.'.$extension;
        }
    }

    public function getIdFromAlias($str) {
        $base = 31;
        $chars = '0123456789bcdfghjklmnpqrstvwxyz';
        $len = strlen($str);
        $val = 0;
        $arr = array_flip(str_split($chars));
        for ($i = 0; $i < $len; ++$i) {
            $val = $val + ($arr[$str[$i]] * pow($base, $len-$i-1));
        }
        $val = $val - 9929;
        if ($val > 2147473718) {
            throw new \Exception("Unable to basedecode a value greater than 2f09zd1 for science reasons.");
        }
        return $val;
    }

    /**
     * Delete this supplier from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the supplier
        $result = parent::delete();

        return $result;
    }
}
