<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Core\Database\Models\Model;

/**
 * Product Category Class
 *
 * Links products to categories.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class ProductPhoto extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "product_photos";

    protected $fillable = [
        "variant_id",
        "product_id",
        "image_id",
        "caption"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $check = ProductPhoto::where('product_id', $model->product_id)->where('highlight', 1)->first();
            if (!is_object($check)) {
                $model->highlight = 1;
            } else {
                if ($model->highlight == 1) {
                    $check->highlight = 0;
                    $check->save();
                }
            }
        });

        self::updating(function($model){
            $check = ProductPhoto::where('product_id', $model->product_id)->where('highlight', 1)->first();
            if (!is_object($check)) {
                $model->highlight = 1;
            } else {
                if ($model->highlight == 1) {
                    $check->highlight = 0;
                    $check->save();
                }
            }
        });
    }

    /**
     * Delete this product from the database, along with any category associations
     *
     */
    public function delete()
    {
        // Delete the product
        $result = parent::delete();

        return $result;
    }

    /**
     * Get image object
     */
    public function image()
    {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Image', 'id', 'image_id');
    }

    /**
     * Get product object
     */
    public function product()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\Product');
    }

    /**
     * Get variant object
     */
    public function variant()
    {
        return $this->belongsTo('UserFrosting\Sprinkle\Site\Database\Models\ProductVariant');
    }
}
