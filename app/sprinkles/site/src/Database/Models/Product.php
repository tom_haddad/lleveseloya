<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Eloquent\Collection as Collection;

use UserFrosting\Sprinkle\Site\Database\Models\Category;
use UserFrosting\Sprinkle\Core\Database\Models\Model;
use UserFrosting\Sprinkle\Site\Database\Models\ProductMeta;
use UserFrosting\Sprinkle\Site\Database\Models\Subcategory;
use UserFrosting\Sprinkle\Account\Database\Models\User;

/**
 * Product Class
 *
 * Represents a product object as stored in the database.
 *
 * @package Lléveselo Ya!
 * @author Tom Haddad
 *
 * @property string name
 * @property string logo
 */
class Product extends Model
{
    /**
     * @var string The name of the table for the current model.
     */
    protected $table = "products";

    // protected $appends = ['price_formatted', 'discount_price_formatted', 'total_sold', 'percentage', 'is_sold_out', 'stock_available', 'quantity_array', 'first_category'];

    protected $appends = ['price_formatted', 'discount_price_formatted', 'total_sold', 'percentage', 'is_sold_out', 'stock_available', 'quantity_array','customer_cost','featured_fee', 'sku', 'barcode', 'inventory_sentence'];

    protected $fillable = [
        "supplier_id",
        "name",
        "description",
        "price",
        "discount_price",
        "featured",
        "draft",
        "approved",
        "category_id",
        "subcategory_id",
        "quantity"
    ];

    /**
     * @var bool Enable timestamps for this class.
     */
    public $timestamps = true;

    public function addView() {
        $view = ProductView::create(array(
            'product_id' => $this->id,
            'ip_address' => $_SERVER['REMOTE_ADDR']
        ));
        $view->save();
    }

    public function assignToCountdowns() {
        $countdowns = Countdown::where('type', 'weekly')->where('active', 1)->get();

        foreach($countdowns as $c) {
            $d = CountdownProduct::create(array(
                'countdown_id' => $c->id,
                'product_id' => $this->id,
                'active' => 1
            ));
            $d->save();
        }
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->url = urlEncode($model->name);
        });

        self::created(function($model){
            $model->assignToCountdowns();
            $model->save();
        });

        self::updating(function($model){
            $model->url = urlEncode($model->name);
        });
    }

    public function category() {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Category', 'id', 'category_id');
    }

    /**
     * Delete this product from the database, along with any user associations
     *
     */
    public function delete()
    {
        // Delete the product
        $result = parent::delete();

        return $result;
    }

    public function getBarcodeAttribute()
    {
        $obj = ProductMeta::where('product_id', $this->attributes['id'])->where('name', 'barcode')->first();

        if (is_object($obj)) {
            return $obj->value;
        } else {
            return false;
        }
    }

    public function getCustomerCostAttribute()
    {
        $price = $this->attributes['discount_price'] + $this->attributes['admin_fee'];

        return number_format($price, 2, '.', ',');
    }

    public function getDiscountPriceFormattedAttribute()
    {
        $price = $this->attributes['discount_price'];

        return number_format($price, 2, '.', ',');
    }

    // public function getFirstCategoryAttribute() {
    //     $check = ProductCategory::where('product_id', $this->attributes['id'])->first();

    //     if (!is_object($check)) {
    //         return "all";
    //     } else {
    //         return $check->category->url;
    //     }
    // }

    public function getFeeInitial() {
        if (isset($this->subcategory_id) && !empty($this->subcategory_id)) {
            $checkCategory = Subcategory::where('id', $this->subcategory_id)->first();
        } else if (isset($this->category_id) && !empty($this->category_id)) {
            $checkCategory = Category::where('id', $this->category_id)->first();
        }
        if (is_object($checkCategory)) {
            $fee = $checkCategory->fee;
            $admin_fee = (($this->discount_price / 100) * $fee);
            return $admin_fee;
        }
    }

    public function getFeaturedFeeAttribute()
    {
        $price = Setting::where('name', 'featuredFee')->first()->value;

        return number_format($price, 2, '.', ',');
    }

    public function getInventorySentenceAttribute()
    {
        $variants = ProductVariant::where('product_id', $this->attributes['id'])->get();
        $amt = 0;
        $count = count($variants);

        if ($count > 0) {
            foreach($variants as $v) {
                $amt = $amt + $v->quantity;
            }

            if ($amt > 0) {
                return $amt.' in stock for '.$count.' variants';
            } else {
                return '0 in stock';
            }
        } else {
            return $this->attributes['quantity'].' in stock';
        }
    }

    public function getIsSoldOutAttribute() {
        $soldout = 0;
        $variants = ProductVariant::where('product_id', $this->attributes['id'])->get();
        $count = count($variants);

        if ($count > 0) {
            foreach($variants as $v) {
                $orders = OrderItem::where('product_id', $this->attributes['id'])->where('variant_id', $v->id)->get();
                $total = 0;

                foreach($orders as $o) {
                    $total = $total + $o->quantity;
                }

                if ($total >= $v->quantity) {
                    $soldout++;
                }
            }

            return ($soldout >= $count && $count !== 0);
        } else {
            $orders = OrderItem::where('product_id', $this->attributes['id'])->get();
            $total = 0;
            foreach($orders as $o) {
                $total = $total + $o->quantity;
            }

            if ($total >= $this->attributes['quantity']) {
                return true;
            } else {
                return false;
            }
        
        }
    }

    public function getMeta() {
        $returner = [];

        foreach($this->meta as $m) {
            $returner[$m->name] = $m->value;
        }
        
        return (object)$returner;
    }

    public function getPercentageAttribute() {
        if ($this->price > 0 && $this->discount_price > 0) {
            $percentage = (100 / $this->price) * $this->discount_price;
        } else {
            $percentage = 0;
        }

        return 100 - ceil($percentage);
    }

    public function getPriceFormattedAttribute()
    {
        $price = $this->attributes['price'];

        return number_format($price, 2, '.', ',');
    }

    public function getStockAvailableAttribute() {
        $available = 0;
        $variants = ProductVariant::where('product_id', $this->attributes['id'])->get();

        if (count($variants) == 0) {
            $orders = OrderItem::where('product_id', $this->attributes['id'])->get();
            $total = 0;
            foreach($orders as $o) {
                $total = $total + $o->quantity;
            }

            if ($total < $this->attributes['quantity']) {
                $available = $this->attributes['quantity'] - $total;
            }
        } else {
            foreach($variants as $v) {
                $orders = OrderItem::where('product_id', $this->attributes['id'])->where('variant_id', $v->id)->get();
                $total = 0;

                foreach($orders as $o) {
                    $total = $total + $o->quantity;
                }

                if ($total < $v->quantity) {
                    $available = $available + ($v->quantity - $total);
                }
            }
        }

        return $available;
    }

    public function getQuantityArrayAttribute() {
        $quantity = $this->getStockAvailableAttribute();

        $array = [];

        for($i = 1; $i <= $quantity; $i++) {
            $array[] = $i;
        }

        return $array;
    }

    public function getSkuAttribute()
    {
        $obj = ProductMeta::where('product_id', $this->attributes['id'])->where('name', 'sku')->first();

        if (is_object($obj)) {
            return $obj->value;
        } else {
            return false;
        }
    }

    public function getThumbnail($width = 140, $height = 140) {
        $check = ProductPhoto::where('product_id', $this->id)->where('highlight', 1)->first();
        if (is_object($check)) {
            $img = Image::where('id', $check->image_id)->first();
            if (!is_object($img)) {
                return '/assets-raw/site/assets/img/placeholder.png';
            }
            return $img->getURLBySize($width, $height);
        } else {
            return '/assets-raw/site/assets/img/placeholder.png';
        }
    }

    public function getTotalSoldAttribute()
    {
        $orders = OrderItem::where('product_id', $this->attributes['id'])->get();

        $total = 0;

        foreach($orders as $o) {
            $total = $total + $o->quantity;
        }

        return $total;
    }

    public function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributes);
    }

    // public function hasCategory($id)
    // {
    //     return (is_object(ProductCategory::where('product_id', $this->id)->where('category_id', $id)->first()));
    // }

    /* return meta data based on query */

    public function meta() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\ProductMeta');
    }

    public function photos() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\ProductPhoto');
    }

    public function setMeta($name, $value) {
        $check = ProductMeta::where('product_id', $this->id)->where('name', $name)->first();

        if (!is_object($check)) {
            $check = new ProductMeta();
            $check->product_id = $this->id;
            $check->name = $name;
        }

        $check->value = $value;
        $check->save();
        return true;
    }

    public function subcategory() {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Subcategory');
    }

    public function supplier() {
        return $this->hasOne('UserFrosting\Sprinkle\Site\Database\Models\Supplier', 'id', 'supplier_id');
    }

    public function variants() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\ProductVariant');
    }

    public function views() {
        return $this->hasMany('UserFrosting\Sprinkle\Site\Database\Models\ProductView');
    }
}
