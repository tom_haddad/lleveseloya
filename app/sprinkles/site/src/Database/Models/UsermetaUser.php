<?php
namespace UserFrosting\Sprinkle\Site\Database\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use UserFrosting\Sprinkle\Account\Database\Models\User;
use UserFrosting\Sprinkle\Site\Database\Models\Usermeta;

trait LinkMeta {
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function bootLinkMeta()
    {
        /**
         * Create a new Owler if necessary, and save the associated owler every time.
         */
        static::saved(function ($metaUser) {
            $metaUser->createRelatedMetaIfNotExists();

            // When creating a new OwlerUser, it might not have had a `user_id` when the `owler`
            // relationship was created.  So, we should set it on the Owler if it hasn't been set yet.
            if (!$metaUser->meta->user_id) {
                $metaUser->meta->user_id = $metaUser->id;
            }

            $metaUser->meta->save();
        });
    }
}

class UsermetaUser extends User {
    use LinkMeta;

    protected $fillable = [
        "user_name",
        "first_name",
        "last_name",
        "email",
        "locale",
        "theme",
        "group_id",
        "flag_verified",
        "flag_enabled",
        "last_activity_id",
        "password",
        "deleted_at"
    ];

    /**
     * Required to be able to access the `owler` relationship in Twig without needing to do eager loading.
     * @see http://stackoverflow.com/questions/29514081/cannot-access-eloquent-attributes-on-twig/35908957#35908957
     */
    public function __isset($name)
    {
        if (in_array($name, [
            'meta'
        ])) {
            return isset($this->meta);
        } else {
            return parent::__isset($name);
        }
    }

    // /**
    //  * Custom accessor for Owler property
    //  */
    // public function getCityAttribute($value)
    // {
    //     return (count($this->meta) ? $this->meta->value : '');
    // }

    // /**
    //  * Custom accessor for Owler property
    //  */
    // public function getCountryAttribute($value)
    // {
    //     return (count($this->meta) ? $this->meta->value : '');
    // }

    /**
     * Get the owler associated with this user.
     */
    public function meta()
    {
        return $this->hasOne('\UserFrosting\Sprinkle\Site\Database\Models\Usermeta', 'user_id');
    }

    // /**
    //  * Custom mutator for Owler property
    //  */
    // public function setCityAttribute($value)
    // {
    //     $this->createRelatedMetaIfNotExists();

    //     $this->meta->value = $value;
    // }

    // /**
    //  * Custom mutator for Owler property
    //  */
    // public function setCountryAttribute($value)
    // {
    //     $this->createRelatedMetaIfNotExists();

    //     $this->meta->value = $value;
    // }

    /**
     * If this instance doesn't already have a related Owler (either in the db on in the current object), then create one
     */
    protected function createRelatedMetaIfNotExists()
    {
        if (!count($this->meta)) {
            $meta = new Usermeta([
                'user_id' => $this->id
            ]);

            $this->setRelation('meta', $meta);
        }
    }
}